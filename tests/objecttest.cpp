#include "stdafx.h"
#include "CppUnitTest.h"
#include "object.h"
#include "objectfunctionreference.h"
#include "objectvariablereference.h"
#include "variableexception.h"
#include "resolver.h"
#include "utils.h"
#include "tostring.h"
#include "objectfactory.h"
#include "expressiontree.h"
#include "assignexpressioncommand.h"
#include "functionptrcallcommand.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace tests
{
	TEST_CLASS(objecttest)
	{
	public:		
		TEST_METHOD(ObjectTestMethod1)
		{
			hlvm::Object object;

			Assert::ExpectException<hlvm::VariableException>([&] {object.getFunctionReferenceByName("func0");});

			hlvm::Function function;
			hlvm::Function::addParameterInfoTo(function, hlvm::ParameterInfoItem({ "param0", "int", hlvm::ParameterMeaning::NONE }));
			object.addFunction("func0", function);

			object.get_locals().addVariable("local0", hlvm::Variable::from(3.14));

			auto result0 = hlvm::Resolver::resolveVariableReferenceToPtr(hlvm::ObjectVariableReference("local0", &object));
			auto result1 = hlvm::Resolver::resolveVariableReferenceToPtr(hlvm::ObjectFunctionReference("func0", &object));

			Assert::AreEqual(ravelib::getDecimals(3.14, 2), ravelib::getDecimals(result0->getDoubleValue(), 2));
			Assert::AreEqual(std::string("int"), boost::any_cast<hlvm::Function>(result1->value).get_parameterInfo().get_items()[0].typeName);
			Assert::AreEqual(hlvm::ParameterMeaning::NONE, boost::any_cast<hlvm::Function>(result1->value).get_parameterInfo().get_items()[0].meaning);

			if (result0->get_dispose())
				delete result0;

			if (result1->get_dispose())
				delete result1;
		}

		TEST_METHOD(ObjectTestMethod2)
		{
			hlvm::Object object;

			Assert::ExpectException<hlvm::VariableException>([&] {object.getFunctionReferenceByName("func0"); });

			hlvm::ObjectFactory::addVariable(object, "targetObjectVariable0", hlvm::Variable::from(1)); // this variable will be changed from inner function

			hlvm::Function function0;

			//-------
			// function1 setup
			// this function will change targetObjectVariable0, adding param0 to it
			hlvm::Function function1;
			hlvm::Function::addParameterInfoTo(function1, hlvm::ParameterInfoItem({ "param0", "int", hlvm::ParameterMeaning::NONE }));

			// expression to add param0 to targetObjectVariable0
			hlvm::ExpressionTree *etree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *eroot = new hlvm::ExpressionNode();

			etree->set_root(eroot);

			hlvm::ExpressionTree::addExpressionDataNode(eroot, { hlvm::VariableReference("param0", &(function1.get_parameters())), hlvm::ObjectVariableReference("targetObjectVariable0", &object) }, { hlvm::Operator::PLUS });

			hlvm::AssignExpressionCommand *command = new hlvm::AssignExpressionCommand(hlvm::ObjectVariableReference("targetObjectVariable0", &object), etree);

			hlvm::Function::addCommandTo(function1, command);
			//-------

			//-------
			// function0 setup
			// function0 calls function1 with parameter with value 1
			hlvm::FunctionPtrCallCommand* callCommand = new hlvm::FunctionPtrCallCommand();
			callCommand->set_variableMap(new hlvm::VariableMap());
			function1.get_parameters().set_parent(callCommand->get_variableMap()); // function1 will "slide" to callCommand when searching for parameter param0
			hlvm::FunctionPtrCallCommand::addParameterTo(callCommand, "param0", hlvm::Variable::from(1));
			callCommand->setFunctionToCall(&function1);
			hlvm::Function::addCommandTo(function0, callCommand);
			//-------

			object.addFunction("function0", function0);	// because we are using copies of functions and not references, we need to add functions to object when they are finished with initialization
			object.addFunction("function1", function1);

			auto objFunc0Reference = hlvm::ObjectFunctionReference("function0", &object);

			objFunc0Reference.execute();

			Assert::AreEqual(2, object.getVariableReferenceByName("targetObjectVariable0").getIntValue());

			objFunc0Reference.execute();

			Assert::AreEqual(3, object.getVariableReferenceByName("targetObjectVariable0").getIntValue());

			objFunc0Reference.execute();
			objFunc0Reference.execute();
			objFunc0Reference.execute();
			objFunc0Reference.execute();
			objFunc0Reference.execute();
			objFunc0Reference.execute();
			objFunc0Reference.execute();

			Assert::AreEqual(10, object.getVariableReferenceByName("targetObjectVariable0").getIntValue());
		}

		TEST_METHOD(ObjectTestMethod3)
		{
			hlvm::Object object;

			Assert::ExpectException<hlvm::VariableException>([&] {object.getFunctionReferenceByName("func0"); });

			hlvm::ObjectFactory::addVariable(object, "targetObjectVariable0", hlvm::Variable::from(1)); // this variable will be changed from inner function

			hlvm::Function function0;

			//-------
			// function1 setup
			// this function will change targetObjectVariable0, adding param0 to it
			hlvm::Function function1;
			hlvm::Function::addParameterInfoTo(function1, hlvm::ParameterInfoItem({ "param0", "int", hlvm::ParameterMeaning::NONE }));

			// expression to add param0 to targetObjectVariable0
			hlvm::ExpressionTree *etree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *eroot = new hlvm::ExpressionNode();

			etree->set_root(eroot);
			
			// in this test, we are adding none to parameter, so result will be the same value, no matter of number of calls
			hlvm::ExpressionTree::addExpressionDataNode(eroot, { hlvm::VariableReference("param0", &(function1.get_parameters())), hlvm::VariableReference::none() }, { hlvm::Operator::PLUS });

			hlvm::AssignExpressionCommand *command = new hlvm::AssignExpressionCommand(hlvm::ObjectVariableReference("targetObjectVariable0", &object), etree);

			hlvm::Function::addCommandTo(function1, command);
			//-------

			//-------
			// function0 setup
			// function0 calls function1 with parameter with value 1
			hlvm::FunctionPtrCallCommand* callCommand = new hlvm::FunctionPtrCallCommand();
			callCommand->set_variableMap(new hlvm::VariableMap());
			function1.get_parameters().set_parent(callCommand->get_variableMap()); // function1 will "slide" to callCommand when searching for parameter param0
			hlvm::FunctionPtrCallCommand::addParameterTo(callCommand, "param0", hlvm::Variable::from(1));
			callCommand->setFunctionToCall(&function1);
			hlvm::Function::addCommandTo(function0, callCommand);
			//-------

			object.addFunction("function0", function0);	// because we are using copies of functions and not references, we need to add functions to object when they are finished with initialization
			object.addFunction("function1", function1);

			auto objFunc0Reference = hlvm::ObjectFunctionReference("function0", &object);

			objFunc0Reference.execute();

			Assert::AreEqual(1, object.getVariableReferenceByName("targetObjectVariable0").getIntValue());

			objFunc0Reference.execute();

			Assert::AreEqual(1, object.getVariableReferenceByName("targetObjectVariable0").getIntValue());

			objFunc0Reference.execute();
			objFunc0Reference.execute();
			objFunc0Reference.execute();
			objFunc0Reference.execute();
			objFunc0Reference.execute();
			objFunc0Reference.execute();
			objFunc0Reference.execute();

			Assert::AreEqual(1, object.getVariableReferenceByName("targetObjectVariable0").getIntValue());
		}
	};
}