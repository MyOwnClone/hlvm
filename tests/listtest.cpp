#include "stdafx.h"
#include "CppUnitTest.h"
#include "variablelist.h"
#include "variableexception.h"
#include "variablemap.h"
#include "resolver.h"
#include "expressiontree.h"
#include "conditiontree.h"
#include "utils.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace tests
{
	TEST_CLASS(listtest)
	{
	public:
		
		TEST_METHOD(ListTestMethod1)
		{
			hlvm::VariableList list0;

			Assert::ExpectException<hlvm::VariableException>([&]{list0.getByIndex(0);});

			list0.addVariableReference(hlvm::Variable::from(0));

			Assert::AreEqual(0, list0.getByIndex(0).getIntValue());

			list0.addVariableReference(hlvm::Variable::from("test"));

			Assert::AreEqual(std::string("test"), list0.getByIndex(1).getStringValue());
		}

		TEST_METHOD(ListTestMethod2)
		{
			hlvm::VariableList list0;
			list0.addVariableReference(hlvm::Variable::from(0));

			hlvm::VariableMap map0;
			map0.addVariable("listRef", hlvm::Variable::from(hlvm::VariableListReference(0, &list0)));
			map0.addVariable("indirectListRef", hlvm::Variable::from(hlvm::VariableReference("listRef", &map0)));

			Assert::AreEqual(0, hlvm::Resolver::resolveVariableReferenceToPtr(hlvm::VariableReference("indirectListRef", &map0))->getIntValue());
		}

		TEST_METHOD(ListTestMethod3)
		{
			hlvm::VariableList list0;
			list0.addVariableReference(hlvm::Variable::from(3.14));

			hlvm::ExpressionTree *tree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode* root = new hlvm::ExpressionNode();

			tree->set_root(root);

			hlvm::ExpressionTree::addExpressionDataNode(root, { hlvm::Variable::from(1), hlvm::VariableListReference(0, &list0) }, { hlvm::Operator::PLUS });

			Assert::AreEqual(ravelib::getDecimals(4.14, 2), ravelib::getDecimals(boost::any_cast<double>(tree->execute()), 2));

			delete tree;
		}

		TEST_METHOD(ListTestMethod4)
		{
			hlvm::VariableList list0;
			list0.addVariableReference(hlvm::Variable::from(3.00));
			list0.addVariableReference(hlvm::Variable::from(3.14));

			hlvm::ConditionTree* tree = new hlvm::ConditionTree();
			hlvm::ConditionNode* root = new hlvm::ConditionNode();

			tree->set_root(root);

			hlvm::ConditionTree::addRelationConditionTo(root, { hlvm::Variable::ptrFrom(hlvm::VariableListReference(0, &list0)), hlvm::Variable::ptrFrom(hlvm::VariableListReference(1, &list0)) }, { hlvm::RelationOperators::LOWER_OR_EQUAL });

			Assert::IsTrue(tree->execute());

			delete tree;
		}

		TEST_METHOD(ListTestMethod5)
		{
			hlvm::VariableList list0;
			list0.addVariableReference(hlvm::Variable::from(3.14));
			list0.addVariableReference(hlvm::Variable::from(1.00));
			list0.addVariableReference(hlvm::Variable::from(2.00));

			hlvm::ExpressionTree *etree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode* eroot = new hlvm::ExpressionNode();

			// 1.00 * 2.00
			hlvm::ExpressionTree::addExpressionDataNode(eroot, { hlvm::VariableListReference(1, &list0), hlvm::VariableListReference(2, &list0) }, { hlvm::Operator::MULTIPLY });

			etree->set_root(eroot);

			hlvm::ConditionTree* ctree = new hlvm::ConditionTree();
			hlvm::ConditionNode* croot = new hlvm::ConditionNode();

			ctree->set_root(croot);

			// (1.00 * 2.00) < (3.14)
			hlvm::ConditionTree::addRelationConditionTo(croot, { hlvm::Variable::ptrFrom(etree), hlvm::Variable::ptrFrom(hlvm::VariableListReference(0, &list0)) }, { hlvm::RelationOperators::LOWER_OR_EQUAL });

			Assert::IsTrue(ctree->execute());

			delete ctree;
		}

		TEST_METHOD(ListTestMethod6)
		{
			hlvm::VariableList list0;
			list0.addVariableReference(hlvm::Variable::from(3.14));
			list0.addVariableReference(hlvm::Variable::from(2.00));

			hlvm::VariableMap map0;
			map0.addVariable("list0", hlvm::Variable::from(&list0));

			hlvm::ExpressionTree *tree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode* root = new hlvm::ExpressionNode();

			tree->set_root(root);

			hlvm::ExpressionTree::addExpressionDataNode(root, { hlvm::VariableListReference(1, &map0, "list0"), hlvm::VariableListReference(0, &map0, "list0") }, { hlvm::Operator::PLUS });

			Assert::AreEqual(ravelib::getDecimals(5.14, 2), ravelib::getDecimals(boost::any_cast<double>(tree->execute()), 2));

			delete tree;
		}

		TEST_METHOD(ListTestMethod7)
		{
			hlvm::VariableList list0;
			list0.addVariableReference(hlvm::Variable::from(3.14));
			list0.addVariableReference(hlvm::Variable::from(2.00));

			hlvm::VariableMap map0;
			map0.addVariable("list0", hlvm::Variable::from(&list0));
			map0.addVariable("var0", hlvm::Variable::from(2.00));

			hlvm::ExpressionTree *tree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode* root = new hlvm::ExpressionNode();

			tree->set_root(root);

			hlvm::ExpressionTree::addExpressionDataNode(root, { hlvm::VariableReference("var0", &map0), hlvm::VariableListReference(0, &map0, "list0") }, { hlvm::Operator::PLUS });

			Assert::AreEqual(ravelib::getDecimals(5.14, 2), ravelib::getDecimals(boost::any_cast<double>(tree->execute()), 2));

			delete tree;
		}

		TEST_METHOD(ListTestMethod8)
		{
			hlvm::VariableList list0;
			list0.addVariableReference(hlvm::Variable::from(3.00));
			list0.addVariableReference(hlvm::Variable::from(3.14));

			hlvm::VariableMap map0;
			map0.addVariable("list0", hlvm::Variable::from(&list0));

			hlvm::ConditionTree* tree = new hlvm::ConditionTree();
			hlvm::ConditionNode* root = new hlvm::ConditionNode();

			tree->set_root(root);

			hlvm::ConditionTree::addRelationConditionTo(root, { hlvm::Variable::ptrFrom(hlvm::VariableListReference(0, &map0, "list0")), hlvm::Variable::ptrFrom(hlvm::VariableListReference(0, &map0, "list0")) }, { hlvm::RelationOperators::LOWER_OR_EQUAL });

			Assert::IsTrue(tree->execute());

			delete tree;
		}

		TEST_METHOD(ListTestMethod9)
		{
			hlvm::VariableList list0;
			list0.addVariableReference(hlvm::Variable::from(3.14));

			hlvm::VariableMap map0;
			map0.addVariable("list0", hlvm::Variable::from(&list0));

			hlvm::ConditionTree* tree = new hlvm::ConditionTree();
			hlvm::ConditionNode* root = new hlvm::ConditionNode();

			tree->set_root(root);

			hlvm::ConditionTree::addRelationConditionTo(root, { hlvm::Variable::ptrFrom(3.00), hlvm::Variable::ptrFrom(hlvm::VariableListReference(0, &map0, "list0")) }, { hlvm::RelationOperators::LOWER_OR_EQUAL });

			Assert::IsTrue(tree->execute());

			delete tree;
		}

		TEST_METHOD(ListTestMethod10)
		{
			// list contains variable reference to map
			hlvm::VariableList list0;

			hlvm::VariableMap map0;
			map0.addVariable("var0", hlvm::Variable::from(3.14));

			list0.addVariableReference(hlvm::Variable::from(hlvm::VariableReference("var0", &map0)));

			hlvm::ConditionTree* tree = new hlvm::ConditionTree();
			hlvm::ConditionNode* root = new hlvm::ConditionNode();

			tree->set_root(root);

			hlvm::ConditionTree::addRelationConditionTo(root, { hlvm::Variable::ptrFrom(3.00), hlvm::Variable::ptrFrom(hlvm::VariableListReference(0, &list0)) }, { hlvm::RelationOperators::LOWER_OR_EQUAL });

			Assert::IsTrue(tree->execute());

			delete tree;
		}

		TEST_METHOD(ListTestMethod11)
		{
			// list contains variable reference to map
			hlvm::VariableList list0;

			hlvm::VariableMap map0;
			map0.addVariable("var0", hlvm::Variable::from(3.14));

			list0.addVariableReference(hlvm::Variable::from(hlvm::VariableReference("var0", &map0)));

			hlvm::ExpressionTree *tree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode* root = new hlvm::ExpressionNode();

			tree->set_root(root);

			hlvm::ExpressionTree::addExpressionDataNode(root, { hlvm::Variable::from(3.00), hlvm::VariableListReference(0, &list0) }, { hlvm::Operator::PLUS });

			Assert::AreEqual(ravelib::getDecimals(6.14, 2), ravelib::getDecimals(boost::any_cast<double>(tree->execute()), 2));

			delete tree;
		}

		TEST_METHOD(ListTestMethod12)
		{
			// expression references list items, one of the list items references other list item
			hlvm::VariableList list0;

			hlvm::VariableMap map0;
			map0.addVariable("var0", hlvm::Variable::from(3.14));

			list0.addVariableReference(hlvm::Variable::from(hlvm::VariableReference("var0", &map0)));
			list0.addVariableReference(hlvm::Variable::from(3.00));
			list0.addVariableReference(hlvm::Variable::from(hlvm::VariableListReference(1, &list0)));

			hlvm::ExpressionTree *tree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode* root = new hlvm::ExpressionNode();

			tree->set_root(root);

			hlvm::ExpressionTree::addExpressionDataNode(root, { hlvm::VariableListReference(2, &list0), hlvm::VariableListReference(0, &list0) }, { hlvm::Operator::PLUS });

			Assert::AreEqual(ravelib::getDecimals(6.14, 2), ravelib::getDecimals(boost::any_cast<double>(tree->execute()), 2));

			delete tree;
		}
	};
}