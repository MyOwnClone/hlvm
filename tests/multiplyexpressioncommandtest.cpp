#include "stdafx.h"
#include "CppUnitTest.h"
#include "CppUnitTest.h"
#include "multiplyexpressioncommand.h"
#include "variableexception.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace tests
{
	TEST_CLASS(multiplyexpressioncommandtest)
	{
	public:
		
		TEST_METHOD(MultiplyExpressionCommandTestMethod1)
		{
			hlvm::VariableMap map;
			map.addVariable("var0", hlvm::Variable::from(1));
			map.addVariable("var1", hlvm::Variable::from(2));
			map.addVariable("var2", hlvm::Variable::from(3));

			hlvm::ExpressionTree *tree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *root = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *left = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *right = new hlvm::ExpressionNode();

			hlvm::ExpressionNode *right0 = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *right1 = new hlvm::ExpressionNode();

			// 1 * 2
			hlvm::ExpressionTree::addExpressionDataNode(left, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::MULTIPLY });

			// 1 + 2
			hlvm::ExpressionTree::addExpressionDataNode(right0, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::PLUS });
			// 1 - 2
			hlvm::ExpressionTree::addExpressionDataNode(right1, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::MINUS });

			// (1+2)+(1-2) == 2
			hlvm::ExpressionTree::addExpressionProxyNode(right, { right0, right1 }, { hlvm::Operator::PLUS });

			// (1*2)+(5-3) == 2+2
			hlvm::ExpressionTree::addExpressionProxyNode(root, { left, right }, { hlvm::Operator::PLUS });

			tree->set_root(root);

			hlvm::MultiplyExpressionCommand *command = new hlvm::MultiplyExpressionCommand(hlvm::VariableReference("var0", &map), tree);

			command->execute();

			Assert::AreEqual(4, map.getVariableByName("var0").getIntValue());
		}

		TEST_METHOD(MultiplyExpressionCommandTestMethod2)
		{
			hlvm::VariableMap map;
			map.addVariable("var0", hlvm::Variable::from(1.0));
			map.addVariable("var1", hlvm::Variable::from(2));
			map.addVariable("var2", hlvm::Variable::from(3));

			hlvm::ExpressionTree *tree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *root = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *left = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *right = new hlvm::ExpressionNode();

			hlvm::ExpressionNode *right0 = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *right1 = new hlvm::ExpressionNode();

			// 1 * 2
			hlvm::ExpressionTree::addExpressionDataNode(left, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::MULTIPLY });

			// 1 + 2
			hlvm::ExpressionTree::addExpressionDataNode(right0, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::PLUS });
			// 1 - 2
			hlvm::ExpressionTree::addExpressionDataNode(right1, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::MINUS });

			// (1+2)+(1-2) == 2
			hlvm::ExpressionTree::addExpressionProxyNode(right, { right0, right1 }, { hlvm::Operator::PLUS });

			// (1*2)+(5-3) == 2+2
			hlvm::ExpressionTree::addExpressionProxyNode(root, { left, right }, { hlvm::Operator::PLUS });

			tree->set_root(root);

			hlvm::MultiplyExpressionCommand *command = new hlvm::MultiplyExpressionCommand(hlvm::VariableReference("var0", &map), tree);

			Assert::ExpectException<hlvm::ExpressionException>([&]{command->execute();});
		}

	};
}