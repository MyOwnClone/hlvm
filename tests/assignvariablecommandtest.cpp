#include "stdafx.h"
#include "CppUnitTest.h"
#include "variablemap.h"
#include "assignvariablecommand.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace tests
{
	TEST_CLASS(assignvariablecommandtest)
	{
	public:
		
		TEST_METHOD(AssignVariableCommandTestMethod1)
		{
			hlvm::VariableMap map;
			map.addVariable("var0", hlvm::Variable::from(0));

			hlvm::Command *command0 = new hlvm::AssignVariableCommand(hlvm::VariableReference("var0", &map), hlvm::VariableReference("var0", &map));

			command0->execute();

			Assert::AreEqual(0, map.getVariableByName("var0").getIntValue());
		}

		TEST_METHOD(AssignVariableCommandTestMethod2)
		{
			hlvm::VariableMap map;
			map.addVariable("var0", hlvm::Variable::from(0));

			hlvm::Command *command0 = new hlvm::AssignVariableCommand(hlvm::VariableReference("var0", &map), hlvm::Variable::from(1));

			command0->execute();

			Assert::AreEqual(1, map.getVariableByName("var0").getIntValue());
		}
	};
}