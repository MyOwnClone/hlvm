#include "stdafx.h"
#include "CppUnitTest.h"
#include "variablemap.h"
#include "expressiontree.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace tests
{
	TEST_CLASS(expressiontest)
	{
	public:
		
		TEST_METHOD(ExpressionTestMethod1)
		{
			hlvm::ExpressionTree *tree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *root = new hlvm::ExpressionNode();

			hlvm::ExpressionTree::addExpressionDataNode(root, { hlvm::Variable::from(0), hlvm::Variable::from(0) }, { hlvm::Operator::PLUS });
			tree->set_root(root);

			Assert::AreEqual(0, boost::any_cast<int>(tree->execute()));

			delete tree;
		}

		TEST_METHOD(ExpressionTestMethod2)
		{
			hlvm::ExpressionTree *tree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *root = new hlvm::ExpressionNode();

			hlvm::ExpressionTree::addExpressionDataNode(root, { hlvm::Variable::from(1), hlvm::Variable::from(0) }, { hlvm::Operator::PLUS });
			tree->set_root(root);

			Assert::AreEqual(1, boost::any_cast<int>(tree->execute()));

			delete tree;
		}

		TEST_METHOD(ExpressionTestMethod3)
		{
			hlvm::ExpressionTree *tree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *root = new hlvm::ExpressionNode();

			hlvm::ExpressionTree::addExpressionDataNode(root, { hlvm::Variable::from(1), hlvm::Variable::from(0) }, { hlvm::Operator::MINUS });
			tree->set_root(root);

			Assert::AreEqual(1, boost::any_cast<int>(tree->execute()));

			delete tree;
		}

		TEST_METHOD(ExpressionTestMethod4)
		{
			hlvm::ExpressionTree *tree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *root = new hlvm::ExpressionNode();

			hlvm::ExpressionTree::addExpressionDataNode(root, { hlvm::Variable::from(1), hlvm::Variable::from(1) }, { hlvm::Operator::MINUS });
			tree->set_root(root);

			Assert::AreEqual(0, boost::any_cast<int>(tree->execute()));

			delete tree;
		}

		TEST_METHOD(ExpressionTestMethod5)
		{
			hlvm::VariableMap map;
			map.addVariable("var0", hlvm::Variable::from(1));

			hlvm::ExpressionTree *tree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *root = new hlvm::ExpressionNode();

			hlvm::ExpressionTree::addExpressionDataNode(root, { hlvm::VariableReference("var0", &map), hlvm::Variable::from(1) }, { hlvm::Operator::MINUS });
			tree->set_root(root);

			Assert::AreEqual(0, boost::any_cast<int>(tree->execute()));

			delete tree;
		}

		TEST_METHOD(ExpressionTestMethod6)
		{
			hlvm::VariableMap map;
			map.addVariable("var0", hlvm::Variable::from(1));
			map.addVariable("var1", hlvm::Variable::from(2));

			hlvm::ExpressionTree *tree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *root = new hlvm::ExpressionNode();

			hlvm::ExpressionTree::addExpressionDataNode(root, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::MULTIPLY });
			tree->set_root(root);

			Assert::AreEqual(2, boost::any_cast<int>(tree->execute()));

			delete tree;
		}

		TEST_METHOD(ExpressionTestMethod7)
		{
			hlvm::VariableMap map;
			map.addVariable("var0", hlvm::Variable::from(1));
			map.addVariable("var1", hlvm::Variable::from(2));
			map.addVariable("var2", hlvm::Variable::from(3));
			map.addVariable("var3", hlvm::Variable::from(4));

			hlvm::ExpressionTree *tree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *root = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *left = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *right = new hlvm::ExpressionNode();

			// 1 * 2
			hlvm::ExpressionTree::addExpressionDataNode(left, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::MULTIPLY });
			// 4 - 3
			hlvm::ExpressionTree::addExpressionDataNode(right, { hlvm::VariableReference("var3", &map), hlvm::VariableReference("var2", &map) }, { hlvm::Operator::MINUS });

			// (1*2)+(4-3) == 2+1
			hlvm::ExpressionTree::addExpressionProxyNode(root, { left, right }, { hlvm::Operator::PLUS });

			tree->set_root(root);

			Assert::AreEqual(3, boost::any_cast<int>(tree->execute()));

			delete tree;
		}

		TEST_METHOD(ExpressionTestMethod8)
		{
			hlvm::VariableMap map;
			map.addVariable("var0", hlvm::Variable::from(1));
			map.addVariable("var1", hlvm::Variable::from(2));
			map.addVariable("var2", hlvm::Variable::from(3));

			hlvm::ExpressionTree *tree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *root = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *left = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *right = new hlvm::ExpressionNode();

			// 1 * 2
			hlvm::ExpressionTree::addExpressionDataNode(left, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::MULTIPLY });
			// 5 - 3
			hlvm::ExpressionTree::addExpressionDataNode(right, { hlvm::Variable::from(5), hlvm::VariableReference("var2", &map) }, { hlvm::Operator::MINUS });

			// (1*2)+(5-3) == 2+2
			hlvm::ExpressionTree::addExpressionProxyNode(root, { left, right }, { hlvm::Operator::PLUS });

			tree->set_root(root);

			Assert::AreEqual(4, boost::any_cast<int>(tree->execute()));

			delete tree;
		}

		TEST_METHOD(ExpressionTestMethod9)
		{
			hlvm::VariableMap map;
			map.addVariable("var0", hlvm::Variable::from(1));
			map.addVariable("var1", hlvm::Variable::from(2));
			map.addVariable("var2", hlvm::Variable::from(3));

			hlvm::ExpressionTree *tree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *root = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *left = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *right = new hlvm::ExpressionNode();

			hlvm::ExpressionNode *right0 = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *right1 = new hlvm::ExpressionNode();

			// 1 * 2
			hlvm::ExpressionTree::addExpressionDataNode(left, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::MULTIPLY });

			// 1 + 2
			hlvm::ExpressionTree::addExpressionDataNode(right0, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::PLUS });
			// 1 - 2
			hlvm::ExpressionTree::addExpressionDataNode(right1, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::MINUS });

			// (1+2)+(1-2) == 2
			hlvm::ExpressionTree::addExpressionProxyNode(right, { right0, right1 }, { hlvm::Operator::PLUS });

			// (1*2)+(5-3) == 2+2
			hlvm::ExpressionTree::addExpressionProxyNode(root, { left, right }, { hlvm::Operator::PLUS });

			tree->set_root(root);

			Assert::AreEqual(4, boost::any_cast<int>(tree->execute()));

			delete tree;
		}
	};
}