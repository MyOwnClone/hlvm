#include "stdafx.h"
#include "CppUnitTest.h"
#include "variablemap.h"
#include "expressiontree.h"
#include "assignexpressioncommand.h"
#include "assignvariablecommand.h"
#include "conditiontree.h"
#include "conditionalcommand.h"
#include "functionptrcallcommand.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace tests
{
	TEST_CLASS(copytest)
	{
	public:
		
		TEST_METHOD(CopyTestMethod1)
		{
			hlvm::VariableMap *outerMap = new hlvm::VariableMap();
			hlvm::VariableMap *innerMap = new hlvm::VariableMap();

			// outerMap is parent map of innerMap
			innerMap->set_parent(outerMap);

			// outer variable
			outerMap->addVariable("outer0", hlvm::Variable::from(0));

			// inner variable which references outer variable
			innerMap->addVariable("inner0", hlvm::Variable::from(hlvm::VariableReference("outer0", outerMap)));

			auto result = innerMap->resolveVariableByName("inner0");
			Assert::AreEqual(0, result->getIntValue());

			outerMap->resolveVariableByName("outer0")->value = 1;

			result = innerMap->resolveVariableByName("inner0");
			Assert::AreEqual(1, result->getIntValue());

			hlvm::VariableMap *innerMapCopy = innerMap->deepCopy();
			// set parent, parent is not copied by deepCopy()
			innerMapCopy->set_parent(outerMap);

			result = innerMapCopy->resolveVariableByName("inner0");
			Assert::AreEqual(1, result->getIntValue());
		}

		TEST_METHOD(CopyTestMethod2)
		{
			hlvm::VariableMap *outerMap = new hlvm::VariableMap();
			outerMap->addVariable("outer0", hlvm::Variable::from(0));

			hlvm::ExpressionTree *expressionTree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *expressionRoot = new hlvm::ExpressionNode();

			hlvm::ExpressionTree::addExpressionDataNode(expressionRoot, { hlvm::VariableReference("outer0", outerMap), hlvm::Variable::from(1) }, { hlvm::Operator::PLUS });
			expressionTree->set_root(expressionRoot);

			hlvm::Command* command0 = new hlvm::AssignExpressionCommand(hlvm::VariableReference("outer0", outerMap), expressionTree);

			Assert::IsTrue(command0->execute());
			Assert::AreEqual(1, outerMap->getVariableByName("outer0").getIntValue());

			hlvm::Command* command0Copy = command0->deepCopy();

			delete command0;

			outerMap->getVariableByName("outer0").value = 0;
			Assert::AreEqual(0, outerMap->getVariableByName("outer0").getIntValue());
			Assert::IsTrue(command0Copy->execute());
			Assert::AreEqual(1, outerMap->getVariableByName("outer0").getIntValue());
		}

		TEST_METHOD(CopyTestMethod3)
		{
			hlvm::VariableMap globals;
			globals.addVariable("global0", hlvm::Variable::from(111));
			globals.addVariable("result", hlvm::Variable::from(0));

			hlvm::FunctionPtrCallCommand *callCommand0 = new hlvm::FunctionPtrCallCommand();
			hlvm::Function calledFunction0;

			hlvm::Function::addParameterInfoTo(calledFunction0, hlvm::ParameterInfoItem({ "param0", typeid(hlvm::VariableReference).name(), hlvm::ParameterMeaning::NONE }));

			auto callCommand0VariableMap = new hlvm::VariableMap();

			// create default vmap for command
			callCommand0->set_variableMap(callCommand0VariableMap);
			// make globals its parent so we can connect "param0" in functions parameters to global0 in globals
			callCommand0->get_variableMap()->set_parent(&globals);
			// functions own parameter variable map has parent set to commands variable map
			calledFunction0.get_parameters().set_parent(callCommand0->get_variableMap());
			// param0 is set to "point" to variable global0 which is stored in globals variablemap, so there is to be recursive search from child variable maps to parents
			callCommand0->get_variableMap()->addVariable("param0", hlvm::Variable::from(hlvm::VariableReference("global0", &calledFunction0.get_parameters())));

			// assign param0 to result, param0 links to global0
			hlvm::AssignVariableCommand *innerCommand = new hlvm::AssignVariableCommand(hlvm::VariableReference("result", &calledFunction0.get_parameters()), hlvm::VariableReference("param0", &calledFunction0.get_parameters()));
			hlvm::Function::addCommandTo(calledFunction0, innerCommand);

			callCommand0->setFunctionToCall(&calledFunction0);

			Assert::IsTrue(callCommand0->execute());
			Assert::AreEqual(111, globals.getVariableByName("result").getIntValue());

			auto commandCopy = callCommand0->deepCopy();

			delete callCommand0;

			globals.getVariableByName("result").value = 0;
			Assert::AreEqual(0, globals.getVariableByName("result").getIntValue());
			Assert::IsTrue(commandCopy->execute());
			Assert::AreEqual(111, globals.getVariableByName("result").getIntValue());
		}

		TEST_METHOD(CopyTestMethod4)
		{
			hlvm::VariableMap globals;
			globals.addVariable("global0", hlvm::Variable::from(1));
			globals.addVariable("result", hlvm::Variable::from(0));

			hlvm::ExpressionTree *expressionTree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *expressionRoot = new hlvm::ExpressionNode();

			hlvm::ExpressionTree::addExpressionDataNode(expressionRoot, { hlvm::VariableReference("global0", &globals), hlvm::Variable::from(1) }, { hlvm::Operator::PLUS });
			expressionTree->set_root(expressionRoot);

			hlvm::FunctionPtrCallCommand *callCommand0 = new hlvm::FunctionPtrCallCommand();
			hlvm::Function calledFunction0;

			auto callCommand0VariableMap = new hlvm::VariableMap();

			// create default vmap for command
			callCommand0->set_variableMap(callCommand0VariableMap);
			// make globals its parent so we can connect "param0" in functions parameters to global0 in globals
			callCommand0->get_variableMap()->set_parent(&globals);
			calledFunction0.get_parameters().set_parent(callCommand0->get_variableMap());

			hlvm::Function::addParameterInfoTo(calledFunction0, hlvm::ParameterInfoItem({ "param0", typeid(hlvm::VariableReference).name(), hlvm::ParameterMeaning::NONE }));
			hlvm::Command* command0 = new hlvm::AssignExpressionCommand(hlvm::VariableReference("result", &calledFunction0.get_parameters()), expressionTree);
			hlvm::Function::addCommandTo(calledFunction0, command0);

			callCommand0->get_variableMap()->addVariable("param0", hlvm::Variable::from(hlvm::VariableReference("global0", &calledFunction0.get_parameters())));

			callCommand0->setFunctionToCall(&calledFunction0);

			Assert::IsTrue(callCommand0->execute());
			Assert::AreEqual(2, globals.getVariableByName("result").getIntValue());

			auto commandCopy = callCommand0->deepCopy();

			delete callCommand0;

			globals.getVariableByName("result").value = 0;
			Assert::AreEqual(0, globals.getVariableByName("result").getIntValue());
			Assert::IsTrue(commandCopy->execute());
			Assert::AreEqual(2, globals.getVariableByName("result").getIntValue());
		}

		TEST_METHOD(CopyTestMethod5)
		{
			// recursive call
			// creates command which calls function which stores its param0 value to result variable

			hlvm::VariableMap globals;
			globals.addVariable("global0", hlvm::Variable::from(0));

			hlvm::FunctionPtrCallCommand *outerCallCommand = new hlvm::FunctionPtrCallCommand();
			hlvm::FunctionPtrCallCommand *innerCallCommand = new hlvm::FunctionPtrCallCommand();
			hlvm::Function calledFunction;

			// function needs one parameter, of type hlvm::VariableReference
			hlvm::Function::addParameterInfoTo(calledFunction, hlvm::ParameterInfoItem({ "param0", typeid(hlvm::VariableReference).name(), hlvm::ParameterMeaning::NONE })); //TODO: resolve VariableReference and get real type

			// create default vmap for command
			// both commands have their own inner vmaps
			outerCallCommand->set_variableMap(new hlvm::VariableMap());
			innerCallCommand->set_variableMap(new hlvm::VariableMap());
			// make globals its parent so we can connect "param0" in functions parameters to global0 in globals
			outerCallCommand->get_variableMap()->set_parent(&globals);
			innerCallCommand->get_variableMap()->set_parent(&globals);
			// functions own parameter variable map has parent set to commands variable map
			hlvm::Function::setParentVariableMap(calledFunction, outerCallCommand->get_variableMap());
			// param0 is set to "point" to variable global0 which is stored in globals variablemap, so there is to be recursive search from child variable maps to parents
			// actual parameters - global0 - will be found in uppermost vmap
			hlvm::FunctionPtrCallCommand::addParameterTo(outerCallCommand, "param0", hlvm::Variable::from(hlvm::VariableReference("global0", &calledFunction.get_parameters())));
			hlvm::FunctionPtrCallCommand::addParameterTo(innerCallCommand, "param0", hlvm::Variable::from(hlvm::VariableReference("global0", &calledFunction.get_parameters())));

			hlvm::ConditionTree* conditionTree = new hlvm::ConditionTree();

			// is param0 <= 10
			conditionTree->set_root(new hlvm::ConditionNode(new hlvm::RelationCondition({ hlvm::Variable::ptrFrom(hlvm::VariableReference("param0", &calledFunction.get_parameters())), hlvm::Variable::ptrFrom(10) }, { hlvm::RelationOperators::LOWER_OR_EQUAL })));

			hlvm::Block conditionalBlock;
			hlvm::Block::addCommandTo(&conditionalBlock, innerCallCommand);

			hlvm::ConditionalCommand *conditionalCommand = new hlvm::ConditionalCommand(conditionalBlock, conditionTree);

			hlvm::ExpressionTree *expressionTree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *root = new hlvm::ExpressionNode();

			// expression: param0 + 1
			hlvm::ExpressionTree::addExpressionDataNode(root, { hlvm::VariableReference("param0", &calledFunction.get_parameters()), hlvm::Variable::from(1) }, { hlvm::Operator::PLUS });
			expressionTree->set_root(root);

			// param0 = param0 + 1
			hlvm::AssignExpressionCommand *innerAssignCommand = new hlvm::AssignExpressionCommand(hlvm::VariableReference("param0", &calledFunction.get_parameters()), expressionTree);
			hlvm::Function::addCommandTo(calledFunction, innerAssignCommand); // param0 = param0+1
			hlvm::Function::addCommandTo(calledFunction, conditionalCommand); // if (param0<=10) callFunction()

			outerCallCommand->setFunctionToCall(&calledFunction);
			innerCallCommand->setFunctionToCall(&calledFunction);

			Assert::IsTrue(outerCallCommand->execute());
			Assert::AreEqual(11, globals.getVariableByName("global0").getIntValue());

			auto commandCopy = outerCallCommand->deepCopy();

			delete outerCallCommand;

			globals.getVariableByName("global0").value = 0;
			Assert::IsTrue(commandCopy->execute());
			Assert::AreEqual(11, globals.getVariableByName("global0").getIntValue());
		}
	};
}