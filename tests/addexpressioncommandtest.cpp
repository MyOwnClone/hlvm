#include "stdafx.h"
#include "CppUnitTest.h"
#include "addexpressioncommand.h"
#include "variableexception.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace tests
{
	TEST_CLASS(addexpressioncommandtest)
	{
	public:
		
		TEST_METHOD(AddExpressionCommandTestMethod1)
		{
			hlvm::VariableMap map;
			map.addVariable("var0", hlvm::Variable::from(1));
			map.addVariable("var1", hlvm::Variable::from(2));
			map.addVariable("var2", hlvm::Variable::from(3));

			hlvm::ExpressionTree *tree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *root = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *left = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *right = new hlvm::ExpressionNode();

			hlvm::ExpressionNode *right0 = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *right1 = new hlvm::ExpressionNode();

			// 1 * 2
			hlvm::ExpressionTree::addExpressionDataNode(left, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::MULTIPLY });

			// 1 + 2
			hlvm::ExpressionTree::addExpressionDataNode(right0, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::PLUS });
			// 1 - 2
			hlvm::ExpressionTree::addExpressionDataNode(right1, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::MINUS });

			// (1+2)+(1-2) == 2
			hlvm::ExpressionTree::addExpressionProxyNode(right, { right0, right1 }, { hlvm::Operator::PLUS });

			// (1*2)+(5-3) == 2+2
			hlvm::ExpressionTree::addExpressionProxyNode(root, { left, right }, { hlvm::Operator::PLUS });

			tree->set_root(root);

			hlvm::AddExpressionCommand *command = new hlvm::AddExpressionCommand(hlvm::VariableReference("var0", &map), tree);

			command->execute();

			Assert::AreEqual(5, map.getVariableByName("var0").getIntValue());
		}

		TEST_METHOD(AddExpressionCommandTestMethod2)
		{
			hlvm::VariableMap map;
			map.addVariable("var0", hlvm::Variable::from(1));
			map.addVariable("var1", hlvm::Variable::from(2));
			map.addVariable("var2", hlvm::Variable::from(3));
			map.addVariable("var3", hlvm::Variable::from(3.0));

			hlvm::ExpressionTree *tree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *root = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *left = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *right = new hlvm::ExpressionNode();

			hlvm::ExpressionNode *right0 = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *right1 = new hlvm::ExpressionNode();

			// 1 * 2
			hlvm::ExpressionTree::addExpressionDataNode(left, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::MULTIPLY });

			// 1 + 2
			hlvm::ExpressionTree::addExpressionDataNode(right0, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::PLUS });
			// 1 - 2
			hlvm::ExpressionTree::addExpressionDataNode(right1, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::MINUS });

			// (1+2)+(1-2) == 2
			hlvm::ExpressionTree::addExpressionProxyNode(right, { right0, right1 }, { hlvm::Operator::PLUS });

			// (1*2)+(5-3) == 2+2
			hlvm::ExpressionTree::addExpressionProxyNode(root, { left, right }, { hlvm::Operator::PLUS });

			tree->set_root(root);

			hlvm::AddExpressionCommand *command = new hlvm::AddExpressionCommand(hlvm::VariableReference("var3", &map), tree);

			Assert::ExpectException<hlvm::VariableException>([&]{command->execute();});
		}

		TEST_METHOD(AddExpressionCommandTestMethod3)
		{
			hlvm::VariableMap map;
			map.addVariable("var0", hlvm::Variable::from(1.0));
			map.addVariable("var1", hlvm::Variable::from(2.0));
			map.addVariable("var2", hlvm::Variable::from(3.0));

			hlvm::ExpressionTree *tree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *root = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *left = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *right = new hlvm::ExpressionNode();

			hlvm::ExpressionNode *right0 = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *right1 = new hlvm::ExpressionNode();

			// 1 * 2
			hlvm::ExpressionTree::addExpressionDataNode(left, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::MULTIPLY });

			// 1 + 2
			hlvm::ExpressionTree::addExpressionDataNode(right0, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::PLUS });
			// 1 - 2
			hlvm::ExpressionTree::addExpressionDataNode(right1, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::MINUS });

			// (1+2)+(1-2) == 2
			hlvm::ExpressionTree::addExpressionProxyNode(right, { right0, right1 }, { hlvm::Operator::PLUS });

			// (1*2)+(5-3) == 2+2
			hlvm::ExpressionTree::addExpressionProxyNode(root, { left, right }, { hlvm::Operator::PLUS });

			tree->set_root(root);

			hlvm::AddExpressionCommand *command = new hlvm::AddExpressionCommand(hlvm::VariableReference("var0", &map), tree);

			command->execute();

			Assert::AreEqual(5.0, map.getVariableByName("var0").getDoubleValue());
		}

		TEST_METHOD(AddExpressionCommandTestMethod4)
		{
			hlvm::VariableMap map;
			map.addVariable("var0", hlvm::Variable::from("str"));
			map.addVariable("var1", hlvm::Variable::from("xxx"));

			hlvm::ExpressionTree *tree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *root = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *left = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *right = new hlvm::ExpressionNode();

			hlvm::ExpressionNode *right0 = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *right1 = new hlvm::ExpressionNode();

			hlvm::ExpressionTree::addExpressionDataNode(root, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::PLUS });

			tree->set_root(root);

			hlvm::AddExpressionCommand *command = new hlvm::AddExpressionCommand(hlvm::VariableReference("var0", &map), tree);

			command->execute();

			Assert::AreEqual(std::string("strstrxxx"), map.getVariableByName("var0").getStringValue());
		}

		TEST_METHOD(AddExpressionCommandTestMethod5)
		{
			hlvm::VariableMap map;
			map.addVariable("var0", hlvm::Variable::from(0));
			map.addVariable("var1", hlvm::Variable::from(1));

			hlvm::ExpressionTree *tree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *root = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *left = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *right = new hlvm::ExpressionNode();

			hlvm::ExpressionNode *right0 = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *right1 = new hlvm::ExpressionNode();

			hlvm::ExpressionTree::addExpressionDataNode(root, { hlvm::VariableReference("var1", &map), hlvm::VariableReference::none() }, { hlvm::Operator::PLUS });

			tree->set_root(root);

			hlvm::AddExpressionCommand *command = new hlvm::AddExpressionCommand(hlvm::VariableReference("var0", &map), tree);

			command->execute();

			Assert::AreEqual(1, map.getVariableByName("var0").getIntValue());
		}
	};
}