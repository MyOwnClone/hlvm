#include "stdafx.h"
#include "CppUnitTest.h"
#include "tostring.h"
#include "variable.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace tests
{
	TEST_CLASS(VariableTest)
	{
	public:
		
		TEST_METHOD(VariableTestMethod)
		{
			// create int variable
			auto var0 = hlvm::Variable::from(0);
			// try to get it
			Assert::AreEqual(0, var0.getIntValue());

			// create float variable
			auto var1 = hlvm::Variable::from(0.0f);
			// try to get it
			Assert::AreEqual(0.0f, var1.getFloatValue());

			// create double variable
			auto var2 = hlvm::Variable::from(0.0);
			// try to get it
			Assert::AreEqual(0.0, var2.getDoubleValue());

			// create string variable
			auto var3 = hlvm::Variable::from(std::string("test"));
			// try to get it
			Assert::AreEqual(std::string("test"), var3.getStringValue() );

			// create variable reference variable
			auto var4 = hlvm::Variable::from(hlvm::VariableReference("var0", NULL));
			// try to get its variable name
			Assert::AreEqual(std::string("var0"), var4.getVariableReferenceValue().get_name());
			// try to get its map instance
			Assert::AreEqual((hlvm::VariableMap*)NULL, var4.getVariableReferenceValue().get_map());
		}
	};
}