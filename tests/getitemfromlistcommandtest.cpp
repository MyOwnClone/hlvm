#include "stdafx.h"
#include "CppUnitTest.h"
#include "variablelist.h"
#include "getitemfromlistcommand.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace tests
{
	TEST_CLASS(getitemfromlistcommandtest)
	{
	public:
		
		TEST_METHOD(GetItemFromListCommandTestMethod1)
		{
			hlvm::VariableList list0;
			list0.addVariableCopy(hlvm::Variable::from(1));

			hlvm::Variable var0;

			hlvm::GetItemFromListCommand* command = new hlvm::GetItemFromListCommand(var0, list0, 0);

			command->execute();

			Assert::AreEqual(1, var0.getIntValue());
		}

	};
}