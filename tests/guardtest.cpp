#include "stdafx.h"
#include "CppUnitTest.h"
#include "guard.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace tests
{
	class Test : public hlvm::GuardedClass
	{
	public:
		int i, j;
		Test(int i, int j) : i(i), j(j) {};
		Test(int i) : i(i) {}
	};

	TEST_CLASS(guardtest)
	{
	public:
		
		TEST_METHOD(GuardTestMethod1)
		{
			auto test = GUARDED_ALLOC(new Test(0, 0));

			Assert::AreEqual(0, ((Test*)test)->i);
			Assert::AreEqual(1, test->getCounter());

			GUARDED_FREE(test);
			Assert::AreEqual(0, test->getCounter());
			Assert::ExpectException<std::runtime_error>([&] {test->decrement();});
		}
	};
}