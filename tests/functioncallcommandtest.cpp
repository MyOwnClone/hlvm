#include "stdafx.h"
#include "CppUnitTest.h"
#include "assignvariablecommand.h"
#include "assignexpressioncommand.h"
#include "conditionalcommand.h"
#include "functionptrcallcommand.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace tests
{
	TEST_CLASS(functioncallcommandtest)
	{
	public:
		
		TEST_METHOD(FunctionCallCommandTestMethod1)
		{
			// creates command which calls function which stores its param0 value to result variable

			hlvm::VariableMap globals;
			globals.addVariable("global0", hlvm::Variable::from(111));
			globals.addVariable("result", hlvm::Variable::from(0));

			hlvm::FunctionPtrCallCommand *callCommand0 = new hlvm::FunctionPtrCallCommand();
			hlvm::Function calledFunction0;

			hlvm::Function::addParameterInfoTo(calledFunction0, hlvm::ParameterInfoItem({ "param0", typeid(hlvm::VariableReference).name(), hlvm::ParameterMeaning::NONE }));

			// create default vmap for command
			callCommand0->set_variableMap(new hlvm::VariableMap());
			// make globals its parent so we can connect "param0" in functions parameters to global0 in globals
			callCommand0->get_variableMap()->set_parent(&globals);
			// functions own parameter variable map has parent set to commands variable map
			calledFunction0.get_parameters().set_parent(callCommand0->get_variableMap());
			// param0 is set to "point" to variable global0 which is stored in globals variablemap, so there is to be recursive search from child variable maps to parents
			callCommand0->get_variableMap()->addVariable("param0", hlvm::Variable::from(hlvm::VariableReference("global0", &calledFunction0.get_parameters())));

			hlvm::AssignVariableCommand *innerCommand = new hlvm::AssignVariableCommand(hlvm::VariableReference("result", &calledFunction0.get_parameters()), hlvm::VariableReference("param0", &calledFunction0.get_parameters()));
			hlvm::Function::addCommandTo(calledFunction0, innerCommand);

			callCommand0->setFunctionToCall(&calledFunction0);

			Assert::IsTrue(callCommand0->execute());

			Assert::AreEqual(111, globals.getVariableByName("result").getIntValue());
		}

		TEST_METHOD(FunctionCallCommandTestMethod2)
		{
			// creates command which calls function which stores its param0 value to result variable

			hlvm::VariableMap globals;
			globals.addVariable("global0", hlvm::Variable::from(0));

			hlvm::FunctionPtrCallCommand *callCommand0 = new hlvm::FunctionPtrCallCommand();
			hlvm::Function calledFunction0;

			hlvm::Function::addParameterInfoTo(calledFunction0, hlvm::ParameterInfoItem({ "param0", typeid(hlvm::VariableReference).name(), hlvm::ParameterMeaning::NONE }));

			// create default vmap for command
			callCommand0->set_variableMap(new hlvm::VariableMap());
			// make globals its parent so we can connect "param0" in functions parameters to global0 in globals
			callCommand0->get_variableMap()->set_parent(&globals);
			// functions own parameter variable map has parent set to commands variable map
			calledFunction0.get_parameters().set_parent(callCommand0->get_variableMap());
			// param0 is set to "point" to variable global0 which is stored in globals variablemap, so there is to be recursive search from child variable maps to parents
			callCommand0->get_variableMap()->addVariable("param0", hlvm::Variable::from(hlvm::VariableReference("global0", &calledFunction0.get_parameters())));

			hlvm::ExpressionTree *tree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *root = new hlvm::ExpressionNode();

			hlvm::ExpressionTree::addExpressionDataNode(root, { hlvm::VariableReference("param0", &calledFunction0.get_parameters()), hlvm::Variable::from(1) }, { hlvm::Operator::PLUS });
			tree->set_root(root);

			hlvm::AssignExpressionCommand *innerCommand = new hlvm::AssignExpressionCommand(hlvm::VariableReference("param0", &calledFunction0.get_parameters()), tree);
			hlvm::Function::addCommandTo(calledFunction0, innerCommand);

			callCommand0->setFunctionToCall(&calledFunction0);

			Assert::IsTrue(callCommand0->execute());

			Assert::AreEqual(1, globals.getVariableByName("global0").getIntValue());
		}

		TEST_METHOD(FunctionCallCommandTestMethod3)
		{
			// recursive call
			// creates command which calls function which stores its param0 value to result variable

			hlvm::VariableMap globals;
			globals.addVariable("global0", hlvm::Variable::from(0));

			hlvm::FunctionPtrCallCommand *outerCallCommand = new hlvm::FunctionPtrCallCommand();
			hlvm::FunctionPtrCallCommand *innerCallCommand = new hlvm::FunctionPtrCallCommand();
			hlvm::Function calledFunction;

			// function needs one parameter, of type hlvm::VariableReference
			hlvm::Function::addParameterInfoTo(calledFunction, hlvm::ParameterInfoItem({ "param0", typeid(hlvm::VariableReference).name(), hlvm::ParameterMeaning::NONE })); //TODO: resolve VariableReference and get real type

			// create default vmap for command
			// both commands have their own inner vmaps
			outerCallCommand->set_variableMap(new hlvm::VariableMap());
			innerCallCommand->set_variableMap(new hlvm::VariableMap());
			// make globals its parent so we can connect "param0" in functions parameters to global0 in globals
			outerCallCommand->get_variableMap()->set_parent(&globals);
			innerCallCommand->get_variableMap()->set_parent(&globals);
			// functions own parameter variable map has parent set to commands variable map
			hlvm::Function::setParentVariableMap(calledFunction, outerCallCommand->get_variableMap());
			// param0 is set to "point" to variable global0 which is stored in globals variablemap, so there is to be recursive search from child variable maps to parents
			// actual parameters - global0 - will be found in uppermost vmap
			hlvm::FunctionPtrCallCommand::addParameterTo(outerCallCommand, "param0", hlvm::Variable::from(hlvm::VariableReference("global0", &calledFunction.get_parameters())));
			hlvm::FunctionPtrCallCommand::addParameterTo(innerCallCommand, "param0", hlvm::Variable::from(hlvm::VariableReference("global0", &calledFunction.get_parameters())));

			hlvm::ConditionTree* conditionTree = new hlvm::ConditionTree();

			// is param0 <= 10
			conditionTree->set_root(new hlvm::ConditionNode(new hlvm::RelationCondition({ hlvm::Variable::ptrFrom(hlvm::VariableReference("param0", &calledFunction.get_parameters())), hlvm::Variable::ptrFrom(10) }, { hlvm::RelationOperators::LOWER_OR_EQUAL })));

			hlvm::Block conditionalBlock;
			hlvm::Block::addCommandTo(&conditionalBlock, innerCallCommand);

			hlvm::ConditionalCommand *conditionalCommand = new hlvm::ConditionalCommand(conditionalBlock, conditionTree);

			hlvm::ExpressionTree *expressionTree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *root = new hlvm::ExpressionNode();

			// expression: param0 + 1
			hlvm::ExpressionTree::addExpressionDataNode(root, { hlvm::VariableReference("param0", &calledFunction.get_parameters()), hlvm::Variable::from(1) }, { hlvm::Operator::PLUS });
			expressionTree->set_root(root);

			// param0 = param0 + 1
			hlvm::AssignExpressionCommand *innerAssignCommand = new hlvm::AssignExpressionCommand(hlvm::VariableReference("param0", &calledFunction.get_parameters()), expressionTree);
			hlvm::Function::addCommandTo(calledFunction, innerAssignCommand); // param0 = param0+1
			hlvm::Function::addCommandTo(calledFunction, conditionalCommand); // if (param0<=10) callFunction()

			outerCallCommand->setFunctionToCall(&calledFunction);
			innerCallCommand->setFunctionToCall(&calledFunction);

			Assert::IsTrue(outerCallCommand->execute());

			Assert::AreEqual(11, globals.getVariableByName("global0").getIntValue());
		}

		TEST_METHOD(FunctionCallCommandTestMethod4)
		{
			// recursive call - trying deeper recursion
			// creates command which calls function which stores its param0 value to result variable

			hlvm::VariableMap globals;
			globals.addVariable("global0", hlvm::Variable::from(0));

			hlvm::FunctionPtrCallCommand *outerCallCommand = new hlvm::FunctionPtrCallCommand();
			hlvm::FunctionPtrCallCommand *innerCallCommand = new hlvm::FunctionPtrCallCommand();
			hlvm::Function calledFunction;

			// function needs one parameter, of type hlvm::VariableReference
			hlvm::Function::addParameterInfoTo(calledFunction, hlvm::ParameterInfoItem({ "param0", typeid(hlvm::VariableReference).name(), hlvm::ParameterMeaning::NONE })); //TODO: resolve VariableReference and get real type

			// create default vmap for command
			// both commands have their own inner vmaps
			outerCallCommand->set_variableMap(new hlvm::VariableMap());
			innerCallCommand->set_variableMap(new hlvm::VariableMap());
			// make globals its parent so we can connect "param0" in functions parameters to global0 in globals
			outerCallCommand->get_variableMap()->set_parent(&globals);
			innerCallCommand->get_variableMap()->set_parent(&globals);
			// functions own parameter variable map has parent set to commands variable map
			hlvm::Function::setParentVariableMap(calledFunction, outerCallCommand->get_variableMap());
			// param0 is set to "point" to variable global0 which is stored in globals variablemap, so there is to be recursive search from child variable maps to parents
			// actual parameters - global0 - will be found in uppermost vmap
			hlvm::FunctionPtrCallCommand::addParameterTo(outerCallCommand, "param0", hlvm::Variable::from(hlvm::VariableReference("global0", &calledFunction.get_parameters())));
			hlvm::FunctionPtrCallCommand::addParameterTo(innerCallCommand, "param0", hlvm::Variable::from(hlvm::VariableReference("global0", &calledFunction.get_parameters())));

			hlvm::ConditionTree* conditionTree = new hlvm::ConditionTree();

			auto depthToTry = 500;

			// is param0 <= 10
			conditionTree->set_root(new hlvm::ConditionNode(new hlvm::RelationCondition({ hlvm::Variable::ptrFrom(hlvm::VariableReference("param0", &calledFunction.get_parameters())), hlvm::Variable::ptrFrom(depthToTry) }, { hlvm::RelationOperators::LOWER_OR_EQUAL })));

			hlvm::Block conditionalBlock;
			hlvm::Block::addCommandTo(&conditionalBlock, innerCallCommand);

			hlvm::ConditionalCommand *conditionalCommand = new hlvm::ConditionalCommand(conditionalBlock, conditionTree);

			hlvm::ExpressionTree *expressionTree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *root = new hlvm::ExpressionNode();

			// expression: param0 + 1
			hlvm::ExpressionTree::addExpressionDataNode(root, { hlvm::VariableReference("param0", &calledFunction.get_parameters()), hlvm::Variable::from(1) }, { hlvm::Operator::PLUS });
			expressionTree->set_root(root);

			// param0 = param0 + 1
			hlvm::AssignExpressionCommand *innerAssignCommand = new hlvm::AssignExpressionCommand(hlvm::VariableReference("param0", &calledFunction.get_parameters()), expressionTree);
			hlvm::Function::addCommandTo(calledFunction, innerAssignCommand); // param0 = param0+1
			hlvm::Function::addCommandTo(calledFunction, conditionalCommand); // if (param0<=10) callFunction()

			outerCallCommand->setFunctionToCall(&calledFunction);
			innerCallCommand->setFunctionToCall(&calledFunction);

			Assert::IsTrue(outerCallCommand->execute());

			Assert::AreEqual(depthToTry + 1, globals.getVariableByName("global0").getIntValue());
		}

	};
}