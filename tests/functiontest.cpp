#include "stdafx.h"
#include "CppUnitTest.h"
#include "function.h"
#include "assignvariablecommand.h"
#include "expressiontree.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace tests
{
	TEST_CLASS(functiontest)
	{
	public:
		
		TEST_METHOD(FunctionTestMethod1)
		{
			hlvm::Function function0;

			hlvm::Function::addParameterInfoTo(&function0, hlvm::ParameterInfoItem({ "param0", typeid(int).name(), hlvm::ParameterMeaning::NONE }));
			hlvm::Function::addParameterInfoTo(&function0, hlvm::ParameterInfoItem({ "param1", typeid(float).name(), hlvm::ParameterMeaning::NONE }));
			hlvm::Function::addParameterInfoTo(&function0, hlvm::ParameterInfoItem({ "param2", typeid(double).name(), hlvm::ParameterMeaning::NONE }));
			hlvm::Function::addParameterInfoTo(&function0, hlvm::ParameterInfoItem({ "param3", typeid(int).name(), hlvm::ParameterMeaning::NONE }));

			hlvm::AssignVariableCommand assignCommand(hlvm::VariableReference("param0", &function0.get_parameters()), hlvm::VariableReference("param3", &function0.get_parameters()));
			hlvm::Function::addCommandTo(&function0, &assignCommand);

			Assert::IsFalse(function0.execute());

			hlvm::Function::addParameterTo(&function0, "param0", hlvm::Variable::from(0));
			hlvm::Function::addParameterTo(&function0, "param1", hlvm::Variable::from(0.0f));
			hlvm::Function::addParameterTo(&function0, "param2", hlvm::Variable::from(0.0));
			hlvm::Function::addParameterTo(&function0, "param3", hlvm::Variable::from(1));

			Assert::IsTrue(function0.execute());

			Assert::AreEqual(1, function0.get_parameters().getVariableByName("param0").getIntValue());
		}

		TEST_METHOD(FunctionTestMethod2)
		{
			hlvm::VariableMap map;
			map.addVariable("local0", hlvm::Variable::from(111));
			hlvm::Function function0;
			function0.get_parameters().set_parent(&map);

			hlvm::Function::addParameterInfoTo(&function0, hlvm::ParameterInfoItem({ "param0", typeid(int).name(), hlvm::ParameterMeaning::NONE }));
			hlvm::Function::addParameterInfoTo(&function0, hlvm::ParameterInfoItem({ "param1", typeid(float).name(), hlvm::ParameterMeaning::NONE }));
			hlvm::Function::addParameterInfoTo(&function0, hlvm::ParameterInfoItem({ "param2", typeid(double).name(), hlvm::ParameterMeaning::NONE }));
			hlvm::Function::addParameterInfoTo(&function0, hlvm::ParameterInfoItem({ "param3", typeid(int).name(), hlvm::ParameterMeaning::NONE }));

			hlvm::AssignVariableCommand assignCommand(hlvm::VariableReference("param0", &function0.get_parameters()), hlvm::VariableReference("local0", &function0.get_parameters()));
			hlvm::Function::addCommandTo(&function0, &assignCommand);

			Assert::IsFalse(function0.execute());

			hlvm::Function::addParameterTo(&function0, "param0", hlvm::Variable::from(0));
			hlvm::Function::addParameterTo(&function0, "param1", hlvm::Variable::from(0.0f));
			hlvm::Function::addParameterTo(&function0, "param2", hlvm::Variable::from(0.0));
			hlvm::Function::addParameterTo(&function0, "param3", hlvm::Variable::from(1));

			Assert::IsTrue(function0.execute());

			Assert::AreEqual(111, function0.get_parameters().getVariableByName("param0").getIntValue());
		}

		TEST_METHOD(FunctionTestMethod3)
		{
			hlvm::ExpressionTree *tree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *root = new hlvm::ExpressionNode();

			hlvm::ExpressionTree::addExpressionDataNode(root, { hlvm::Variable::from(10), hlvm::Variable::from(1) }, { hlvm::Operator::MINUS });
			tree->set_root(root);

			hlvm::Function function0;

			hlvm::Function::addParameterInfoTo(&function0, hlvm::ParameterInfoItem({ "param0", typeid(int).name(), hlvm::ParameterMeaning::NONE }));
			hlvm::Function::addParameterInfoTo(&function0, hlvm::ParameterInfoItem({ "param1", typeid(float).name(), hlvm::ParameterMeaning::NONE }));
			hlvm::Function::addParameterInfoTo(&function0, hlvm::ParameterInfoItem({ "param2", typeid(double).name(), hlvm::ParameterMeaning::NONE }));
			hlvm::Function::addParameterInfoTo(&function0, hlvm::ParameterInfoItem({ "param3", typeid(int).name(), hlvm::ParameterMeaning::NONE }));

			hlvm::AssignVariableCommand assignCommand(hlvm::VariableReference("param0", &function0.get_parameters()), tree);
			hlvm::Function::addCommandTo(&function0, &assignCommand);

			Assert::IsFalse(function0.execute());

			hlvm::Function::addParameterTo(&function0, "param0", hlvm::Variable::from(0));
			hlvm::Function::addParameterTo(&function0, "param1", hlvm::Variable::from(0.0f));
			hlvm::Function::addParameterTo(&function0, "param2", hlvm::Variable::from(0.0));
			hlvm::Function::addParameterTo(&function0, "param3", hlvm::Variable::from(1));

			Assert::IsTrue(function0.execute());

			Assert::AreEqual(9, function0.get_parameters().getVariableByName("param0").getIntValue());
		}

		TEST_METHOD(FunctionTestMethod4)
		{
			hlvm::ExpressionTree *tree0 = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *root0 = new hlvm::ExpressionNode();

			hlvm::ExpressionTree::addExpressionDataNode(root0, { hlvm::Variable::from(10), hlvm::Variable::from(1) }, { hlvm::Operator::MINUS });
			tree0->set_root(root0);

			hlvm::ExpressionTree *tree1 = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *root1 = new hlvm::ExpressionNode();

			hlvm::ExpressionTree::addExpressionDataNode(root1, { hlvm::Variable::from(10), hlvm::Variable::from(1) }, { hlvm::Operator::MINUS });
			tree1->set_root(root1);

			hlvm::Function function0;

			hlvm::Function::addParameterInfoTo(&function0, hlvm::ParameterInfoItem({ "param0", typeid(int).name(), hlvm::ParameterMeaning::NONE }));
			hlvm::Function::addParameterInfoTo(&function0, hlvm::ParameterInfoItem({ "param1", typeid(float).name(), hlvm::ParameterMeaning::NONE }));
			hlvm::Function::addParameterInfoTo(&function0, hlvm::ParameterInfoItem({ "param2", typeid(double).name(), hlvm::ParameterMeaning::NONE }));
			hlvm::Function::addParameterInfoTo(&function0, hlvm::ParameterInfoItem({ "param3", typeid(hlvm::ExpressionTree*).name(), hlvm::ParameterMeaning::NONE }));

			hlvm::AssignVariableCommand assignCommand0(hlvm::VariableReference("param0", &function0.get_parameters()), tree0);
			hlvm::Function::addCommandTo(&function0, &assignCommand0);

			hlvm::AssignVariableCommand assignCommand1(hlvm::VariableReference("param1", &function0.get_parameters()), hlvm::VariableReference("param3", &function0.get_parameters()));
			hlvm::Function::addCommandTo(&function0, &assignCommand1);

			Assert::IsFalse(function0.execute());

			hlvm::Function::addParameterTo(&function0, "param0", hlvm::Variable::from(0));
			hlvm::Function::addParameterTo(&function0, "param1", hlvm::Variable::from(0.0f));
			hlvm::Function::addParameterTo(&function0, "param2", hlvm::Variable::from(0.0));
			hlvm::Function::addParameterTo(&function0, "param3", hlvm::Variable::from(tree1));

			Assert::IsTrue(function0.execute());

			Assert::AreEqual(9, function0.get_parameters().getVariableByName("param0").getIntValue());
			Assert::AreEqual(9, function0.get_parameters().getVariableByName("param1").getIntValue());
		}
	};
}