#include "stdafx.h"
#include "CppUnitTest.h"
#include "variablemap.h"
#include "variableexception.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace tests
{
	TEST_CLASS(variablemaptest)
	{
	public:
		
		TEST_METHOD(VariableMapTestMethod)
		{
			hlvm::VariableMap map;

			// try to get variable when there is no one, must throw
			Assert::ExpectException<hlvm::VariableException>([&map] {map.getVariableByName("var0");});
			// add variable, must return true if there is no previous variable with same name
			Assert::IsTrue(map.addVariable("var0", hlvm::Variable::from(0)));
			// try to add variable with the same name, should fail and return false
			Assert::IsFalse(map.addVariable("var0", hlvm::Variable::from(0)));

			// try to get intValue from variable
			Assert::AreEqual(0, map.getVariableByName("var0").getIntValue());

			// remove variable, must return true if variable existed 
			Assert::IsTrue(map.removeVariable("var0"));
			// try to get variable, it is removed so this should throw
			Assert::ExpectException<hlvm::VariableException>([&map] {map.getVariableByName("var0");});
		}
	};
}