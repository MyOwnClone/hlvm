#include "stdafx.h"
#include "CppUnitTest.h"
#include "object.h"
#include "expressiontree.h"
#include "objectvariablereference.h"
#include "variablelist.h"
#include "conditiontree.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace tests
{
	TEST_CLASS(apitest)
	{
	public:
		
		TEST_METHOD(ApiTestMethod1)
		{
			// our base object
			hlvm::Object object;

			// created first, it should have id == 0
			Assert::IsTrue(object.get_id() >= 0);

			hlvm::ExpressionTree* tree0 = new hlvm::ExpressionTree();
			Assert::IsTrue(tree0->get_id() >= 0);

			hlvm::ExpressionTree::addExpressionDataNodeToRoot(tree0, { hlvm::Variable::from(1) }, {  });

			Assert::AreEqual(1, boost::any_cast<int>(tree0->execute()));
			// try to call once again
			Assert::AreEqual(1, boost::any_cast<int>(tree0->execute()));		

			// add variable to object, we will be refering to it
			object.addLocalVariable("var0", hlvm::Variable::from(1));
			// new tree, which will be refering to local variable of object
			hlvm::ExpressionTree* tree1 = new hlvm::ExpressionTree();
			hlvm::ExpressionTree::addExpressionDataNodeToRoot(tree1, { hlvm::ObjectVariableReference("var0", &object) }, {});

			Assert::AreEqual(1, boost::any_cast<int>(tree1->execute()));

			// pointer to list is added to local variables of object
			hlvm::VariableList list0;
			// list has one item with value 1
			list0.addVariableCopy(hlvm::Variable::from(1));
			// and second item with value 2
			list0.addVariableCopy(hlvm::Variable::from(2));
			// list's copy is added to locals
			object.addLocalVariable("arr0", hlvm::Variable::copyFrom(&list0));

			Assert::AreEqual(1, boost::any_cast<int>(object.getVariableReferenceByName("arr0").getVariableListPtr()->getByIndex(0).getIntValue()) );
			Assert::AreEqual(2, boost::any_cast<int>(object.getVariableReferenceByName("arr0").getVariableListPtr()->getByIndex(1).getIntValue()));

			hlvm::ExpressionTree* tree2 = new hlvm::ExpressionTree();
			hlvm::ExpressionTree::addExpressionDataNodeToRoot(tree2, { hlvm::ObjectVariableReference("arr0", &object, 1) }, {});

			Assert::AreEqual(2, boost::any_cast<int>(tree2->execute()));

			hlvm::ExpressionTree* tree3 = new hlvm::ExpressionTree();
			hlvm::ExpressionTree::addExpressionDataNodeToRoot(tree3, { hlvm::ObjectVariableReference("arr0", &object, 0), hlvm::ObjectVariableReference("arr0", &object, 1) }, { hlvm::Operator::PLUS });

			Assert::AreEqual(3, boost::any_cast<int>(tree3->execute()));
		}

		TEST_METHOD(ApiTestMethod2)
		{
			hlvm::Object object0;
			object0.addLocalVariable("var0", hlvm::Variable::from(1));
			hlvm::VariableList list0;
			list0.addVariableCopy(hlvm::Variable::from(1));
			list0.addVariableCopy(hlvm::Variable::from(2));
			object0.addLocalVariable("arr0", hlvm::Variable::copyFrom(&list0));

			hlvm::Object object1;
			object1.addLocalVariable("var0", hlvm::Variable::from(1));
			hlvm::VariableList list1;
			list1.addVariableCopy(hlvm::Variable::from(2));
			list1.addVariableCopy(hlvm::Variable::from(1));
			object1.addLocalVariable("arr0", hlvm::Variable::copyFrom(&list1));

			{
				hlvm::ConditionTree *conditionTree0 = new hlvm::ConditionTree();
				hlvm::ConditionTree::addRelationConditionWithAnyToRoot(conditionTree0, { hlvm::ObjectVariableReference("arr0", &object0, 0), hlvm::ObjectVariableReference("arr0", &object1, 0) }, { hlvm::RelationOperators::EQUAL });

				Assert::IsFalse(conditionTree0->execute());
			}

			{
				hlvm::ConditionTree *conditionTree0 = new hlvm::ConditionTree();
				hlvm::ConditionTree::addRelationConditionWithAnyToRoot(conditionTree0, { hlvm::ObjectVariableReference("arr0", &object0, 1), hlvm::ObjectVariableReference("arr0", &object1, 0) }, { hlvm::RelationOperators::EQUAL });

				Assert::IsTrue(conditionTree0->execute());
			}

			{
				hlvm::ConditionTree *conditionTree0 = new hlvm::ConditionTree();
				hlvm::ConditionTree::addRelationConditionWithAnyToRoot(conditionTree0, { hlvm::ObjectVariableReference("var0", &object0), hlvm::ObjectVariableReference("var0", &object1) }, { hlvm::RelationOperators::EQUAL });

				Assert::IsTrue(conditionTree0->execute());
			}

			{
				hlvm::ConditionTree *conditionTree0 = new hlvm::ConditionTree();
				hlvm::ConditionTree::addRelationConditionWithAnyToRoot(conditionTree0, { hlvm::ObjectVariableReference("arr0", &object0, 0), hlvm::ObjectVariableReference("arr0", &object1, 0) }, { hlvm::RelationOperators::LOWER });

				Assert::IsTrue(conditionTree0->execute());
			}
		}

		TEST_METHOD(ApiTestMethod3)
		{
			hlvm::Object object0;
			object0.addLocalVariable("var0", hlvm::Variable::from(1));

			hlvm::VariableList list0;
			// pointer to object0 is added to list
			list0.addVariableCopy(hlvm::Variable::from(&object0));

			Assert::AreEqual(1, list0.getByIndex(0).getObjectPtr()->getVariableReferenceByName("var0").getIntValue());

			// change value, this should be reflected in next call
			object0.getVariableReferenceByName("var0").value = 2;

			Assert::AreEqual(2, list0.getByIndex(0).getObjectPtr()->getVariableReferenceByName("var0").getIntValue());			
		}
	};
}