#include "stdafx.h"
#include "CppUnitTest.h"
#include "objectfactory.h"
#include "variable.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace tests
{
	TEST_CLASS(objectfactorytest)
	{
	public:
		
		TEST_METHOD(ObjectFactoryTestMethod1)
		{
			hlvm::Object obj = hlvm::ObjectFactory::getObject();

			Assert::IsTrue(obj.get_id() >= 0);

			hlvm::ObjectFactory::addVariable(obj, "objVariable0", hlvm::Variable::from(0));

			Assert::AreEqual(0, obj.getVariableReferenceByName("objVariable0").getIntValue());
		}

	};
}