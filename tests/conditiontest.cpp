#include "stdafx.h"
#include "CppUnitTest.h"
#include "conditiontree.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace tests
{
	TEST_CLASS(conditiontest)
	{
	public:
		
		TEST_METHOD(ConditionTestMethod1)
		{
			std::auto_ptr<hlvm::ConditionTree> tree(new hlvm::ConditionTree());

			tree->set_root(
				new hlvm::ConditionNode(
				new hlvm::RelationCondition(
			{
				hlvm::Variable::ptrFrom(5),
				hlvm::Variable::ptrFrom(5)
			},
			{
				hlvm::RelationOperators::LOWER_OR_EQUAL
			})
			));

			Assert::IsTrue(tree->execute());
		}

		TEST_METHOD(ConditionTestMethod2)
		{
			std::auto_ptr<hlvm::ConditionTree> tree(new hlvm::ConditionTree());

			tree->set_root(
				new hlvm::ConditionNode(
				new hlvm::RelationCondition(
			{
				hlvm::Variable::ptrFrom(4),
				hlvm::Variable::ptrFrom(5)
			},
			{
				hlvm::RelationOperators::EQUAL
			})
			));

			Assert::IsFalse(tree->execute());
		}

		TEST_METHOD(ConditionTestMethod3)
		{
			hlvm::VariableMap map;
			map.addVariable("var0", hlvm::Variable::from(4));
			map.addVariable("var1", hlvm::Variable::from(4));
			map.addVariable("var2", hlvm::Variable::from(5));

			std::auto_ptr<hlvm::ConditionTree> tree0(new hlvm::ConditionTree());

			tree0->set_root(
				new hlvm::ConditionNode(
				new hlvm::RelationCondition(
			{
				hlvm::Variable::ptrFrom(hlvm::VariableReference("var0", &map)),
				hlvm::Variable::ptrFrom(hlvm::VariableReference("var1", &map))
			},
			{
				hlvm::RelationOperators::EQUAL
			})
			));

			Assert::IsTrue(tree0->execute());

			std::auto_ptr<hlvm::ConditionTree> tree1(new hlvm::ConditionTree());

			tree1->set_root(
				new hlvm::ConditionNode(
				new hlvm::RelationCondition(
			{
				hlvm::Variable::ptrFrom(hlvm::VariableReference("var0", &map)),
				hlvm::Variable::ptrFrom(hlvm::VariableReference("var2", &map))
			},
			{
				hlvm::RelationOperators::EQUAL
			})
			));

			Assert::IsFalse(tree1->execute());
		}

		TEST_METHOD(ConditionTestMethod4)
		{
			hlvm::VariableMap map;
			map.addVariable("var0", hlvm::Variable::from(4));
			map.addVariable("var1", hlvm::Variable::from(4));
			map.addVariable("var2", hlvm::Variable::from(5));

			hlvm::ConditionTree* tree0 = new hlvm::ConditionTree();
			// is var0 equal to var1
			hlvm::RelationCondition* leftCondition = new hlvm::RelationCondition({ hlvm::Variable::ptrFrom(hlvm::VariableReference("var0", &map)), hlvm::Variable::ptrFrom(hlvm::VariableReference("var1", &map)) }, { hlvm::RelationOperators::EQUAL });
			// is var0 lower or equal to var2
			hlvm::RelationCondition* rightCondition = new hlvm::RelationCondition({ hlvm::Variable::ptrFrom(hlvm::VariableReference("var0", &map)), hlvm::Variable::ptrFrom(hlvm::VariableReference("var2", &map)) }, { hlvm::RelationOperators::LOWER_OR_EQUAL });

			hlvm::ConditionNode *left = new hlvm::ConditionNode(leftCondition);
			hlvm::ConditionNode *right = new hlvm::ConditionNode(rightCondition);

			// left and right subnodes are "connected" with AND clausule
			hlvm::ConditionNode *root = new hlvm::ConditionNode(new hlvm::LogicCondition(hlvm::LogicOperators::AND));
			root->get_children().push_back(left);
			root->get_children().push_back(right);

			tree0->set_root(root);

			Assert::IsTrue(tree0->execute());

			delete tree0;
		}

		TEST_METHOD(ConditionTestMethod5)
		{
			hlvm::VariableMap map;
			map.addVariable("var0", hlvm::Variable::from(4));
			map.addVariable("var1", hlvm::Variable::from(4));
			map.addVariable("var2", hlvm::Variable::from(5));

			hlvm::ConditionTree* tree0 = new hlvm::ConditionTree();
			hlvm::ConditionNode *left = new hlvm::ConditionNode();
			// is var0 equal to var1
			hlvm::ConditionTree::addRelationConditionTo(left, { hlvm::Variable::ptrFrom(hlvm::VariableReference("var0", &map)), hlvm::Variable::ptrFrom(hlvm::VariableReference("var1", &map)) }, { hlvm::RelationOperators::EQUAL });

			hlvm::ConditionNode *right = new hlvm::ConditionNode();
			// is var0 lower or equal to var2
			hlvm::ConditionTree::addRelationConditionTo(right, { hlvm::Variable::ptrFrom(hlvm::VariableReference("var0", &map)), hlvm::Variable::ptrFrom(hlvm::VariableReference("var2", &map)) }, { hlvm::RelationOperators::EQUAL });

			// left and right subnodes are "connected" with OR clausule
			hlvm::ConditionNode *root = new hlvm::ConditionNode();
			hlvm::ConditionTree::addLogicConditionTo(root, { left, right }, { hlvm::LogicOperators::AND });

			tree0->set_root(root);

			Assert::IsFalse(tree0->execute());

			delete tree0;
		}

		TEST_METHOD(ConditionTestMethod6)
		{
			hlvm::VariableMap map;
			map.addVariable("var0", hlvm::Variable::from(1));
			map.addVariable("var1", hlvm::Variable::from(2));
			map.addVariable("var2", hlvm::Variable::from(3));

			hlvm::ExpressionTree *tree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *root = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *left = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *right = new hlvm::ExpressionNode();

			hlvm::ExpressionNode *right0 = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *right1 = new hlvm::ExpressionNode();

			// 1 * 2
			hlvm::ExpressionTree::addExpressionDataNode(left, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::MULTIPLY });

			// 1 + 2
			hlvm::ExpressionTree::addExpressionDataNode(right0, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::PLUS });
			// 1 - 2
			hlvm::ExpressionTree::addExpressionDataNode(right1, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::MINUS });

			// (1+2)+(1-2) == 2
			hlvm::ExpressionTree::addExpressionProxyNode(right, { right0, right1 }, { hlvm::Operator::PLUS });

			// (1*2)+(5-3) == 2+2
			hlvm::ExpressionTree::addExpressionProxyNode(root, { left, right }, { hlvm::Operator::PLUS });

			tree->set_root(root);

			hlvm::ConditionTree* tree0 = new hlvm::ConditionTree();
			tree0->set_root(
				new hlvm::ConditionNode(
				new hlvm::RelationCondition(
			{
				hlvm::Variable::ptrFrom(tree),
				hlvm::Variable::ptrFrom(4)
			},
			{
				hlvm::RelationOperators::EQUAL
			})
			));

			Assert::IsTrue(tree0->execute());
		}
	};
}