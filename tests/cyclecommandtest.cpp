#include "stdafx.h"
#include "CppUnitTest.h"
#include "variablemap.h"
#include "expressionnode.h"
#include "expressiontree.h"
#include "conditiontree.h"
#include "assignexpressioncommand.h"
#include "conditionalcommand.h"
#include "cyclecommand.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace tests
{
	TEST_CLASS(cyclecommandtest)
	{
	public:
		
		TEST_METHOD(CycleCommandTestMethod)
		{
			// expression to be right side of condition
			hlvm::VariableMap map;
			map.addVariable("var0", hlvm::Variable::from(1));
			map.addVariable("var1", hlvm::Variable::from(2));
			map.addVariable("var2", hlvm::Variable::from(3));
			map.addVariable("result", hlvm::Variable::from(0));
			map.addVariable("loopControl", hlvm::Variable::from(0));

			hlvm::ExpressionTree *expressionTree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *expressionTreeRoot = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *expressionTreeLeftChild = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *expressionTreeRightChild = new hlvm::ExpressionNode();

			hlvm::ExpressionNode *rightChildLeftSubchild = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *rightChildRightSubchild = new hlvm::ExpressionNode();

			// 1 * 2
			hlvm::ExpressionTree::addExpressionDataNode(expressionTreeLeftChild, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::MULTIPLY });
			// 1 + 2
			hlvm::ExpressionTree::addExpressionDataNode(rightChildLeftSubchild, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::PLUS });
			// 1 - 2
			hlvm::ExpressionTree::addExpressionDataNode(rightChildRightSubchild, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::MINUS });

			// (1+2)+(1-2) == 2
			hlvm::ExpressionTree::addExpressionProxyNode(expressionTreeRightChild, { rightChildLeftSubchild, rightChildRightSubchild }, { hlvm::Operator::PLUS });

			// (1*2)+(5-3) == 2+2
			hlvm::ExpressionTree::addExpressionProxyNode(expressionTreeRoot, { expressionTreeLeftChild, expressionTreeRightChild }, { hlvm::Operator::PLUS });

			expressionTree->set_root(expressionTreeRoot);

			// condition is: loopControl <= 4
			hlvm::ConditionTree* conditionTree = new hlvm::ConditionTree();
			conditionTree->set_root(new hlvm::ConditionNode());

			hlvm::ConditionTree::addRelationConditionTo(conditionTree->get_root(), { hlvm::Variable::ptrFrom(hlvm::VariableReference("loopControl", &map)), hlvm::Variable::ptrFrom(expressionTree) }, { hlvm::RelationOperators::LOWER_OR_EQUAL });

			hlvm::Block innerBlock;

			hlvm::ExpressionTree *assignCommandExpressionTree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *assignCommandExpressionTreeRoot = new hlvm::ExpressionNode();

			hlvm::ExpressionTree::addExpressionDataNode(assignCommandExpressionTreeRoot, { hlvm::VariableReference("loopControl", &map), hlvm::Variable::from(1) }, { hlvm::Operator::PLUS });
			hlvm::AssignExpressionCommand *assignCommand = new hlvm::AssignExpressionCommand(hlvm::VariableReference("loopControl", &map), assignCommandExpressionTree);

			assignCommandExpressionTree->set_root(assignCommandExpressionTreeRoot);

			hlvm::Block::addCommandTo(&innerBlock, assignCommand);

			// loop until loopControl <= 4
			hlvm::CycleCommand *cycleCommand = new hlvm::CycleCommand(innerBlock, conditionTree);

			// so, latest value of loopControl is equal to 5
			cycleCommand->execute();

			Assert::AreEqual(5, map.getVariableByName("loopControl").getIntValue());

			delete expressionTree;
			delete conditionTree;
			delete assignCommandExpressionTree;
		}
	};
}