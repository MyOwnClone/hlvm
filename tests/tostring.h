#include "stdafx.h"
#include <string>

#include "variablemap.h"
#include "parameterinfo.h"

namespace Microsoft
{
	namespace VisualStudio
	{
		namespace CppUnitTestFramework
		{
			template<> static std::wstring ToString<hlvm::VariableMap>(hlvm::VariableMap *t)
			{
				return L"";
			}

			template<> static std::wstring ToString<hlvm::ParameterMeaning>(const hlvm::ParameterMeaning &t)
			{
				return L"";
			}
		}
	}
}