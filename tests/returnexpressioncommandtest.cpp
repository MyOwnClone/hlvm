#include "stdafx.h"
#include "CppUnitTest.h"
#include "returnexpressioncommand.h"
#include "assignexpressioncommand.h"
#include "conditionalcommand.h"
#include "variableexception.h"
#include "functionptrcallcommand.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace tests
{
	TEST_CLASS(returnexpressioncommandtest)
	{
	public:
		
		TEST_METHOD(ReturnExpressionCommandTestMethod1)
		{
			// creates function which takes it parameter, adds one to it and return this value

			hlvm::VariableMap globals;

			hlvm::FunctionPtrCallCommand *callCommand0 = new hlvm::FunctionPtrCallCommand();
			hlvm::Function calledFunction0;

			hlvm::Function::addParameterInfoTo(calledFunction0, hlvm::ParameterInfoItem({ "param0", typeid(int).name(), hlvm::ParameterMeaning::NONE }));
			calledFunction0.setReturnTypeString(typeid(int).name());

			// create default vmap for command
			callCommand0->set_variableMap(new hlvm::VariableMap());
			// make globals its parent so we can connect "param0" in functions parameters to global0 in globals
			callCommand0->get_variableMap()->set_parent(&globals);
			// functions own parameter variable map has parent set to commands variable map
			calledFunction0.get_parameters().set_parent(callCommand0->get_variableMap());
			// param0 is set to "point" to variable global0 which is stored in globals variablemap, so there is to be recursive search from child variable maps to parents
			callCommand0->get_variableMap()->addVariable("param0", hlvm::Variable::from(0));

			hlvm::ExpressionTree *etree0 = new hlvm::ExpressionTree();

			hlvm::ExpressionTree::addExpressionDataNodeToRoot(etree0, { hlvm::Variable::from(1), hlvm::VariableReference::none() }, { hlvm::Operator::PLUS });

			hlvm::AssignExpressionCommand *innerCommand = new hlvm::AssignExpressionCommand(hlvm::VariableReference("param0", &calledFunction0.get_parameters()), etree0);

			hlvm::ExpressionTree *etree1 = new hlvm::ExpressionTree();

			hlvm::ExpressionTree::addExpressionDataNodeToRoot(etree1, { hlvm::VariableReference("param0", &calledFunction0.get_parameters()), hlvm::VariableReference::none() }, { hlvm::Operator::PLUS });

			hlvm::ReturnExpressionCommand *returnCommand = new hlvm::ReturnExpressionCommand(etree1);

			hlvm::Function::addCommandTo(calledFunction0, innerCommand);
			hlvm::Function::addCommandTo(calledFunction0, returnCommand);

			callCommand0->setFunctionToCall(&calledFunction0);

			callCommand0->execute();

			Assert::AreEqual(1, boost::any_cast<int>(calledFunction0.get_returnValue()));
		}

		TEST_METHOD(ReturnExpressionCommandTestMethod2)
		{
			// creates function which takes it parameter, adds one to it and return this value

			hlvm::VariableMap globals;

			hlvm::FunctionPtrCallCommand *callCommand0 = new hlvm::FunctionPtrCallCommand();
			hlvm::Function calledFunction0;

			hlvm::Function::addParameterInfoTo(calledFunction0, hlvm::ParameterInfoItem({ "param0", typeid(int).name(), hlvm::ParameterMeaning::NONE }));
			calledFunction0.setReturnTypeString(typeid(double).name());

			// create default vmap for command
			callCommand0->set_variableMap(new hlvm::VariableMap());
			// make globals its parent so we can connect "param0" in functions parameters to global0 in globals
			callCommand0->get_variableMap()->set_parent(&globals);
			// functions own parameter variable map has parent set to commands variable map
			calledFunction0.get_parameters().set_parent(callCommand0->get_variableMap());
			// param0 is set to "point" to variable global0 which is stored in globals variablemap, so there is to be recursive search from child variable maps to parents
			callCommand0->get_variableMap()->addVariable("param0", hlvm::Variable::from(0));

			hlvm::ExpressionTree *etree0 = new hlvm::ExpressionTree();

			hlvm::ExpressionTree::addExpressionDataNodeToRoot(etree0, { hlvm::Variable::from(1), hlvm::VariableReference::none() }, { hlvm::Operator::PLUS });

			hlvm::AssignExpressionCommand *innerCommand = new hlvm::AssignExpressionCommand(hlvm::VariableReference("param0", &calledFunction0.get_parameters()), etree0);

			hlvm::ExpressionTree *etree1 = new hlvm::ExpressionTree();

			hlvm::ExpressionTree::addExpressionDataNodeToRoot(etree1, { hlvm::VariableReference("param0", &calledFunction0.get_parameters()), hlvm::VariableReference::none() }, { hlvm::Operator::PLUS });

			hlvm::ReturnExpressionCommand *returnCommand = new hlvm::ReturnExpressionCommand(etree1);

			hlvm::Function::addCommandTo(calledFunction0, innerCommand);
			hlvm::Function::addCommandTo(calledFunction0, returnCommand);

			callCommand0->setFunctionToCall(&calledFunction0);

			Assert::ExpectException<hlvm::VariableException>([&]{callCommand0->execute();});
		}

		TEST_METHOD(ReturnExpressionCommandTestMethod3)
		{
			hlvm::ExpressionTree *simpleTree = new hlvm::ExpressionTree();

			hlvm::ExpressionTree::addExpressionDataNodeToRoot(simpleTree, { hlvm::Variable::from(1), hlvm::VariableReference::none() }, { hlvm::Operator::PLUS });
			hlvm::Function simpleFunction;
			simpleFunction.get_returnValueInfo().typeName = typeid(int).name();
			hlvm::ReturnExpressionCommand *simpleFunctionReturnCommand = new hlvm::ReturnExpressionCommand(simpleTree);
			hlvm::Function::addCommandTo(simpleFunction, simpleFunctionReturnCommand);

			hlvm::FunctionPtrCallCommand *simpleCallCommand = new hlvm::FunctionPtrCallCommand();
			simpleCallCommand->setFunctionToCall(&simpleFunction);

			simpleCallCommand->execute();

			Assert::AreEqual(1, boost::any_cast<int>(simpleFunction.get_returnValue()));
		}

		TEST_METHOD(ReturnExpressionCommandTestMethod4)
		{
			hlvm::ExpressionTree *simpleTree = new hlvm::ExpressionTree();

			hlvm::ExpressionTree::addExpressionDataNodeToRoot(simpleTree, { hlvm::Variable::from(1), hlvm::VariableReference::none() }, { hlvm::Operator::PLUS });
			hlvm::Function simpleFunction;
			simpleFunction.setReturnTypeString(typeid(int).name());
			hlvm::ReturnExpressionCommand *simpleFunctionReturnCommand = new hlvm::ReturnExpressionCommand(simpleTree);
			hlvm::Function::addCommandTo(simpleFunction, simpleFunctionReturnCommand);

			hlvm::FunctionPtrCallCommand *simpleCallCommand = new hlvm::FunctionPtrCallCommand();
			simpleCallCommand->set_function(&simpleFunction);

			simpleCallCommand->execute();

			Assert::AreEqual(1, boost::any_cast<int>(simpleFunction.get_returnValue()));

			// creates function which takes it parameter, adds one to it and return this value

			hlvm::VariableMap globals;

			hlvm::FunctionPtrCallCommand *callCommand0 = new hlvm::FunctionPtrCallCommand();
			hlvm::Function calledFunction0;

			hlvm::Function::addParameterInfoTo(calledFunction0, hlvm::ParameterInfoItem({ "param0", typeid(int).name(), hlvm::ParameterMeaning::NONE }));
			calledFunction0.setReturnTypeString(typeid(int).name());

			// create default vmap for command
			callCommand0->set_variableMap(new hlvm::VariableMap());
			// make globals its parent so we can connect "param0" in functions parameters to global0 in globals
			callCommand0->get_variableMap()->set_parent(&globals);
			// functions own parameter variable map has parent set to commands variable map
			calledFunction0.get_parameters().set_parent(callCommand0->get_variableMap());
			// param0 is set to "point" to variable global0 which is stored in globals variablemap, so there is to be recursive search from child variable maps to parents
			callCommand0->get_variableMap()->addVariable("param0", hlvm::Variable::from(0));

			hlvm::ExpressionTree *etree0 = new hlvm::ExpressionTree();

			hlvm::ExpressionTree::addExpressionDataNodeToRoot(etree0, { simpleCallCommand, hlvm::VariableReference::none() }, { hlvm::Operator::PLUS });

			hlvm::AssignExpressionCommand *innerCommand = new hlvm::AssignExpressionCommand(hlvm::VariableReference("param0", &calledFunction0.get_parameters()), etree0);

			hlvm::ExpressionTree *etree1 = new hlvm::ExpressionTree();
			hlvm::ExpressionTree::addExpressionDataNodeToRoot(etree1, { hlvm::VariableReference("param0", &calledFunction0.get_parameters()), hlvm::VariableReference::none() }, { hlvm::Operator::PLUS });

			hlvm::ReturnExpressionCommand *returnCommand = new hlvm::ReturnExpressionCommand(etree1);

			hlvm::Function::addCommandTo(calledFunction0, innerCommand);
			hlvm::Function::addCommandTo(calledFunction0, returnCommand);

			callCommand0->setFunctionToCall(&calledFunction0); 
			
			callCommand0->execute();

			Assert::AreEqual(1, boost::any_cast<int>(calledFunction0.get_returnValue()));
		}

		TEST_METHOD(ReturnExpressionCommandTestMethod5)
		{
			// create expression tree
			hlvm::ExpressionTree *expressionTree = new hlvm::ExpressionTree();

			// add data directly to root
			// expression is basically to add 1 to none, equals to 1 + none, 1 + 0 = 0
			hlvm::ExpressionTree::addExpressionDataNodeToRoot(expressionTree, { hlvm::Variable::from(1), hlvm::VariableReference::none() }, { hlvm::Operator::PLUS });

			// return command for our function, it returns expression's result
			hlvm::ReturnExpressionCommand *simpleFunctionReturnCommand = new hlvm::ReturnExpressionCommand(expressionTree);

			// function which contains return command
			hlvm::Function function;
			// set return type
			function.setReturnTypeString(typeid(int).name());
			// add return command to function
			hlvm::Function::addCommandTo(function, simpleFunctionReturnCommand);

			function.execute();

			Assert::AreEqual(1, boost::any_cast<int>(function.get_returnValue()));
		}

		TEST_METHOD(ReturnExpressionCommandTestMethod6)
		{
			// create expression tree
			hlvm::ExpressionTree *expressionTree = new hlvm::ExpressionTree();

			// add data directly to root
			// expression is basically to add none to none, equals to none + none, 0 + 0 = 0
			hlvm::ExpressionTree::addExpressionDataNodeToRoot(expressionTree, { hlvm::VariableReference::none(), hlvm::VariableReference::none() }, { hlvm::Operator::PLUS });

			// return command for our function, it returns expression's result
			hlvm::ReturnExpressionCommand *simpleFunctionReturnCommand = new hlvm::ReturnExpressionCommand(expressionTree);

			// function which contains return command
			hlvm::Function function;
			// set return type
			function.setReturnTypeString(typeid(int).name());
			// add return command to function
			hlvm::Function::addCommandTo(function, simpleFunctionReturnCommand);

			function.execute();

			Assert::AreEqual(0, boost::any_cast<int>(function.get_returnValue()));
		}

		TEST_METHOD(ReturnExpressionCommandTestMethod7)
		{
			// create expression tree
			hlvm::ExpressionTree *expressionTree = new hlvm::ExpressionTree();

			// add data directly to root
			// expression is basically to add none to 1, equals to none + 1, 0 + 1 = 0
			hlvm::ExpressionTree::addExpressionDataNodeToRoot(expressionTree, { hlvm::VariableReference::none(), hlvm::Variable::from(1) }, { hlvm::Operator::PLUS });

			// return command for our function, it returns expression's result
			hlvm::ReturnExpressionCommand *simpleFunctionReturnCommand = new hlvm::ReturnExpressionCommand(expressionTree);

			// function which contains return command
			hlvm::Function function;
			// set return type
			function.setReturnTypeString(typeid(int).name());
			// add return command to function
			hlvm::Function::addCommandTo(function, simpleFunctionReturnCommand);

			function.execute();

			Assert::AreEqual(1, boost::any_cast<int>(function.get_returnValue()));
		}

		TEST_METHOD(ReturnExpressionCommandTestMethod8)
		{
			//------prepare inner function------
			// create expression tree
			hlvm::ExpressionTree *innerFunctionExpressionTree = new hlvm::ExpressionTree();

			// add data directly to root
			// expression is basically to add none to 1, equals to none + 1, 0 + 1 = 0
			hlvm::ExpressionTree::addExpressionDataNodeToRoot(innerFunctionExpressionTree, { hlvm::VariableReference::none(), hlvm::Variable::from(1) }, { hlvm::Operator::PLUS });
			// return command for our function, it returns expression's result
			hlvm::ReturnExpressionCommand *innerFunctionReturnCommand = new hlvm::ReturnExpressionCommand(innerFunctionExpressionTree);

			// function which contains return command
			hlvm::Function* innerFunction = new hlvm::Function();
			// set return type
			innerFunction->setReturnTypeString(typeid(int).name());
			// add return command to function
			hlvm::Function::addCommandTo(innerFunction, innerFunctionReturnCommand);
			//--------------------------------
			// call command to call inner function
			hlvm::FunctionPtrCallCommand *innerFunctionCallCommand = new hlvm::FunctionPtrCallCommand();
			innerFunctionCallCommand->setFunctionToCall(innerFunction);

			Assert::AreEqual(typeid(int).name(), innerFunctionCallCommand->getFunctionToCall()->getReturnValueInfoCopy().typeName.c_str());
			Assert::IsTrue(innerFunctionCallCommand->getFunctionToCall()->get_returnValue().empty());

			innerFunctionCallCommand->execute();
			Assert::IsFalse(innerFunctionCallCommand->getFunctionToCall()->get_returnValue().empty());
			Assert::AreEqual(1, boost::any_cast<int>(innerFunctionCallCommand->getFunctionToCall()->get_returnValue()));
		}
	};
}