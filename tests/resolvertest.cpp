#include "stdafx.h"
#include "CppUnitTest.h"
#include "object.h"
#include "expressiontree.h"
#include "objectvariablereference.h"
#include "variablelist.h"
#include "conditiontree.h"
#include "resolver.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace tests
{
	TEST_CLASS(resolvertest)
	{
	public:
		
		TEST_METHOD(ResolverTestMethod1)
		{
			hlvm::Object object0;
			object0.addLocalVariable("var0", hlvm::Variable::from(1));

			hlvm::VariableList list0;
			// pointer to object0 is added to list
			list0.addVariableCopy(hlvm::Variable::from(&object0));

			Assert::AreEqual(1, list0.getByIndex(0).getObjectPtr()->getVariableReferenceByName("var0").getIntValue());

			hlvm::VariableListReference ref0(0, &list0);

			Assert::AreEqual(1, hlvm::Resolver::resolve(ref0)->getObjectPtr()->getVariableReferenceByName("var0").getIntValue() );

			// reference to 0th item in list0, we know that it is object, so we pass name of variable in this object
			hlvm::VariableListReference ref1(0, &list0, "var0");

			Assert::AreEqual(1, hlvm::Resolver::resolve(ref1)->getIntValue());

			list0.getByIndex(0).getObjectPtr()->getVariableReferenceByName("var0").value = 2;

			Assert::AreEqual(2, hlvm::Resolver::resolve(ref1)->getIntValue());

			list0.getByIndex(0).getObjectPtr()->getVariableReferenceByName("var0").value = 1;

			object0.addLocalVariable("var1", hlvm::Variable::from(2));

			hlvm::VariableListReference ref2(0, &list0, "var1");

			Assert::AreEqual(2, hlvm::Resolver::resolve(ref2)->getIntValue());

			hlvm::ConditionTree* conditionTree0 = new hlvm::ConditionTree();
			hlvm::ConditionTree::addRelationConditionWithAnyToRoot(conditionTree0, { hlvm::VariableListReference(0, &list0, "var0"), hlvm::VariableListReference(0, &list0, "var1") }, { hlvm::RelationOperators::LOWER });

			Assert::IsTrue(conditionTree0->execute());

			hlvm::ExpressionTree* expressionTree0 = new hlvm::ExpressionTree();
			hlvm::ExpressionTree::addExpressionDataNodeToRoot(expressionTree0, { hlvm::VariableListReference(0, &list0, "var0"), hlvm::VariableListReference(0, &list0, "var1") }, { hlvm::Operator::PLUS });
			
			// basically VariableListReference is used when there is list needed to resolve first and ObjectVariableReference where there is object needed to resolve first
			Assert::AreEqual(3, boost::any_cast<int>(expressionTree0->execute()));
		}

		TEST_METHOD(ResolverTestMethod2)
		{
			hlvm::VariableList list0;
			hlvm::VariableList list1;
			list1.addVariableCopy(hlvm::Variable::from(1));
			list1.addVariableCopy(hlvm::Variable::from(2));
			list1.addVariableCopy(hlvm::Variable::from(3));

			list0.addVariableCopy(hlvm::Variable::from(&list1));

			Assert::AreEqual(1, list0.getByIndex(0).getVariableListPtr()->getByIndex(0).getIntValue());
			Assert::AreEqual(2, list0.getByIndex(0).getVariableListPtr()->getByIndex(1).getIntValue());
			Assert::AreEqual(3, list0.getByIndex(0).getVariableListPtr()->getByIndex(2).getIntValue());

			hlvm::VariableListReference ref0(0, &list0);

			auto innerList = hlvm::Resolver::resolve(ref0);

			Assert::AreEqual(1, innerList->getVariableListPtr()->getByIndex(0).getIntValue());
			Assert::AreEqual(2, innerList->getVariableListPtr()->getByIndex(1).getIntValue());
			Assert::AreEqual(3, innerList->getVariableListPtr()->getByIndex(2).getIntValue());

			auto item = hlvm::Resolver::resolve(hlvm::VariableListReference(0, innerList->getVariableListPtr()));

			Assert::AreEqual(1, item->getIntValue());

			item = hlvm::Resolver::resolve(hlvm::VariableListReference(1, innerList->getVariableListPtr()));

			Assert::AreEqual(2, item->getIntValue());

			item = hlvm::Resolver::resolve(hlvm::VariableListReference(2, innerList->getVariableListPtr()));

			Assert::AreEqual(3, item->getIntValue());
		}
	};
}