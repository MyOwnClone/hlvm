#include "stdafx.h"
#include "CppUnitTest.h"
#include "variablemap.h"
#include "expressiontree.h"
#include "conditiontree.h"
#include "conditionalcommand.h"
#include "assignexpressioncommand.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace tests
{
	TEST_CLASS(conditionalcommandtest)
	{
	public:
		
		TEST_METHOD(ConditionalCommandTestMethod)
		{
			// expression to be right side of condition
			hlvm::VariableMap map;
			map.addVariable("var0", hlvm::Variable::from(1));
			map.addVariable("var1", hlvm::Variable::from(2));
			map.addVariable("var2", hlvm::Variable::from(3));
			map.addVariable("result", hlvm::Variable::from(0));

			hlvm::ExpressionTree *tree = new hlvm::ExpressionTree();
			hlvm::ExpressionNode *root = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *left = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *right = new hlvm::ExpressionNode();

			hlvm::ExpressionNode *right0 = new hlvm::ExpressionNode();
			hlvm::ExpressionNode *right1 = new hlvm::ExpressionNode();

			// 1 * 2
			hlvm::ExpressionTree::addExpressionDataNode(left, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::MULTIPLY });
			// 1 + 2
			hlvm::ExpressionTree::addExpressionDataNode(right0, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::PLUS });
			// 1 - 2
			hlvm::ExpressionTree::addExpressionDataNode(right1, { hlvm::VariableReference("var0", &map), hlvm::VariableReference("var1", &map) }, { hlvm::Operator::MINUS });

			// (1+2)+(1-2) == 2
			hlvm::ExpressionTree::addExpressionProxyNode(right, { right0, right1 }, { hlvm::Operator::PLUS });

			// (1*2)+(5-3) == 2+2
			hlvm::ExpressionTree::addExpressionProxyNode(root, { left, right }, { hlvm::Operator::PLUS });

			tree->set_root(root);

			hlvm::ConditionTree* tree0 = new hlvm::ConditionTree();
			tree0->set_root(new hlvm::ConditionNode());

			hlvm::ConditionTree::addRelationConditionTo(tree0->get_root(), { hlvm::Variable::ptrFrom(tree), hlvm::Variable::ptrFrom(4) }, { hlvm::RelationOperators::EQUAL });

			hlvm::Block block;
			hlvm::Command* command0 = new hlvm::AssignExpressionCommand(hlvm::VariableReference("result", &map), tree);
			hlvm::Block::addCommandTo(&block, command0);
			hlvm::ConditionalCommand *command1 = new hlvm::ConditionalCommand(block, tree0);

			command1->execute();

			Assert::AreEqual(4, map.getVariableByName("result").getIntValue());
		}
	};
}