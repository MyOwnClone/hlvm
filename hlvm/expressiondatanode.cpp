#include "expressiondatanode.h"
#include "variablemap.h"
#include "resolver.h"

namespace hlvm
{
	boost::any ExpressionDataNode::execute(std::vector<TreeNode<Expression*>*> children, std::string highestPriorityTypeName)
	{
		boost::any result;

		if (highestPriorityTypeName == typeid(double).name())
		{
			result = (double)0;
		}
		if (highestPriorityTypeName == typeid(float).name())
		{
			result = (float)0;
		}
		else if (highestPriorityTypeName == typeid(int).name())
		{
			result = (int)0;
		}
		else if (highestPriorityTypeName == typeid(std::string).name())
		{
			result = std::string();
		}

		if (data.size() > 0)
		{
			if (data.size() != (operators.size() + 1))
			{
				throw ExpressionException("Number of operators should be equal to number of data minus 1.");
			}

			if (data.size() == 1)
			{
				auto tmpResult = Resolver::resolveVariableReferenceAndExecuteToPtr(data[0]);
				result = tmpResult->value;
			}
			else
			{
				for (auto i = 0; i < (int)data.size(); i += 2)
				{
					int operatorIndex = i / 2;

					boost::any value0 = data[i];
					boost::any value1 = data[i + 1];

					auto tmpValue0 = Resolver::resolveVariableReferenceAndExecuteToPtr(value0);
					auto tmpValue1 = Resolver::resolveVariableReferenceAndExecuteToPtr(value1);

					bool value0missing = false;
					bool value1missing = false;

					if (tmpValue0)
					{
						value0 = tmpValue0->value;
					}
					else
					{
						//case of VariableReference::none
						value0missing = true;
					}

					if (tmpValue1)
					{
						value1 = tmpValue1->value;
					}
					else
					{
						//case of VariableReference::none
						value1missing = true;
					}

					if (value0missing && !value1missing)
					{
						result = value1;
						break;
					}
					else if (!value0missing && value1missing)
					{
						result = value0;
						break;
					}
					else if (value0missing && value1missing)
					{
						break;
					}

					if (TypePriorities::isLowerPriority(value0.type().name(), value1.type().name()))
					{
						//higher priority type is double
						if (value1.type().name() == typeid(double).name())
						{
							if (value0.type().name() == typeid(int).name())
							{
								result = executeInner((double)(boost::any_cast<int>(value0)), boost::any_cast<double>(value1), operatorIndex, result);
							}
						}
						//higher priority type is float
						if (value1.type().name() == typeid(float).name())
						{
							if (value0.type().name() == typeid(int).name())
							{
								result = executeInner((float)(boost::any_cast<int>(value0)), boost::any_cast<float>(value1), operatorIndex, result);
							}
						}
					}
					else
					{
						// same priority						
						result = executeInner(value0, value1, operatorIndex, result);
					}

					if (tmpValue0->get_dispose())
					{
						delete tmpValue0;
					}
					if (tmpValue1->get_dispose())
					{
						delete tmpValue1;
					}
				}
			}
		}

		return result;
	}

	ExpressionDataNode* ExpressionDataNode::copy()
	{
		auto result = new ExpressionDataNode();

		result->operators = operators;
		result->data = data;

		return result;
	}

	ExpressionDataNode::~ExpressionDataNode()
	{

	}
}