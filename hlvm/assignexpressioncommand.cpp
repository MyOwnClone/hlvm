#include "assignexpressioncommand.h"
#include "resolver.h"

namespace hlvm
{
	bool AssignExpressionCommand::execute()
	{
		Variable *realVariableTarget = NULL;

		realVariableTarget = Resolver::resolveVariableReferenceAndExecuteToPtr(target);

		if (!realVariableTarget)
		{
			throw CommandException("Target is not VariableReference neither Variable");
		}

		realVariableTarget->value = source->execute();

		if (realVariableTarget->get_dispose())
		{
			delete realVariableTarget;
		}

		return true;
	}

	AssignExpressionCommand* AssignExpressionCommand::copy()
	{
		AssignExpressionCommand* result = new AssignExpressionCommand(target, source->copy());
		return result;
	}

	AssignExpressionCommand* AssignExpressionCommand::deepCopy()
	{
		AssignExpressionCommand* result = new AssignExpressionCommand(target, source->copy());

		if (variableMap)
		{
			result->variableMap = variableMap->deepCopy();
		}

		return result;
	}
}