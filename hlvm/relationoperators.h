#pragma once

namespace hlvm
{
	enum class RelationOperators
	{
		LOWER,
		GREATER,
		LOWER_OR_EQUAL,
		GREATER_OR_EQUAL,
		EQUAL,
		NON_EQUAL
	};
}