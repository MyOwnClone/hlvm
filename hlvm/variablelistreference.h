#pragma once

#include <string>
#include "main.h"

namespace hlvm
{
	class VariableList;
	class VariableMap;
	class VariableListReference
	{
	protected:
		int index;
		VariableList* list;
		VariableMap* map; 
		std::string name;
	public:
		VariableListReference() 
		{
			index = -1;
			list = NULL;
			map = NULL;
		}

		VariableListReference(int index, VariableList* list) : index(index), list(list)
		{

		}

		VariableListReference(int index, VariableList* list, VariableMap* map, std::string name) : index(index), list(list), map(map), name(name)
		{

		}

		VariableListReference(int index, VariableList* list, std::string name) : index(index), list(list), map(NULL), name(name)
		{

		}

		VariableListReference(int index, VariableMap* map, std::string name) : index(index), map(map), name(name)
		{
			list = NULL;
		}

		def_get_set(std::string, name);
		def_get_set(VariableList*, list);
		def_get_set(VariableMap*, map);
		def_get_set(int, index);
	};
}