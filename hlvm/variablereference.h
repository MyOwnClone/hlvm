#pragma once

#include <string>

//#include "variablemap.h"
//#include "variable.h"
#include "main.h"

namespace hlvm
{
	class VariableMap;

	class VariableReference
	{
	private:
		bool empty;
		std::string name;
		VariableMap *map = NULL;
	public:
		VariableReference()
		{
			this->empty = false;
		}

		VariableReference(std::string name, VariableMap *map) : name(name), map(map), empty(false)
		{

		}

		def_get_set(bool, empty);
		def_get_set(std::string, name);
		def_get_set(VariableMap *, map);

		static VariableReference none()
		{
			VariableReference result;
			result.set_empty(true);

			return result;
		};
	};
}