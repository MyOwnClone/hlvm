#pragma once
#include "main.h"

namespace hlvm
{
	class Disposable
	{
	protected:
		bool dispose;
	public:
		Disposable(bool value = false) 
		{
			dispose = value;
		}

		def_get_set(bool, dispose);
	};
}