#pragma once

#include "condition.h"
#include "logicoperators.h"

namespace hlvm
{
	class LogicCondition : public Condition
	{
	public:
		std::vector<LogicOperators> operators;

		LogicCondition(std::vector<LogicOperators> operators) : operators(operators)
		{

		}

		LogicCondition(LogicOperators operatorInst)
		{
			operators.push_back(operatorInst);
		}

		LogicCondition()
		{

		}

		LogicCondition(const LogicCondition& orig)
		{
			this->operators = orig.operators;
		}

		virtual bool execute(std::vector<TreeNode<Condition*>*> children);
		virtual LogicCondition* copy();
		virtual ~LogicCondition();
	};
}