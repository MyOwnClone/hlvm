#include "block.h"

namespace hlvm
{
	bool Block::execute()
	{
		for (auto i = 0; i < (int)commands.size(); i++)
		{
			if (!commands[i]->execute())
			{
				return false;
			}
		}

		return true;
	}

	Block *Block::copy()
	{
		Block *result = new Block();

		result->commands = commands;

		return result;
	}

	void Block::addCommand(Command* command)
	{
		commands.push_back(command);
	}

	void Block::addCommandTo(Block *block, Command* command)
	{
		block->addCommand(command);
	}

	Block* Block::deepCopy()
	{
		Block * result = new Block();

		for (auto i = 0; i < (int)commands.size(); i++)
		{
			result->addCommand(commands[i]->deepCopy());
		}

		return result;
	}

	int Block::counter = 0;
}