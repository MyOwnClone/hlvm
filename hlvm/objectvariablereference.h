#include <string>

#include "main.h"

namespace hlvm
{
	class Object;
	class ObjectVariableReference
	{
	protected:
		Object* object;
		std::string name;
		int index;
	public:
		ObjectVariableReference()
		{
			object = NULL;
			index = -1;
		}

		def_get_set(int, index);
		def_get_set(Object*, object);
		def_get_set(std::string, name);

		ObjectVariableReference(std::string name, Object* object, int index = -1) : name(name), object(object), index(index)
		{

		}
	};
}