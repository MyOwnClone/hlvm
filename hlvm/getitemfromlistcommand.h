#pragma once

#include "boost/any.hpp"
#include "command.h"

namespace hlvm
{
	class VariableList;
	class GetItemFromListCommand : public Command
	{
	private:
		boost::any target;
		boost::any list;
		int index;
	public:
		GetItemFromListCommand()
		{
			list = NULL;
			index = -1;
		}

		GetItemFromListCommand(boost::any target, boost::any list, int index) : target(target), list(list), index(index), Command()
		{

		};

		virtual bool execute();
		virtual GetItemFromListCommand* copy();
		virtual GetItemFromListCommand* deepCopy();
	};
}