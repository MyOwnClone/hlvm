#pragma once

#include "block.h"
#include "command.h"
#include "conditiontree.h"

namespace hlvm
{
	class CycleCommand : public Command
	{
	protected:
		Block block;
		ConditionTree *conditionTree;
	public:
		CycleCommand(Block &block, ConditionTree *conditionTree) :block(block), conditionTree(conditionTree) {}

		virtual bool execute();
		virtual CycleCommand* copy();
	};
}