#pragma once

#include "boost/any.hpp"
#include "command.h"
#include "expressiontree.h"
#include "commandexception.h"

namespace hlvm
{
	class DivideExpressionCommand : public Command
	{
	private:
		boost::any target;
		ExpressionTree* source;
	public:
		DivideExpressionCommand(boost::any target, ExpressionTree* source) : target(target), source(source), Command()
		{

		};

		virtual bool execute();
		virtual DivideExpressionCommand* copy();
		virtual DivideExpressionCommand* deepCopy();
	};
}