#pragma once

#include <exception>

namespace hlvm
{
	class VariableException : public std::runtime_error
	{
	public:
		VariableException() : std::runtime_error("uknown VariableException") {}
		VariableException(std::string message) : std::runtime_error(message) {}
	};
}