#pragma once
#include <unordered_map>
#include <string>
#include <vector>

#include "variable.h"
#include "main.h"

namespace hlvm
{
	class VariableMap
	{
	private:
		std::unordered_map<std::string, Variable> map;
		VariableMap* parent = NULL;
	public:
		// iterators
		typedef std::unordered_map<std::string, Variable>::iterator iterator;
		typedef std::unordered_map<std::string, Variable>::const_iterator const_iterator;

		iterator begin() { return map.begin(); }
		const_iterator begin() const { return map.cbegin(); }
		iterator end() { return map.end(); }
		const_iterator end() const { return map.cend(); }
		// /iterators

		// public API
		bool addVariable(std::string name, Variable variable);
		bool removeVariable(std::string name);
		Variable& getVariableByName(std::string name, bool lookInParents = true);
		bool variableExists(std::string name, bool lookInParents = true);
		Variable* resolveVariableByName(std::string name, bool lookInParents = true);

		std::vector<std::string> keysToVector();
		std::vector<boost::any> valuesToVector();

		def_get_set(VariableMap*, parent);

		VariableMap* deepCopy();
	};
}