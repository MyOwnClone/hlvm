#include "expressiontree.h"
#include "variablemap.h"
#include "variablelist.h"
#include "functionptrcallcommand.h"
#include "objectvariablereference.h"
#include "object.h"

namespace hlvm
{
	boost::any ExpressionTree::execute()
	{
		if (!get_root())
		{
			throw ExpressionException("Root node of ExpressionTree is NULL!!!");
		}

		auto node = get_root()->get_value();
		auto children = get_root()->get_children();
		return node->execute(children, getHighestPriorityTypeName());
	}

	ExpressionTree::~ExpressionTree()
	{
		traverseNodes([](hlvm::TreeNode<Expression*> *node)
		{
			delete node->get_value();
			node->set_value(NULL);
		});
	}

	ExpressionTree* ExpressionTree::copy()
	{
		return (ExpressionTree*)(Tree::copy());
	}

	void ExpressionTree::addExpressionDataNodeToRoot(ExpressionTree *tree, std::vector<boost::any> data, std::vector<Operator> operators)
	{
		hlvm::ExpressionNode *root = new hlvm::ExpressionNode();
		tree->set_root(root);

		addExpressionDataNode(root, data, operators);
	}

	void ExpressionTree::addExpressionDataNode(ExpressionNode *node, std::vector<boost::any> data, std::vector<Operator> operators)
	{
		auto result = new ExpressionDataNode();
		result->set_data(data);
		result->set_operators(operators);
		node->set_value(result);
	}

	void ExpressionTree::addExpressionProxyNode(ExpressionNode *node, std::vector<ExpressionNode*> children, std::vector<Operator> operators)
	{
		auto result = new ExpressionProxyNode();
		result->set_operators(operators);
		node->set_value(result);

		for (auto i = 0; i < (int)children.size(); i++)
		{
			node->get_children().push_back(children[i]);
		}
	}

	void ExpressionTree::addNode(ExpressionNode *node, std::vector<boost::any> data, std::vector<Operator> operators)
	{
		addExpressionDataNode(node, data, operators);
	}

	void ExpressionTree::addNode(ExpressionNode *node, std::vector<ExpressionNode*> children, std::vector<Operator> operators)
	{
		addExpressionProxyNode(node, children, operators);
	}

	const char* ExpressionTree::processVariableListReference(boost::any data)
	{
		const char* type;
		auto ref0 = boost::any_cast<VariableListReference>(data);

		auto list = ref0.get_list();

		if (list)
		{
			auto index = ref0.get_index();

			auto item = list->getByIndex(index);

			type = item.value.type().name();

			if (type == typeid(VariableListReference).name())
			{
				return processVariableListReference(item.value);
			}
		}
		else
		{
			auto index = ref0.get_index();
			auto map = ref0.get_map();
			auto name = ref0.get_name();

			auto value = map->getVariableByName(name);

			type = value.value.type().name();

			if (type == typeid(VariableList).name())
			{
				auto ref1 = boost::any_cast<VariableList>(value.value);

				auto value1 = ref1.getByIndex(index);

				type = value1.value.type().name();
			}
			else if (type == typeid(VariableList*).name())
			{
				auto ref1 = boost::any_cast<VariableList*>(value.value);

				auto value1 = ref1->getByIndex(index);

				type = value1.value.type().name();
			}
		}

		return type;
	}

	std::string ExpressionTree::getHighestPriorityTypeName(TreeNode<Expression*> *node)
	{
		std::string result = typeid(int).name();

		if (!node)
		{
			node = get_root();
		}

		auto realNode = node->get_value();

		if (typeid(*realNode).name() == typeid(ExpressionDataNode).name())
		{
			auto data = ((ExpressionDataNode*)realNode)->get_data();

			if (data.size() > 0)
			{
				for (auto i = 0; i < (int)data.size(); i++)
				{
					auto type = data[i].type().name();

					if (type == typeid(VariableReference).name())
					{
						auto ref0 = boost::any_cast<VariableReference>(data[i]);

						if (ref0.get_empty())
						{
							continue;
						}
						else
						{
							type = ref0.get_map()->getVariableByName(ref0.get_name()).value.type().name();
						}
					}
					else if (type == typeid(ObjectVariableReference).name())
					{
						auto ref0 = boost::any_cast<ObjectVariableReference>(data[i]);

						auto object = ref0.get_object();
						auto name	= ref0.get_name();
						auto index	= ref0.get_index();

						auto var0 = object->getVariableReferenceByName(name);

						if (index == -1)
						{
							type = var0.value.type().name();
						}
						else
						{
							if (var0.value.type().name() == typeid(VariableList).name())
							{
								auto var1 = boost::any_cast<VariableList>(var0.value);

								auto var2 = var1.getByIndex(index);

								type = var2.value.type().name();
							}
							else if (var0.value.type().name() == typeid(VariableList*).name())
							{
								auto var1 = boost::any_cast<VariableList*>(var0.value);

								auto var2 = var1->getByIndex(index);

								type = var2.value.type().name();
							}
							else
								throw ExpressionException("ObjectVariableReference has index but target type is not array!!!");
						}
					}
					else if (type == typeid(ObjectVariableReference*).name())
					{
						auto ref0 = boost::any_cast<ObjectVariableReference*>(data[i]);
					}
					else if (type == typeid(VariableListReference).name())
					{
						type = processVariableListReference(data[i]);
					}
					else if (type == typeid(Variable).name())
					{
						auto ref0 = boost::any_cast<Variable>(data[i]);

						type = ref0.value.type().name();
					}
					else if (type == typeid(FunctionPtrCallCommand*).name())
					{
						auto ref0 = boost::any_cast<FunctionPtrCallCommand*>(data[i]);

						auto info = ref0->get_function()->getReturnValueInfoCopy();

						type = info.typeName.c_str();

						// dirty hack to overcome missing value of type in outer scope
						if (TypePriorities::isLowerPriority(result, type))
						{
							result = type;
						}
					}

					if (TypePriorities::isLowerPriority(result, type))
					{
						result = type;
					}
				}
			}
		}
		else if (typeid(*realNode).name() == typeid(ExpressionProxyNode).name())
		{
			auto children = node->get_children();
			if (children.size() > 0)
			{
				for (auto i = 0; i < (int)children.size(); i++)
				{
					auto type = getHighestPriorityTypeName(children[i]);

					if (TypePriorities::isLowerPriority(result, type))
					{
						result = type;
					}
				}
			}
		}

		return result;
	}

	int ExpressionTree::counter = 0;
}