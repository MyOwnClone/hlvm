#pragma once

#include "command.h"
#include "expressiontree.h"
#include "commandexception.h"

namespace hlvm
{
	class ReturnExpressionCommand : public Command
	{
	private:
		ExpressionTree* source;
	public:
		ReturnExpressionCommand(ExpressionTree* source) : source(source), Command()
		{

		};

		virtual bool execute();
		virtual ReturnExpressionCommand* copy();
		virtual ReturnExpressionCommand* deepCopy();
	};
}