#pragma once

#include "treenode.h"
#include "main.h"

namespace hlvm
{
	template<typename T> class Tree
	{
	private:
		TreeNode<T> *root = NULL;

		void copyInner(TreeNode<T>* oldParent, TreeNode<T>* newParent)
		{
			if (!oldParent)
			{
				return;
			}

			for (auto i = 0; i < (int)oldParent->get_children().size(); i++)
			{
				auto newChild = oldParent->get_children()[i]->copy();
				newChild->set_parent(newParent);

				newParent->get_children().push_back(newChild);

				copyInner(oldParent->get_children()[i], newChild);
			}
		}
	public:
		def_get_set(TreeNode<T> *, root);

		void traverseValues(void(*traverseValueFunc)(T), TreeNode<T> *node = NULL)
		{
			if (!node)
			{
				node = root;
			}

			if (!node)
			{
				return;
			}

			if (traverseValueFunc != NULL)
			{
				traverseValueFunc(node->get_value());
			}

			if (node->get_children().size() > 0)
			{
				for (auto i = 0; i < (int)node->get_children().size(); i++)
				{
					if (node->get_children()[i] != NULL)
					{
						traverseValues(traverseValueFunc, node->get_children()[i]);
					}					
				}
			}
		}

		void traverseNodes(void(*traverseNodeFunc)(TreeNode<T>*), TreeNode<T> *node = NULL)
		{
			if (!node)
			{
				node = root;
			}

			if (!node)
			{
				return;
			}

			if (traverseNodeFunc != NULL)
			{
				traverseNodeFunc(node);
			}

			if (node->get_children().size() > 0)
			{
				for (auto i = 0; i < (int)node->get_children().size(); i++)
				{
					if (node->get_children()[i] != NULL)
					{
						traverseNodes(traverseNodeFunc, node->get_children()[i]);
					}
				}
			}
		}

		void deleteAllNodes(TreeNode<T> *node = NULL)
		{
			if (!node)
			{
				node = root;
			}

			if (!node)
			{
				return;
			}

			if (node->get_children().size() > 0)
			{
				for (auto i = 0; i < (int)node->get_children().size(); i++)
				{
					if (node->get_children()[i] != NULL)
					{
						deleteAllNodes(node->get_children()[i]);
					}
				}
			}

			delete node;
			node = NULL;
		}

		virtual ~Tree()
		{
			deleteAllNodes();
		}

		virtual Tree* copy()
		{
			auto result = new Tree();

			auto rootCopy = root->copy();

			result->set_root(rootCopy);

			copyInner(root, rootCopy);

			return result;
		}
	};
}