#pragma once

#include <vector>
#include "treenode.h"
#include "logicoperators.h"
#include "relationoperators.h"
#include "variable.h"
#include "conditionexception.h"
#include "condition.h"
#include "relationoperators.h"
#include "relationcondition.h"
#include "logicoperators.h"

namespace hlvm
{	
	class ConditionNode : public TreeNode<Condition*>
	{
	public:
		ConditionNode(Condition* cond) : TreeNode(cond)
		{

		}

		ConditionNode() : TreeNode(NULL)
		{

		}

		bool execute();
		virtual ConditionNode* copy();
	};		
}