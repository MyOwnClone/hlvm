#pragma once

#include <vector>
#include "expression.h"
#include "operator.h"

namespace hlvm
{
	class ExpressionProxyNode : public Expression
	{
	public:
		ExpressionProxyNode(std::vector<Operator> operators)
		{
			this->operators = operators;
		}

		ExpressionProxyNode(Operator operatorInst)
		{
			operators.push_back(operatorInst);
		}

		ExpressionProxyNode()
		{

		}

		ExpressionProxyNode(const ExpressionProxyNode& orig)
		{
			this->operators = orig.operators;
		}

		virtual boost::any execute(std::vector<TreeNode<Expression*>*> children, std::string highestPriorityTypeName);
		virtual ExpressionProxyNode* copy();
		virtual ~ExpressionProxyNode();
	};
}