#include "variablelist.h"
#include "variableexception.h"

namespace hlvm
{
	void VariableList::addVariableReference(Variable &var)
	{
		items.push_back(var);
	}

	void VariableList::addVariableCopy(Variable variable)
	{
		items.push_back(variable);
	}

	Variable& VariableList::getByIndex(int i)
	{
		if (i < (int)items.size())
		{
			return items[i];
		}
		else
		{
			throw VariableException("wrong index!!!");
		}
	}
}