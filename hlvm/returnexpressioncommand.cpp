#include "returnexpressioncommand.h"
#include "resolver.h"
#include "variableexception.h"
#include "block.h"
#include "function.h"

namespace hlvm
{
	bool ReturnExpressionCommand::execute()
	{
		if (!blockParent || !blockParent->get_functionParent())
		{
			throw CommandException("Parent function is not set!!!");
		}

		auto tmpValue = source->execute();

		blockParent->get_functionParent()->get_returnValue() = boost::any();

		if (tmpValue.type().name() == typeid(int).name() && blockParent->get_functionParent()->getReturnValueInfoCopy().typeName == typeid(int).name())
		{
			blockParent->get_functionParent()->get_returnValue() = boost::any_cast<int>(tmpValue);
		}
		else if (tmpValue.type().name() == typeid(float).name() && blockParent->get_functionParent()->getReturnValueInfoCopy().typeName == typeid(float).name())
		{
			blockParent->get_functionParent()->get_returnValue() = boost::any_cast<float>(tmpValue);
		}
		else if (tmpValue.type().name() == typeid(double).name() && blockParent->get_functionParent()->getReturnValueInfoCopy().typeName == typeid(double).name())
		{
			blockParent->get_functionParent()->get_returnValue() = boost::any_cast<double>(tmpValue);
		}
		else if (tmpValue.type().name() == typeid(std::string).name() && blockParent->get_functionParent()->getReturnValueInfoCopy().typeName == typeid(std::string).name())
		{
			blockParent->get_functionParent()->get_returnValue() = boost::any_cast<std::string>(tmpValue);
		}
		else
		{
			throw VariableException("variable type mismatch!!!");
		}

		return true;
	}

	ReturnExpressionCommand* ReturnExpressionCommand::copy()
	{
		ReturnExpressionCommand* result = new ReturnExpressionCommand(source->copy());
		return result;
	}

	ReturnExpressionCommand* ReturnExpressionCommand::deepCopy()
	{
		ReturnExpressionCommand* result = new ReturnExpressionCommand(source->copy());

		if (variableMap)
		{
			result->variableMap = variableMap->deepCopy();
		}

		return result;
	}
}