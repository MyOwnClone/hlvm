#include "relationcondition.h"
#include "resolver.h"

namespace hlvm
{
	bool RelationCondition::execute(std::vector<TreeNode<Condition*>*> children)
	{
		if (children.size() > 0 || operands.size() != 2 || operators.size() != 1)
		{
			throw ConditionException("Children, operand or operator number does not match. There should be two operands and one operator.");
		}

		Variable* operand0 = NULL;
		Variable* operand1 = NULL;

		operand0 = Resolver::resolveVariableReferenceAndExecuteToPtr(operands[0]);
		operand1 = Resolver::resolveVariableReferenceAndExecuteToPtr(operands[1]);

		if (operand0 == NULL)
		{
			operand0 = operands[0];
		}
		if (operand1 == NULL)
		{
			operand1 = operands[1];
		}

		if (operand0->value.type().name() == typeid(int).name())
		{
			// we need that both operands have same type
			if (operand1->value.type().name() != typeid(int).name())
			{
				throw ConditionException("Both operands should have same type.");
			}

			auto operand0Integer = operand0->getIntValue();

			if (operand0->get_dispose())
			{
				delete operand0;
			}

			auto operand1Integer = operand1->getIntValue();

			if (operand1->get_dispose())
			{
				delete operand1;
			}

			switch (operators[0])
			{
			case RelationOperators::EQUAL:
				return operand0Integer == operand1Integer;
			case RelationOperators::NON_EQUAL:
				return operand0Integer != operand1Integer;
			case RelationOperators::GREATER:
				return operand0Integer > operand1Integer;
			case RelationOperators::LOWER:
				return operand0Integer < operand1Integer;
			case RelationOperators::LOWER_OR_EQUAL:
				return operand0Integer <= operand1Integer;
			case RelationOperators::GREATER_OR_EQUAL:
				return operand0Integer >= operand1Integer;
			default:
				throw ConditionException("Unknown RelationOperator.");
			}
		}
		else if (operand0->value.type().name() == typeid(double).name())
		{
			// we need that both operands have same type
			if (operand1->value.type().name() != typeid(double).name())
			{
				throw ConditionException("Both operands should have a same type!");
			}

			auto operand0Double = operand0->getDoubleValue();

			if (operand0->get_dispose())
			{
				delete operand0;
			}

			auto operand1Double = operand1->getDoubleValue();

			if (operand1->get_dispose())
			{
				delete operand1;
			}

			switch (operators[0])
			{
			case RelationOperators::EQUAL:
				return operand0Double == operand1Double;
			case RelationOperators::NON_EQUAL:
				return operand0Double != operand1Double;
			case RelationOperators::GREATER:
				return operand0Double > operand1Double;
			case RelationOperators::LOWER:
				return operand0Double < operand1Double;
			case RelationOperators::LOWER_OR_EQUAL:
				return operand0Double <= operand1Double;
			case RelationOperators::GREATER_OR_EQUAL:
				return operand0Double >= operand1Double;
			default:
				throw ConditionException("Unknown RelationOperator.");
			}
		}
		else if (operand0->value.type().name() == typeid(std::string).name())
		{
			// we need that both operands have same type
			if (operand1->value.type().name() != typeid(std::string).name())
			{
				throw ConditionException("Both operands should have a same type!");
			}

			auto operand0String = operand0->getStringValue();

			if (operand0->get_dispose())
			{
				delete operand0;
			}

			auto operand1String = operand1->getStringValue();

			if (operand1->get_dispose())
			{
				delete operand1;
			}

			switch (operators[0])
			{
			case RelationOperators::EQUAL:
				return operand0String == operand1String;
			case RelationOperators::NON_EQUAL:
				return operand0String != operand1String;
			case RelationOperators::GREATER:
				throw ConditionException("Greater operator does not make sense for string.");
			case RelationOperators::LOWER:
				throw ConditionException("Lower operator does not make sense for string.");
			case RelationOperators::LOWER_OR_EQUAL:
				throw ConditionException("LowerOrEqual operator does not make sense for string.");
			case RelationOperators::GREATER_OR_EQUAL:
				throw ConditionException("GreaterOrEqual operator does not make sense for string.");
			default:
				throw ConditionException("Unknown RelationOperator.");
			}
		}
		else
		{
			throw new ConditionException("Unknown type!");
		}
	}

	RelationCondition* RelationCondition::copy()
	{
		auto result = new RelationCondition();

		result->operators = operators;

		for (auto i = 0; i < (int)operands.size(); i++)
		{
			result->operands.push_back(operands[i]->copy());
		}

		return result;
	}

	RelationCondition::~RelationCondition()
	{
		for (auto i = 0; i < (int)operands.size(); i++)
		{
			delete operands[i];
		}
	}

}