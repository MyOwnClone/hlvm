#pragma once

#include <vector>
#include "treenode.h"

namespace hlvm
{
	class Condition
	{
	public:
		virtual bool execute(std::vector<TreeNode<Condition*>*> children) = 0;
		virtual Condition* copy() = 0;
		virtual ~Condition()
		{

		}
	};
}