#pragma once

#include "boost/any.hpp"
#include "command.h"
#include "expressiontree.h"
#include "commandexception.h"

namespace hlvm
{
	class AssignExpressionCommand : public Command
	{
	private:
		boost::any target;
		ExpressionTree* source;
	public:
		AssignExpressionCommand(boost::any target, ExpressionTree* source) : target(target), source(source), Command()
		{

		};

		virtual bool execute();
		virtual AssignExpressionCommand* copy();
		virtual AssignExpressionCommand* deepCopy();
	};
}