#pragma once
#include "boost/any.hpp"
#include <vector>
#include <string>

#include "expressionexception.h"
#include "treenode.h"
#include "main.h"
#include "variablereference.h"
#include "typepriorities.h"
#include "expression.h"
#include "expressiondatanode.h"
#include "expressionproxynode.h"

namespace hlvm
{
	class ExpressionNode : public TreeNode<Expression*>
	{
	public:
		ExpressionNode(Expression* cond) : TreeNode(cond)
		{

		}

		ExpressionNode() : TreeNode(NULL)
		{

		}

		boost::any execute(std::vector<TreeNode<Expression*>*> children, std::string highestPriorityTypeName);
		virtual ExpressionNode* copy();
		std::vector<boost::any>& getData();
		std::vector<Operator>& getOperators();
	};
}