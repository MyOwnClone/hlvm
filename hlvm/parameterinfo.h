#pragma once

#include <string>
#include <vector>

#include "main.h"

namespace hlvm
{
	enum class ParameterMeaning
	{
		NONE
	};

	struct ParameterInfoItem
	{
	public:
		std::string name;
		std::string typeName;
		ParameterMeaning meaning;
	};

	class ParameterInfo
	{
	protected:
		std::vector<ParameterInfoItem> items;
	public:
		def_get_set(std::vector<ParameterInfoItem>&, items);

		ParameterInfo() {};

		ParameterInfo(std::vector<ParameterInfoItem> items) :items(items)
		{

		}
	};
}
