#pragma once

#include "variablemap.h"
#include "parameterinfo.h"
#include "returnvalueinfo.h"
#include "block.h"
#include "main.h"
#include "boost/any.hpp"

namespace hlvm
{
	class Function
	{
	protected:
		VariableMap parameters;
		ParameterInfo parameterInfo;
		ReturnValueInfo returnValueInfo;
		Block block;
		boost::any returnValue;
	public:
		virtual bool execute();
		bool checkParameters();
		//VariableMap* getParametersPtr();
		Function* copy();
		Function* deepCopy();

		Function() {
			block.set_functionParent(this);
		}

		Function(const Function& orig)
		{
			parameters = orig.parameters;
			parameterInfo = orig.parameterInfo;
			block = orig.block;
			//block.set_functionParent(this);
			returnValueInfo = orig.returnValueInfo;
			//returnValue = orig.returnValue;
		}
		Function* operator=(const Function& orig)
		{
			parameters = orig.parameters;
			parameterInfo = orig.parameterInfo;
			block = orig.block;
			//block.set_functionParent(this);
			returnValueInfo = orig.returnValueInfo;
			//returnValue = orig.returnValue;

			return this;
		}

		def_get_set(boost::any&, returnValue);
		def_get_set(VariableMap&, parameters);
		def_get(ReturnValueInfo&, returnValueInfo);
		def_get(ParameterInfo&, parameterInfo);
		def_get(Block&, block);

		ReturnValueInfo getReturnValueInfoCopy()
		{
			return returnValueInfo; 
		}

		void setReturnTypeString(std::string returnTypeString);

		static void addParameterInfoTo(Function* function, ParameterInfoItem parameterInfo);
		static void addParameterInfoTo(Function &function, ParameterInfoItem parameterInfo);
		static void addCommandTo(Function* function, Command *command);
		static void addCommandTo(Function &function, Command *command);
		static void addParameterTo(Function &function, std::string name, Variable variable);
		static void addParameterTo(Function* function, std::string name, Variable variable);
		static void setParentVariableMap(Function* function, VariableMap* map);
		static void setParentVariableMap(Function &function, VariableMap* map);

		virtual ~Function()
		{

		}
	};
}