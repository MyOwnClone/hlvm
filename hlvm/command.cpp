#include "command.h"

namespace hlvm
{
	bool Command::execute()
	{
		return true;
	}

	Command* Command::copy()
	{
		Command* result = new Command();

		result->variableMap = variableMap;

		return result;
	}

	Command* Command::deepCopy()
	{
		Command* result = new Command();

		if (variableMap)
		{
			result->variableMap = variableMap->deepCopy();
		}
		else
		{
			result->variableMap = NULL;
		}

		return result;
	}
}