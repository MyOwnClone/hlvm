#pragma once
#include <exception>
#include <string>

namespace hlvm
{
	class ConditionException : public std::runtime_error
	{
	public:
		ConditionException(std::string message) : std::runtime_error(message) {}
	};
}