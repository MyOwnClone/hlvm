#pragma once

#include <vector>
#include "command.h"
#include "main.h"

namespace hlvm
{
	class Function;
	class Block
	{
	private:
		int id;
		static int counter;
	protected:
		std::vector<Command*> commands;
		Function *functionParent;
	public:
		Block()
		{
			id = counter++;
		}

		/*Block(const Block& orig)
		{
			this->id = orig.id;
			for (auto i = 0; i < (int)orig.commands.size(); i++)
			{
				this->addCommand(orig.commands[i]->deepCopy());
			}
		}*/

		/*Block& operator=(const Block& orig)
		{
			this->id = orig.id;
			for (auto i = 0; i < (int)orig.commands.size(); i++)
			{
				this->addCommand(orig.commands[i]->deepCopy());
			}

			return *this;
		}*/

		def_get_set(Function*, functionParent);
		def_get(int, id);
		def_get_set(std::vector<Command*>&, commands);
		bool execute();
		virtual Block *copy();
		void addCommand(Command* command);
		static void addCommandTo(Block *block, Command* command);
		Block* deepCopy();

		~Block()
		{
			for (auto i = 0; i < (int)commands.size(); i++)
			{
				if (commands[i])
				{
					//delete commands[i];
					//commands[i] = NULL;
				}
			}
		}
	};
}