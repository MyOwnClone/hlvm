#pragma once

#include <vector>
#include "variable.h"

namespace hlvm
{
	class VariableList
	{
	protected:
		std::vector<Variable> items;
	public:
		VariableList() {};

		VariableList(const VariableList &orig) 
		{
			items = orig.items;
		};

		void addVariableReference(Variable &var);
		void addVariableCopy(Variable variable);
		Variable& getByIndex(int i);
	};
}