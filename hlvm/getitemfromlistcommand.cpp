#include "getitemfromlistcommand.h"
#include "resolver.h"
#include "variableexception.h"
#include "variablelist.h"
#include "commandexception.h"

namespace hlvm
{
	bool GetItemFromListCommand::execute()
	{
		Variable *realVariableTarget = NULL;

		realVariableTarget = Resolver::resolve(target);

		if (!realVariableTarget)
		{
			throw CommandException("Target is not VariableReference neither Variable");
		}

		Variable *realVariableList = NULL;

		realVariableList = Resolver::resolve(list);

		if (!realVariableList)
		{
			throw CommandException("Target is not VariableReference neither Variable");
		}

		if (realVariableList && index >= 0)
		{
			if (realVariableList->value.type().name() == typeid(VariableList).name())
			{
				auto realList = boost::any_cast<VariableList>(realVariableList->value);

				auto tmpResult = realList.getByIndex(index);

				if (tmpResult.value.type().name() == typeid(Variable).name())
				{
					auto ref0 = boost::any_cast<Variable>(tmpResult.value);

					realVariableTarget->value = ref0.value;
				}
				else if (tmpResult.value.type().name() == typeid(Variable*).name())
				{
					auto ref0 = boost::any_cast<Variable*>(tmpResult.value);

					realVariableTarget->value = ref0->value;
				}
				else
				{
					realVariableTarget->value = tmpResult;
				}
			}
			else if (realVariableList->value.type().name() == typeid(VariableList*).name())
			{
				auto realList = boost::any_cast<VariableList*>(realVariableList->value);

				auto tmpResult = realList->getByIndex(index);

				if (tmpResult.value.type().name() == typeid(Variable).name())
				{
					auto ref0 = boost::any_cast<Variable>(tmpResult.value);

					realVariableTarget->value = ref0.value;
				}
				else if (tmpResult.value.type().name() == typeid(Variable*).name())
				{
					auto ref0 = boost::any_cast<Variable*>(tmpResult.value);

					realVariableTarget->value = ref0->value;
				}
				else
				{
					realVariableTarget->value = tmpResult;
				}
			}
		}

		if (realVariableTarget->get_dispose())
		{
			delete realVariableTarget;
		}

		return true;
	}

	GetItemFromListCommand* GetItemFromListCommand::copy()
	{
		GetItemFromListCommand* result = new GetItemFromListCommand(target, list, index);
		return result;
	}

	GetItemFromListCommand* GetItemFromListCommand::deepCopy()
	{
		GetItemFromListCommand* result = new GetItemFromListCommand(target, list, index);
		return result;
	}
}