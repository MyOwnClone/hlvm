namespace hlvm
{
	class Mutable
	{
	protected:
		bool _isMutable;
	public:
		void setMutable(bool value)
		{
			_isMutable = value;
		}

		bool isMutable()
		{
			return _isMutable;
		}
	};
}