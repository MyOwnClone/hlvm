#include "object.h"
#include <string>

namespace hlvm
{
	class ObjectFactory
	{
	public:
		static Object getObject();
		static bool addVariable(Object& obj, std::string name, Variable variable);
	};
}