#pragma once

#include "boost/any.hpp"
#include "variable.h"
#include "variablelistreference.h"

namespace hlvm
{
	class Object;

	class Resolver
	{
	protected:
		static Variable* processVariableListReferenceToPtr(boost::any reference);
		static Variable* processObjectVariableReferenceToPtr(Object *object, std::string name, int index);
	public:
		static Variable* resolveVariableReferenceToPtr(VariableReference reference);
		static Variable* resolveVariableReferenceToPtr(VariableReference *reference);
		static Variable* resolveVariableReferenceToPtr(boost::any reference);
		static Variable* resolveVariableReferenceAndExecuteToPtr(boost::any reference, int index = -1);
		static Variable* resolve(boost::any reference, int index = -1);
		static Variable* resolveVariableListReferenceToPtr(VariableListReference *reference);
		static Variable* resolveVariableListReferenceToPtr(VariableListReference reference);
	};
}