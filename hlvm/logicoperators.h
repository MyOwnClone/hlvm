#pragma once

namespace hlvm
{
	enum class LogicOperators
	{
		AND,
		OR
	};
}