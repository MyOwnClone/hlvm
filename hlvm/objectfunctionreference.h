#include <string>

#include "main.h"

namespace hlvm
{
	class Object;
	class ObjectFunctionReference
	{
	protected:
		Object* object;
		std::string name;
	public:
		def_get_set(Object*, object);
		def_get_set(std::string, name);

		ObjectFunctionReference(std::string name, Object* object) : name(name), object(object)
		{

		}

		void execute()
		{
			auto function = object->getFunctionByName(name);
			function.execute();
		}
	};
}