#pragma once

#include <vector>
#include "main.h"

namespace hlvm
{
	template<typename T> class TreeNode
	{
	private:
		T value;
		TreeNode<T> *parent = NULL;
		std::vector<TreeNode<T>*> children;
	protected:
	public:
		def_get_set(std::vector<TreeNode<T>*>&, children);
		def_get_set(TreeNode<T> *, parent);
		def_get_set(T, value);

		TreeNode<T>(T value)
		{
			this->value = value;
		}

		virtual TreeNode* copy()
		{
			auto result = new TreeNode<T>(value);

			result->parent = NULL;

			return result;
		}
	};
}