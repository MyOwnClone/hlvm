#pragma once

#include <unordered_map>
#include <string>

namespace hlvm
{
	class GuardedClass
	{
	protected:
		static std::unordered_map<std::string, int> counters;
	public:
		GuardedClass* increment()
		{
			auto name = typeid(this).name();

			if (counters.find(name) == counters.end())
			{
				counters[name] = 1;
			}
			else
			{
				counters[name]++;
			}

			return this;
		}

		GuardedClass* decrement()
		{
			auto name = typeid(this).name();

			if (counters.find(name) == counters.end())
			{
				throw std::runtime_error("releasing unexpected instance, counter has invalid value!!!");
			}
			else
			{
				counters[name]--;

				if (counters[name] < 0)
				{
					throw std::runtime_error("releasing unexpected instance, counter has invalid value!!!");
				}
			}

			return this;
		}

		int getCounter()
		{
			return counters[typeid(this).name()];
		}

		virtual ~GuardedClass()
		{
			decrement();
		}
	};

	#define GUARDED_ALLOC(ctorCall) (decltype(ctorCall))((ctorCall)->increment())
	#define GUARDED_FREE(x) (delete (x))
}