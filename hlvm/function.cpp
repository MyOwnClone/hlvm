#include "function.h"

namespace hlvm
{
	bool Function::execute()
	{
		if (checkParameters())
		{
			return block.execute();
		}
		else
		{
			return false;
		}
	}

	bool Function::checkParameters()
	{
		for (auto i = 0; i < (int)parameterInfo.get_items().size(); i++)
		{
			auto item = parameterInfo.get_items()[i];

			auto name = item.name;
			auto typeName = item.typeName;
			auto meaning = item.meaning;

			if (!parameters.variableExists(name))
			{
				return false;
			}

			if (parameters.getVariableByName(name).value.type().name() != typeName)
			{
				return false;
			}
		}

		return true;
	}

	Function* Function::copy()
	{
		Function* result = new Function();

		result->parameters = parameters;
		result->parameterInfo = parameterInfo;
		result->block = block;

		return result;
	}

	void Function::addParameterInfoTo(Function &function, ParameterInfoItem parameterInfo)
	{
		function.get_parameterInfo().get_items().push_back(parameterInfo);
	}

	void Function::setReturnTypeString(std::string returnTypeString)
	{
		get_returnValueInfo().typeName = returnTypeString;
	}

	void Function::addParameterInfoTo(Function* function, ParameterInfoItem parameterInfo)
	{
		function->get_parameterInfo().get_items().push_back(parameterInfo);
	}

	void Function::addCommandTo(Function* function, Command *command)
	{
		command->set_blockParent(&function->block);
		function->get_block().addCommand(command);
	}

	void Function::addCommandTo(Function &function, Command *command)
	{
		command->set_blockParent(&function.block);
		function.get_block().addCommand(command);
	}

	void Function::addParameterTo(Function* function, std::string name, Variable variable)
	{
		function->get_parameters().addVariable(name, variable);
	}

	void Function::addParameterTo(Function &function, std::string name, Variable variable)
	{
		function.get_parameters().addVariable(name, variable);
	}

	void Function::setParentVariableMap(Function* function, VariableMap* map)
	{
		function->get_parameters().set_parent(map);
	}

	void Function::setParentVariableMap(Function &function, VariableMap* map)
	{
		function.get_parameters().set_parent(map);
	}

	Function* Function::deepCopy()
	{
		Function* result = new Function();

		result->parameters = *(parameters.deepCopy());
		result->parameterInfo = parameterInfo;
		result->block = *(block.deepCopy());

		return result;
	}
}