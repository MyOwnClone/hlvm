#include "typepriorities.h"

namespace hlvm
{
	std::unordered_map<std::string, int> TypePriorities::priorities = TypePriorities::fillPriorities();

	std::unordered_map<std::string, int> TypePriorities::fillPriorities()
	{
		std::unordered_map<std::string, int> result;

		result.insert(std::make_pair<std::string, int>(typeid(int).name(), 0));
		result.insert(std::make_pair<std::string, int>(typeid(float).name(), 1));
		result.insert(std::make_pair<std::string, int>(typeid(double).name(), 2));
		result.insert(std::make_pair<std::string, int>(typeid(std::string).name(), 3));

		return result;
	}
}