#include "divideexpressioncommand.h"
#include "resolver.h"
#include "variableexception.h"

namespace hlvm
{
	bool DivideExpressionCommand::execute()
	{
		Variable *realVariableTarget = NULL;

		realVariableTarget = Resolver::resolveVariableReferenceAndExecuteToPtr(target);

		if (!realVariableTarget)
		{
			throw CommandException("Target is not VariableReference neither Variable");
		}

		auto tmpValue = source->execute();

		if (tmpValue.type().name() == typeid(int).name() && realVariableTarget->value.type().name() == typeid(int).name())
		{
			realVariableTarget->value = boost::any_cast<int>(realVariableTarget->value) / boost::any_cast<int>(tmpValue);
		}
		else if (tmpValue.type().name() == typeid(float).name() && realVariableTarget->value.type().name() == typeid(float).name())
		{
			realVariableTarget->value = boost::any_cast<float>(realVariableTarget->value) / boost::any_cast<float>(tmpValue);
		}
		else if (tmpValue.type().name() == typeid(double).name() && realVariableTarget->value.type().name() == typeid(double).name())
		{
			realVariableTarget->value = boost::any_cast<double>(realVariableTarget->value) / boost::any_cast<double>(tmpValue);
		}
		else
		{
			throw VariableException("variable type mismatch!!!");
		}

		if (realVariableTarget->get_dispose())
		{
			delete realVariableTarget;
		}

		return true;
	}

	DivideExpressionCommand* DivideExpressionCommand::copy()
	{
		DivideExpressionCommand* result = new DivideExpressionCommand(target, source->copy());
		return result;
	}

	DivideExpressionCommand* DivideExpressionCommand::deepCopy()
	{
		DivideExpressionCommand* result = new DivideExpressionCommand(target, source->copy());

		if (variableMap)
		{
			result->variableMap = variableMap->deepCopy();
		}

		return result;
	}
}