#include "blockfactory.h"

namespace hlvm
{
	Block BlockFactory::getBlock()
	{
		return Block();
	}

	void BlockFactory::addCommand(Block& block, Command* command)
	{
		block.addCommand(command);
	}
}