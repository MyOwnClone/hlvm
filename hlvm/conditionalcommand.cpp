#include "conditionalcommand.h"

namespace hlvm
{
	bool ConditionalCommand::execute()
	{
		if (conditionTree->execute())
		{
			block.execute();
		}

		return true;
	}

	ConditionalCommand* ConditionalCommand::copy()
	{
		ConditionalCommand* result = new ConditionalCommand(*block.copy(), conditionTree->copy());
		return result;
	}
}