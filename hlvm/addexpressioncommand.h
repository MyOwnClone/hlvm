#pragma once

#include "boost/any.hpp"
#include "command.h"
#include "expressiontree.h"
#include "commandexception.h"

namespace hlvm
{
	class AddExpressionCommand : public Command
	{
	private:
		boost::any target;
		ExpressionTree* source;
	public:
		AddExpressionCommand(boost::any target, ExpressionTree* source) : target(target), source(source), Command()
		{

		};

		virtual bool execute();
		virtual AddExpressionCommand* copy();
		virtual AddExpressionCommand* deepCopy();
	};
}