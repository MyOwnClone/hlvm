#include "expressionnode.h"

namespace hlvm
{
	class ExpressionDataNode;
	boost::any ExpressionNode::execute(std::vector<TreeNode<Expression*>*> children, std::string highestPriorityTypeName)
	{
		return get_value()->execute(get_children(), highestPriorityTypeName);
	}

	ExpressionNode* ExpressionNode::copy()
	{
		auto result = new ExpressionNode(get_value()->copy());

		result->set_parent(NULL);

		return result;
	}

	std::vector<boost::any>& ExpressionNode::getData()
	{
		return ((ExpressionDataNode*) this->get_value())->get_data();
	}

	std::vector<Operator>& ExpressionNode::getOperators()
	{
		return ((ExpressionProxyNode*) this->get_value())->get_operators();
	}
}