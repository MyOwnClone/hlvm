#include "expression.h"

namespace hlvm
{
	boost::any Expression::executeInner(boost::any value0, boost::any value1, int operatorIndex, boost::any &result)
	{
		if (value0.type().name() == typeid(int).name())
		{
			if (value0.type().name() != value1.type().name())
			{
				throw ExpressionException("Variable type mismatch!!!");
			}

			switch (operators[operatorIndex])
			{
			case Operator::PLUS:
				result = boost::any_cast<int>(result)+(boost::any_cast<int>(value0)+boost::any_cast<int>(value1));
				break;
			case Operator::MINUS:
				result = boost::any_cast<int>(result)+(boost::any_cast<int>(value0)-boost::any_cast<int>(value1));
				break;
			case Operator::MULTIPLY:
				result = boost::any_cast<int>(result)+(boost::any_cast<int>(value0)* boost::any_cast<int>(value1));
				break;
			case Operator::DIVIDE:
				result = boost::any_cast<int>(result)+(boost::any_cast<int>(value0) / boost::any_cast<int>(value1));
				break;
			default:
				throw ExpressionException("Unknown operator.");
			}
		}
		else if (value0.type().name() == typeid(double).name())
		{
			if (value0.type().name() != value1.type().name())
			{
				throw ExpressionException("Variable type mismatch!!!");
			}

			switch (operators[operatorIndex])
			{
			case Operator::PLUS:
				result = boost::any_cast<double>(result)+(boost::any_cast<double>(value0)+boost::any_cast<double>(value1));
				break;
			case Operator::MINUS:
				result = boost::any_cast<double>(result)+(boost::any_cast<double>(value0)-boost::any_cast<double>(value1));
				break;
			case Operator::MULTIPLY:
				result = boost::any_cast<double>(result)+(boost::any_cast<double>(value0)* boost::any_cast<double>(value1));
				break;
			case Operator::DIVIDE:
				result = boost::any_cast<double>(result)+(boost::any_cast<double>(value0) / boost::any_cast<double>(value1));
				break;
			default:
				throw ExpressionException("Unknown operator.");
			}
		}
		else if (value0.type().name() == typeid(float).name())
		{
			if (value0.type().name() != value1.type().name())
			{
				throw ExpressionException("Variable type mismatch!!!");
			}

			switch (operators[operatorIndex])
			{
			case Operator::PLUS:
				result = boost::any_cast<float>(result)+(boost::any_cast<float>(value0)+boost::any_cast<float>(value1));
				break;
			case Operator::MINUS:
				result = boost::any_cast<float>(result)+(boost::any_cast<float>(value0)-boost::any_cast<float>(value1));
				break;
			case Operator::MULTIPLY:
				result = boost::any_cast<float>(result)+(boost::any_cast<float>(value0)* boost::any_cast<float>(value1));
				break;
			case Operator::DIVIDE:
				result = boost::any_cast<float>(result)+(boost::any_cast<float>(value0) / boost::any_cast<float>(value1));
				break;
			default:
				throw ExpressionException("Unknown operator.");
			}
		}
		else if (value0.type().name() == typeid(std::string).name())
		{
			if (value0.type().name() != value1.type().name())
			{
				throw ExpressionException("Variable type mismatch!!!");
			}

			switch (operators[operatorIndex])
			{
			case Operator::PLUS:
				result = boost::any_cast<std::string>(result)+(boost::any_cast<std::string>(value0)+boost::any_cast<std::string>(value1));
				break;
			default:
				throw ExpressionException("Unknown operator.");
			}
		}
		else
		{
			throw ExpressionException("Wrong left side variable type!!!");
		}

		return result;
	}

	boost::any Expression::execute(std::vector<TreeNode<Expression*>*> children, std::string highestPriorityTypeName)
	{
		return boost::any();
	}

	Expression* Expression::copy()
	{
		auto result = new Expression();

		result->operators = operators;

		return result;
	}
}