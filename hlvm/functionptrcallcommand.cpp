#include "functionptrcallcommand.h"

namespace hlvm
{
	bool FunctionPtrCallCommand::execute()
	{
		return function->execute();
	}

	FunctionPtrCallCommand* FunctionPtrCallCommand::copy()
	{
		FunctionPtrCallCommand* result = new FunctionPtrCallCommand();

		result->set_function(function);
		result->set_variableMap(get_variableMap());

		return result;
	}

	void FunctionPtrCallCommand::addParameterTo(FunctionPtrCallCommand* callCommand, std::string name, Variable variable)
	{
		callCommand->get_variableMap()->addVariable(name, variable);
	}

	FunctionPtrCallCommand* FunctionPtrCallCommand::deepCopy()
	{
		FunctionPtrCallCommand* result = new FunctionPtrCallCommand();

		result->set_variableMap(get_variableMap()->deepCopy());
		result->get_variableMap()->set_parent(get_variableMap()->get_parent());
		result->function = function;
		//result->function->getParametersPtr()->set_parent(get_variableMap());

		return result;
	}

	void FunctionPtrCallCommand::setFunctionToCall(Function* function)
	{
		this->function = function;
	}

	Function* FunctionPtrCallCommand::getFunctionToCall()
	{
		return this->function;
	}
}