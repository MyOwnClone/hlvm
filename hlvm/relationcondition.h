#pragma once

#include <vector>
#include "condition.h"
#include "relationoperators.h"
#include "variable.h"
#include "conditionexception.h"
#include "variablemap.h"
#include "expressiontree.h"

namespace hlvm
{
	class RelationCondition : public Condition
	{
	public:
		std::vector<Variable*> operands;
		std::vector<RelationOperators> operators;

		RelationCondition()
		{

		}

		RelationCondition(std::vector<Variable*> operands, std::vector<RelationOperators> operators) : operands(operands), operators(operators)
		{

		}

		RelationCondition(const RelationCondition& orig)
		{
			this->operators = orig.operators;
			this->operands = orig.operands;
		}

		virtual bool execute(std::vector<TreeNode<Condition*>*> children);
		virtual RelationCondition* copy();
		virtual ~RelationCondition();
	};
}