#include <math.h>

namespace ravelib
{
	/* Function to control # of decimal places to be output for x */
	// source http://stackoverflow.com/questions/1343890/rounding-number-to-2-decimal-places-in-c
	double getDecimals(const double x, const int numDecimals)
	{
		int y = (int)x;
		double z = x - y;
		double m = pow(10.0, numDecimals);
		double q = z*m;
		double r = round(q);

		return static_cast<double>(y)+(1.0 / m)*r;
	}
}