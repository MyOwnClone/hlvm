#include "variable.h"
#include "variablelist.h"
#include "object.h"

namespace hlvm
{
	bool Variable::isNull()
	{
		return nullFlag;
	}

	Variable Variable::from(int i)
	{
		Variable result;
		result.value = i;
		return result;
	}

	Variable Variable::from(float f)
	{
		Variable result;
		result.value = f;
		return result;
	}

	Variable Variable::from(double d)
	{
		Variable result;
		result.value = d;
		return result;
	}

	Variable Variable::from(std::string s)
	{
		Variable result;
		result.value = s;
		return result;
	}

	Variable Variable::from(Function* f)
	{
		Variable result;
		result.value = f;
		return result;
	}

	Variable Variable::copyFrom(VariableList* l)
	{
		Variable result;
		result.value = new VariableList(*l);
		return result;
	}

	Variable Variable::copyFrom(Object* o)
	{
		Variable result;
		result.value = new Object(*o);
		return result;
	}

	VariableList* Variable::getVariableListPtr()
	{
		return boost::any_cast<VariableList*>(value);
	}

	Variable Variable::from(Object* o)
	{
		Variable result;
		result.value = o;
		return result;
	}

	Variable Variable::from(ExpressionTree* t)
	{
		Variable result;
		result.value = t;
		return result;
	}

	Variable Variable::from(VariableReference reference)
	{
		Variable result;
		result.value = reference;
		return result;
	}

	Variable Variable::from(VariableList* l)
	{
		Variable result;
		result.value = l;
		return result;
	}

	Variable Variable::fromAny(boost::any item)
	{
		Variable result;
		result.value = item;
		return result;
	}

	Variable Variable::from(VariableListReference reference)
	{
		Variable result;
		result.value = reference;
		return result;
	}
	
	Variable* Variable::getInstance()
	{
		return new Variable();
	}

	Variable* Variable::ptrFromAny(boost::any item)
	{
		Variable *result = getInstance();
		return result->value = item, result;
	}

	Variable* Variable::ptrFrom(Function* f)
	{
		Variable *result = getInstance();
		return result->value = f, result;
	}

	Variable* Variable::ptrFrom(boost::any item)
	{
		Variable *result = getInstance();
		return result->value = item, result;
	}

	Variable* Variable::ptrFrom(ExpressionTree* t)
	{
		Variable *result = getInstance();
		return result->value = t, result;
	}

	Variable* Variable::ptrFrom(int i)
	{
		Variable *result = getInstance();
		return result->value = i, result;
	}

	Variable* Variable::ptrFrom(double d)
	{
		Variable *result = getInstance();
		return result->value = d, result;
	}

	Variable* Variable::ptrFrom(std::string s)
	{
		Variable *result = getInstance();
		return result->value = s, result;
	}

	Variable* Variable::ptrFrom(VariableList* l)
	{
		Variable *result = getInstance();
		return result->value = l, result;
	}

	Variable* Variable::ptrFrom(VariableReference reference)
	{
		Variable *result = getInstance();
		return result->value = reference, result;
	}

	Variable* Variable::ptrFrom(VariableListReference reference)
	{
		Variable *result = getInstance();
		return result->value = reference, result;
	}

	Variable Variable::getNullInstance()
	{
		Variable result;

		result.nullFlag = true;

		return result;
	}

	Variable* Variable::copy()
	{
		Variable *result = new Variable();

		result->value = value;

		return result;
	}

	int Variable::getIntValue()
	{
		return boost::any_cast<int>(value);
	}

	Function* Variable::getFunctionPtr()
	{
		return boost::any_cast<Function*>(value);
	}

	double Variable::getDoubleValue()
	{
		return boost::any_cast<double>(value);
	}

	float Variable::getFloatValue()
	{
		return boost::any_cast<float>(value);
	}

	std::string Variable::getStringValue()
	{
		return boost::any_cast<std::string>(value);
	}

	VariableReference Variable::getVariableReferenceValue()
	{
		return boost::any_cast<VariableReference>(value);
	}

	Variable* Variable::getVariablePtrValue()
	{
		return boost::any_cast<Variable*>(value);
	}

	Variable& Variable::getVariableRefValue()
	{
		return boost::any_cast<Variable&>(value);
	}

	Object* Variable::getObjectPtr()
	{
		return boost::any_cast<Object*>(value);
	}

	Variable* Variable::deepCopy()
	{
		Variable *result = new Variable();

		// value of variable is pointer to another variable
		// so we want value of pointed variable
		if (value.type().name() == typeid(Variable*).name())
		{
			result->value = boost::any_cast<Variable*>(value)->value;
		}
		else
		{
			// can be primitive data type or VariableReference
			result->value = value;
		}

		return result;
	}
};