#include "expressionproxynode.h"

namespace hlvm
{
	boost::any ExpressionProxyNode::execute(std::vector<TreeNode<Expression*>*> children, std::string highestPriorityTypeName)
	{
		boost::any result;

		if (highestPriorityTypeName == typeid(double).name())
		{
			result = (double)0;
		}
		else if (highestPriorityTypeName == typeid(int).name())
		{
			result = (int)0;
		}

		if (children.size() > 0)
		{
			if (children.size() != (operators.size() + 1))
			{
				throw ExpressionException("Number of operators should be equal to number of children minus 1.");
			}

			for (auto i = 0; i < (int)children.size(); i += 2)
			{
				int operatorIndex = i / 2;

				auto child0 = children[i];
				auto child1 = children[i + 1];

				boost::any value0 = child0->get_value()->execute(child0->get_children(), highestPriorityTypeName);
				boost::any value1 = child1->get_value()->execute(child1->get_children(), highestPriorityTypeName);

				return executeInner(value0, value1, operatorIndex, result);
			}

			return result;
		}
		else
		{
			throw ExpressionException("Children are missing!");
		}
	}

	ExpressionProxyNode* ExpressionProxyNode::copy()
	{
		auto result = new ExpressionProxyNode();

		result->operators = operators;

		return result;
	}

	ExpressionProxyNode::~ExpressionProxyNode()
	{

	}
}