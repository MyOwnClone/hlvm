#pragma once

#include "command.h"
#include "function.h"
#include "main.h"
#include "variable.h"

namespace hlvm
{
	class FunctionPtrCallCommand : public Command
	{
	protected:
		Function* function;
	public:
		def_get_set(Function*, function);

		virtual bool execute();
		virtual FunctionPtrCallCommand* copy();
		static void addParameterTo(FunctionPtrCallCommand* callCommand, std::string name, Variable variable);
		virtual FunctionPtrCallCommand* deepCopy();
		void setFunctionToCall(Function* function);
		Function* getFunctionToCall();
	};
}