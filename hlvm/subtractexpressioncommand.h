#pragma once

#include "boost/any.hpp"
#include "command.h"
#include "expressiontree.h"
#include "commandexception.h"

namespace hlvm
{
	class SubtractExpressionCommand : public Command
	{
	private:
		boost::any target;
		ExpressionTree* source;
	public:
		SubtractExpressionCommand(boost::any target, ExpressionTree* source) : target(target), source(source), Command()
		{

		};

		virtual bool execute();
		virtual SubtractExpressionCommand* copy();
		virtual SubtractExpressionCommand* deepCopy();
	};
}