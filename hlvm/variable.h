#pragma once

#include <string>
#include "boost/any.hpp"

#include "variablereference.h"
#include "variablelistreference.h"
#include "disposable.h"

namespace hlvm
{
	class Function;
	class VariableList;
	class ExpressionTree;
	class Object;
	class Variable : public Disposable
	{
	private:
		bool nullFlag;	// indicates "NULL" variable
	public:
		boost::any value;

		Variable() { nullFlag = false; };
		Variable(const Variable& orig) { this->value = orig.value;};

		Variable* setDispose(bool value)
		{
			dispose = value;
			return this;
		}

		virtual bool isNull();
		static Variable from(int i);
		static Variable from(float f);
		static Variable from(double d);
		static Variable from(std::string s);
		static Variable copyFrom(VariableList* l);
		static Variable copyFrom(Object* o);
		static Variable from(Object* o);
		static Variable from(VariableList* l);
		static Variable from(ExpressionTree* t);
		static Variable from(Function* f);
		static Variable fromAny(boost::any item);
		static Variable from(VariableReference reference);
		static Variable from(VariableListReference reference);
		static Variable* ptrFrom(boost::any item);
		static Variable* getInstance();
		static Variable* ptrFrom(VariableList* l);
		static Variable* ptrFrom(ExpressionTree* t);
		static Variable* ptrFrom(Function* f);
		static Variable* ptrFrom(int i);
		static Variable* ptrFrom(double d);
		static Variable* ptrFrom(std::string s);
		static Variable* ptrFrom(VariableReference reference);
		static Variable* ptrFrom(VariableListReference reference);
		static Variable getNullInstance();
		static Variable* ptrFromAny(boost::any item);
		Variable* copy();
		Function* getFunctionPtr();
		Function getFunction();
		int getIntValue();
		double getDoubleValue();
		float getFloatValue();
		VariableList* getVariableListPtr();
		Object* getObjectPtr();
		std::string getStringValue();
		VariableReference getVariableReferenceValue();
		Variable* getVariablePtrValue();
		Variable& getVariableRefValue();
		Variable* deepCopy();
	};
}