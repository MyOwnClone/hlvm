#include "conditionnode.h"

namespace hlvm
{
	bool ConditionNode::execute()
	{
		return get_value()->execute(get_children());
	}

	ConditionNode* ConditionNode::copy()
	{
		auto result = new ConditionNode(get_value()->copy());

		result->set_parent(NULL);

		return result;
	}
}