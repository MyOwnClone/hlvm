#include "conditiontree.h"
#include "objectvariablereference.h"

namespace hlvm
{
	bool ConditionTree::execute()
	{
		if (!get_root())
		{
			throw ConditionException("root node is NULL!!!");
		}

		auto node = get_root()->get_value();
		auto children = get_root()->get_children();
		return node->execute(children);
	}

	ConditionTree::~ConditionTree()
	{
		traverseNodes([](hlvm::TreeNode<Condition*> *node){ delete node->get_value(); node->set_value(NULL); });
	}

	ConditionTree* ConditionTree::copy()
	{
		return (ConditionTree*)(Tree::copy());
	}

	void ConditionTree::addRelationConditionTo(TreeNode<Condition*>* node, std::vector<Variable*> variables, std::vector<RelationOperators> operators)
	{
		addRelationConditionTo((ConditionNode*)node, variables, operators);
	}

	void ConditionTree::addRelationConditionTo(ConditionNode* node, std::vector<Variable*> variables, std::vector<RelationOperators> operators)
	{
		auto result = new RelationCondition(variables, operators);

		node->set_value(result);
	}

	void ConditionTree::addLogicConditionTo(TreeNode<Condition*>* node, std::vector<ConditionNode*> conditionNodes, std::vector<LogicOperators> operators)
	{
		addLogicConditionTo((ConditionNode*)node, conditionNodes, operators);
	}

	void ConditionTree::addLogicConditionTo(ConditionNode* node, std::vector<ConditionNode*> conditionNodes, std::vector<LogicOperators> operators)
	{
		auto result = new LogicCondition(operators);

		for (auto i = 0; i < (int)conditionNodes.size(); i++)
		{
			node->get_children().push_back(conditionNodes[i]);
		}

		node->set_value(result);
	}

	void ConditionTree::addRelationConditionWithVariablesToRoot(ConditionTree* tree, std::vector<Variable*> variables, std::vector<RelationOperators> operators)
	{
		ConditionNode *root = new ConditionNode();
		tree->set_root(root);

		addRelationConditionTo(root, variables, operators);
	}

	void ConditionTree::addRelationConditionWithAnyToRoot(ConditionTree* tree, std::vector<boost::any> variables, std::vector<RelationOperators> operators)
	{
		std::vector<Variable*> tmp;

		for (auto i = 0; i < variables.size(); i++)
		{
			if (variables[i].type().name() == typeid(Variable*).name())
			{
				tmp.push_back(boost::any_cast<Variable*>(variables[i]));
			}
			else if (variables[i].type().name() == typeid(ObjectVariableReference).name())
			{
				auto tmpValue = boost::any_cast<ObjectVariableReference>(variables[i]);

				tmp.push_back(Variable::ptrFrom(tmpValue));
			}
			else if (variables[i].type().name() == typeid(ObjectVariableReference*).name())
			{
				auto tmpValue = boost::any_cast<ObjectVariableReference*>(variables[i]);

				tmp.push_back(Variable::ptrFrom(tmpValue));
			}
			else if (variables[i].type().name() == typeid(VariableListReference).name())
			{
				auto tmpValue = boost::any_cast<VariableListReference>(variables[i]);

				tmp.push_back(Variable::ptrFrom(tmpValue));
			}
			else if (variables[i].type().name() == typeid(VariableListReference*).name())
			{
				auto tmpValue = boost::any_cast<VariableListReference*>(variables[i]);

				tmp.push_back(Variable::ptrFrom(tmpValue));
			}
		}

		addRelationConditionWithVariablesToRoot(tree, tmp, operators);
	}
}