#pragma once

#include <string>

namespace hlvm
{
	enum class ReturnValueMeaning
	{
		NONE
	};

	struct ReturnValueInfo
	{
	public:
		std::string name;
		std::string typeName;
		ReturnValueMeaning meaning;
	};
}