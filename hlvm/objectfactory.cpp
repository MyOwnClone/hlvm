#include "objectfactory.h"

namespace hlvm
{
	Object ObjectFactory::getObject()
	{
		return Object();
	}

	bool ObjectFactory::addVariable(Object& obj, std::string name, Variable variable)
	{
		return obj.get_locals().addVariable(name, variable);
	}
}