#pragma once
#include <exception>
#include <string>

namespace hlvm
{
	class ExpressionException : public std::runtime_error
	{
	public:
		ExpressionException(std::string message) : std::runtime_error(message) {};
	};
}