#pragma once

#include <vector>
#include "expression.h"
#include "variablereference.h"
#include "typepriorities.h"

namespace hlvm
{
	class ExpressionDataNode : public Expression
	{
	protected:
		std::vector<boost::any> data;
	public:
		def_get_set(std::vector<boost::any>&, data);
		ExpressionDataNode(std::vector<Operator> operators)
		{
			this->operators = operators;
		}

		ExpressionDataNode(Operator operatorInst)
		{
			operators.push_back(operatorInst);
		}

		ExpressionDataNode()
		{

		}

		ExpressionDataNode(const ExpressionDataNode& orig)
		{
			this->operators = orig.operators;
		}

		boost::any execute(std::vector<TreeNode<Expression*>*> children, std::string highestPriorityTypeName);
		virtual ExpressionDataNode* copy();
		virtual ~ExpressionDataNode();
	};
}