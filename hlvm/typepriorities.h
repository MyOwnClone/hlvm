#pragma once

#include <unordered_map>
#include <string>

namespace hlvm
{
	class TypePriorities
	{
	private:
		static std::unordered_map<std::string, int> priorities;
		static std::unordered_map<std::string, int> fillPriorities();
	public:
		static int getPriority(std::string name)
		{
			return priorities[name];
		}

		static int isLowerPriority(std::string name0, std::string name1)
		{
			return priorities[name0] < priorities[name1];
		}
	};
};