#pragma once

namespace hlvm
{
	enum class Operator
	{
		PLUS,
		MINUS,
		MULTIPLY,
		DIVIDE,
		NONE
	};
}