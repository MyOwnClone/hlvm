#pragma once

#include <exception>
#include <string>

namespace hlvm
{
	class CommandException : public std::runtime_error
	{
	public:
		CommandException(std::string message) : std::runtime_error(message) {}
	};
}