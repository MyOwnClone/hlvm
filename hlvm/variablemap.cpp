#include "variablemap.h"
#include "variableexception.h"

namespace hlvm
{
	bool VariableMap::addVariable(std::string name, Variable variable)
	{
		if (map.find(name) == map.end())
		{
			map[name] = variable;

			return true;
		}
		else
		{
			return false;
		}
	}

	bool VariableMap::removeVariable(std::string name)
	{
		if (map.find(name) != map.end())
		{
			map.erase(map.find(name));

			return true;
		}
		else
		{
			return false;
		}
	}

	Variable* VariableMap::resolveVariableByName(std::string name, bool lookInParents)
	{
		if (map.find(name) != map.end())
		{
			auto result = map[name];

			if (result.value.type().name() == typeid(VariableReference).name())
			{
				auto ref0 = boost::any_cast<VariableReference>(result.value);

				auto name = ref0.get_name();
				auto vmap = ref0.get_map();

				return vmap->resolveVariableByName(name, lookInParents);
			}
			else
			{
				return &(map[name]);
			}
		}
		else
		{
			if (lookInParents && parent)
			{
				return parent->resolveVariableByName(name, lookInParents);
			}
			else
				throw VariableException();
		}
	}

	Variable& VariableMap::getVariableByName(std::string name, bool lookInParents)
	{
		if (map.find(name) != map.end())
		{
			return map[name];
		}
		else
		{
			if (lookInParents && parent)
			{
				return parent->getVariableByName(name, lookInParents);
			}
			else
				throw VariableException();
		}
	}

	std::vector<std::string> VariableMap::keysToVector()
	{
		std::vector<std::string> result;

		for (auto it = begin(); it != end(); it++)
		{
			result.push_back(it->first);
		}

		return result;
	}

	std::vector<boost::any> VariableMap::valuesToVector()
	{
		std::vector<boost::any> result;

		for (auto it = begin(); it != end(); it++)
		{
			result.push_back(it->second.value);
		}

		return result;
	}

	bool VariableMap::variableExists(std::string name, bool lookInParents)
	{
		auto exists = map.find(name) != map.end();

		if (exists)
		{
			return true;
		}
		else if (lookInParents && parent)
		{
			return parent->variableExists(name, lookInParents);
		}
		else
		{
			return false;
		}
	}

	VariableMap* VariableMap::deepCopy()
	{
		VariableMap* result = new VariableMap();

		// parent won't be copied
		result->set_parent(get_parent());

		for (auto it = map.begin(); it != map.end(); it++)
		{
			auto name = it->first;
			auto data = (it->second).deepCopy();

			result->map[name] = *data;
		}

		return result;
	}
}