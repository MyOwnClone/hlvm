#pragma once

#include <vector>
#include "boost/any.hpp"
#include "operator.h"
#include "expressionexception.h"
#include "main.h"
#include "treenode.h"

namespace hlvm
{
	class Expression
	{
	public:
		std::vector<Operator> operators;
	protected:
		boost::any executeInner(boost::any value0, boost::any value1, int operatorIndex, boost::any &result);
	public:
		def_get_set(std::vector<Operator>&, operators);
		virtual boost::any execute(std::vector<TreeNode<Expression*>*> children, std::string highestPriorityTypeName);
		virtual Expression* copy();
	};
}