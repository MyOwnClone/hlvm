#pragma once

#include "boost/any.hpp"
#include "command.h"
#include "commandexception.h"
#include "variablereference.h"
#include "expressiontree.h"

namespace hlvm
{
	class AssignVariableCommand : public Command
	{
	protected:
		boost::any target;
		boost::any source;
	public:
		AssignVariableCommand(boost::any target, boost::any source) : target(target), source(source)
		{

		}
		virtual bool execute();
		virtual AssignVariableCommand* copy();		
		virtual AssignVariableCommand* deepCopy();
	};
}