#pragma once

#include "block.h"

namespace hlvm
{
	class BlockFactory
	{
	public:
		static Block getBlock();
		static void addCommand(Block& block, Command* command);
	};
}