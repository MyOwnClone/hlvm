#pragma once

#include "variablemap.h"
#include "main.h"
#include "mutable.h"

namespace hlvm
{
	class Block;
	class Command : public Mutable
	{
	protected:
		VariableMap *variableMap = NULL;
		Block *blockParent;
	public:
		Command()
		{
			variableMap = NULL;
			blockParent = NULL;
		}

		bool virtual execute();
		virtual Command* copy();
		virtual Command* deepCopy();

		def_get_set(VariableMap *, variableMap);
		def_get_set(Block*, blockParent);
	};
}