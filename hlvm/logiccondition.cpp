#include "logiccondition.h"
#include "conditionexception.h"
#include "conditionnode.h"

namespace hlvm
{
	bool LogicCondition::execute(std::vector<TreeNode<Condition*>*> children)
	{
		if (operators.size() != children.size() - 1)
		{
			throw ConditionException("Number of operators should be equal to number of children minus one!!!");
		}

		bool matches = true;

		for (auto i = 0; i < (int)children.size(); i += 2)
		{
			int operatorIndex = i / 2;

			auto result0 = ((ConditionNode*)children[i])->execute();
			auto result1 = ((ConditionNode*)children[i + 1])->execute();

			switch (operators[operatorIndex])
			{
			case LogicOperators::AND:
				matches = result0 && result1;
				break;
			case LogicOperators::OR:
				matches = result0 || result1;
				break;
			default:
				throw ConditionException("Unknown LogicOperators");
			}

			if (!matches)
			{
				return false;
			}
		}

		return true;
	}

	LogicCondition* LogicCondition::copy()
	{
		auto result = new LogicCondition();

		result->operators = operators;

		return result;
	}

	LogicCondition::~LogicCondition()
	{

	}
}