#include "assignvariablecommand.h"
#include "resolver.h"

namespace hlvm
{
	bool AssignVariableCommand::execute()
	{
		Variable *realVariableTarget = NULL;
		Variable *realVariableSource = NULL;

		realVariableTarget = Resolver::resolveVariableReferenceAndExecuteToPtr(target);

		if (realVariableTarget == NULL)
		{
			throw CommandException("Target is not VariableReference neither Variable");
		}

		realVariableSource = Resolver::resolveVariableReferenceAndExecuteToPtr(source);

		if (realVariableSource == NULL)
		{
			throw CommandException("Target is not VariableReference neither Variable");
		}

		realVariableTarget->value = boost::any(realVariableSource->value);

		if (realVariableTarget->get_dispose())
		{
			delete realVariableTarget;
		}

		if (realVariableSource->get_dispose())
		{
			delete realVariableSource;
		}

		return true;
	}

	AssignVariableCommand* AssignVariableCommand::copy()
	{
		AssignVariableCommand* result = new AssignVariableCommand(target, source);
		return result;
	}

	AssignVariableCommand* AssignVariableCommand::deepCopy()
	{
		if (target.type().name() == typeid(VariableReference).name())
		{
			auto ref0 = boost::any_cast<VariableReference>(target);

			auto vmapCopy = ref0.get_map()->deepCopy();

			vmapCopy->set_parent(ref0.get_map()->get_parent());

			target = hlvm::VariableReference(ref0.get_name(), vmapCopy); //FIX: this creates copy of source variable map each time deepCopy is called
		}

		if (source.type().name() == typeid(VariableReference).name())
		{
			auto ref0 = boost::any_cast<VariableReference>(source);

			auto vmapCopy = ref0.get_map()->deepCopy();
			vmapCopy->set_parent(ref0.get_map()->get_parent());

			source = hlvm::VariableReference(ref0.get_name(), vmapCopy); //FIX: this creates copy of source variable map each time deepCopy is called
		}

		AssignVariableCommand* result = new AssignVariableCommand(target, source);
		return result;
	}
}