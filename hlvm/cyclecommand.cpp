#include "cyclecommand.h"

namespace hlvm
{
	bool CycleCommand::execute()
	{
		while (conditionTree->execute())
		{
			block.execute();
		}

		return true;
	}

	CycleCommand* CycleCommand::copy()
	{
		CycleCommand* result = new CycleCommand(*block.copy(), conditionTree->copy());
		return result;
	}
}