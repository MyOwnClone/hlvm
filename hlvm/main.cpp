#include <iostream>

#include "variable.h"
#include "variablemap.h"
#include "logicoperators.h"
#include "variableexception.h"
#include "treenode.h"
#include "tree.h"
#include "conditiontree.h"
#include "expressiontree.h"
#include "variablereference.h"
#include "expressiontree.h"
#include "expressionnode.h"
#include "expressiondatanode.h"
#include "logiccondition.h"
#include "relationcondition.h"
#include "command.h"
#include "assignvariablecommand.h"
#include "assignexpressioncommand.h"
#include "block.h"
#include "conditionalcommand.h"
#include "cyclecommand.h"
#include "guard.h"
#include "variablelistreference.h"

int main(int argc, char **argv)
{
	hlvm::VariableMap map0;
	hlvm::VariableMap map1;

	hlvm::Variable result0;
	
	try {
		result0 = map0.getVariableByName("var0");
	}
	catch (hlvm::VariableException)
	{
		std::cout << "var0 does not exists." << std::endl;
	}

	hlvm::Variable var;
	var.value = 10;

	if (!map0.addVariable("var0", var))
	{
		std::cout << "failed to add var0" << std::endl;
	}

	if (!map0.addVariable("var0", var))
	{
		std::cout << "failed to add var0" << std::endl;
	}

	std::cout << "type name: " << var.value.type().name() << std::endl;
	std::cout << "is typeid int? " << (var.value.type() == typeid(int)) << std::endl;

	auto result1 = map0.getVariableByName("var0");

	std::cout << boost::any_cast<int>(result1.value) << std::endl;

	map1 = map0;

	map1.getVariableByName("var0").value = 11;

	auto result2 = map1.getVariableByName("var0");
	std::cout << boost::any_cast<int>(result2.value) << std::endl;
	result1 = map0.getVariableByName("var0");
	std::cout << boost::any_cast<int>(result1.value) << std::endl;

	map1.removeVariable("var0");

	if (map0.getVariableByName("var0").isNull())
	{
		std::cout << "removed" << std::endl;
	}
	else
	{
		std::cout << "not removed" << std::endl;
	}

	for (auto it = map0.begin(); it != map0.end(); it++)
	{
		std::cout << it->first << ":" << boost::any_cast<int>(it->second.value) << std::endl;
	}

	auto keys = map0.keysToVector();
	auto values = map0.valuesToVector();

    for (auto it = keys.begin(); it != keys.end(); it++)
	{
		std::cout << *it << std::endl;
	}

	 for (auto it = values.begin(); it != values.end(); it++)	{
		std::cout << boost::any_cast<int>(var) << std::endl;
	}

	// tree testing

	hlvm::TreeNode<int>* root	= new hlvm::TreeNode<int>(0);
	hlvm::TreeNode<int>* left	= new hlvm::TreeNode<int>(1);
	hlvm::TreeNode<int>* right	= new hlvm::TreeNode<int>(2);

	root->get_children().push_back(left);
	root->get_children().push_back(right);

	left->set_parent(root);
	right->set_parent(root);

	std::auto_ptr<hlvm::Tree<int>> dynamicTree (new hlvm::Tree<int>());
	dynamicTree->set_root(root);

	dynamicTree->traverseValues([](int i) { std::cout << i << std::endl; });
	dynamicTree->traverseNodes([](hlvm::TreeNode<int> *node) { std::cout << node->get_value() << std::endl; });

	// condition trees
	
	std::auto_ptr<hlvm::ConditionTree> ctree (new hlvm::ConditionTree());
	
	hlvm::ConditionNode *croot = new hlvm::ConditionNode(new hlvm::LogicCondition(hlvm::LogicOperators::AND));
	ctree->set_root(croot);
	croot->get_children().push_back(new hlvm::ConditionNode(new hlvm::RelationCondition(
	{ 
		hlvm::Variable::ptrFrom(0), 
		hlvm::Variable::ptrFrom(1) 
	}, 
	{ 
		hlvm::RelationOperators::LOWER_OR_EQUAL
	})));

	croot->get_children().push_back(new hlvm::ConditionNode(new hlvm::RelationCondition(
	{ 
		hlvm::Variable::ptrFrom(0), 
		hlvm::Variable::ptrFrom(0) 
	}, 
	{ 
		hlvm::RelationOperators::EQUAL
	})));

	auto dynamicTreeResult0 = croot->execute();

	std::auto_ptr<hlvm::ConditionTree> ctreeCopy(ctree->copy());
	
	auto dynamicTreeResult1 = ctreeCopy->execute();

	// expression trees

	hlvm::VariableMap expressionTreeMap;

	expressionTreeMap.addVariable("var0", hlvm::Variable::from(2));

	hlvm::ExpressionTree *etree0 = new hlvm::ExpressionTree();
	hlvm::ExpressionNode *eroot0 = new hlvm::ExpressionNode(new hlvm::ExpressionDataNode());
	eroot0->getData().push_back(hlvm::Variable::from(1));
	eroot0->getData().push_back(hlvm::VariableReference("var0", &expressionTreeMap));
	eroot0->getOperators().push_back(hlvm::Operator::PLUS);

	etree0->set_root(eroot0);

	std::cout << boost::any_cast<int>(etree0->execute()) << std::endl;

	delete etree0;

	hlvm::VariableMap expressionTreeMap2;
	expressionTreeMap2.addVariable("var0", hlvm::Variable::from(2));
	expressionTreeMap2.addVariable("var1", hlvm::Variable::from(1));

	hlvm::ExpressionTree *etree1 = new hlvm::ExpressionTree();
	hlvm::ExpressionNode *eroot1 = new hlvm::ExpressionNode(new hlvm::ExpressionProxyNode());
	eroot1->getOperators().push_back(hlvm::Operator::MINUS);

	hlvm::ExpressionDataNode *eleft1 = new hlvm::ExpressionDataNode();
	hlvm::ExpressionDataNode *eright1 = new hlvm::ExpressionDataNode();

	eleft1->get_data().push_back(hlvm::Variable::from(1));
	eleft1->get_data().push_back(hlvm::VariableReference("var0", &expressionTreeMap2));
	eleft1->get_operators().push_back(hlvm::Operator::PLUS);

	eright1->get_data().push_back(hlvm::Variable::from(1));
	eright1->get_data().push_back(hlvm::VariableReference("var1", &expressionTreeMap2));
	eright1->get_operators().push_back(hlvm::Operator::MULTIPLY);

	eroot1->get_children().push_back(new hlvm::ExpressionNode(eleft1));
	eroot1->get_children().push_back(new hlvm::ExpressionNode(eright1));

	etree1->set_root(eroot1);

	std::cout << boost::any_cast<int>(etree1->execute()) << std::endl;

	delete etree1;

	{
		// condition trees with references
		hlvm::VariableMap map;
		map.addVariable("var0", hlvm::Variable::from(0));
		map.addVariable("var1", hlvm::Variable::from(1));
		map.addVariable("var2", hlvm::Variable::from(0));
		map.addVariable("var3", hlvm::Variable::from(0));

		std::auto_ptr<hlvm::ConditionTree> ctree(new hlvm::ConditionTree());

		hlvm::ConditionNode *croot = new hlvm::ConditionNode(new hlvm::LogicCondition(hlvm::LogicOperators::AND));
		ctree->set_root(croot);
		croot->get_children().push_back(new hlvm::ConditionNode(new hlvm::RelationCondition(
		{
			hlvm::Variable::ptrFrom(hlvm::VariableReference("var0", &map)),
			hlvm::Variable::ptrFrom(hlvm::VariableReference("var1", &map))
		},
		{
			hlvm::RelationOperators::LOWER_OR_EQUAL
		})));

		croot->get_children().push_back(new hlvm::ConditionNode(new hlvm::RelationCondition(
		{
			hlvm::Variable::ptrFrom(hlvm::VariableReference("var2", &map)),
			hlvm::Variable::ptrFrom(hlvm::VariableReference("var3", &map)),
		},
		{
			hlvm::RelationOperators::EQUAL
		})));

		auto dynamicTreeResult0 = croot->execute();
	}

	{
		// expression tree with different types
		hlvm::VariableMap expressionTreeMap;

		expressionTreeMap.addVariable("var0", hlvm::Variable::from(2.1f));

		hlvm::ExpressionTree *etree0 = new hlvm::ExpressionTree();
		hlvm::ExpressionNode *eroot0 = new hlvm::ExpressionNode(new hlvm::ExpressionDataNode());
		eroot0->getData().push_back(hlvm::Variable::from(1));
		eroot0->getData().push_back(hlvm::VariableReference("var0", &expressionTreeMap));
		eroot0->getOperators().push_back(hlvm::Operator::PLUS);

		etree0->set_root(eroot0);

		std::cout << boost::any_cast<double>(etree0->execute()) << std::endl;

		delete etree0;
	}

	{
		// etree

		// expression tree with different types
		hlvm::VariableMap expressionTreeMap;

		expressionTreeMap.addVariable("var0", hlvm::Variable::from(2.1f));

		hlvm::ExpressionTree *etree0 = new hlvm::ExpressionTree();
		hlvm::ExpressionNode *eroot0 = new hlvm::ExpressionNode(new hlvm::ExpressionDataNode());
		eroot0->getData().push_back(hlvm::Variable::from(1));
		eroot0->getData().push_back(hlvm::VariableReference("var0", &expressionTreeMap));
		eroot0->getOperators().push_back(hlvm::Operator::PLUS);

		etree0->set_root(eroot0);

		std::cout << boost::any_cast<double>(etree0->execute()) << std::endl;

		hlvm::ExpressionTree *etree1 = etree0->copy();

		//delete etree0;

		expressionTreeMap.getVariableByName("var0").value = 1.1f;
		eroot0->getData()[0] = hlvm::Variable::from(0);

		std::cout << boost::any_cast<float>(etree1->execute()) << std::endl;
		std::cout << boost::any_cast<float>(etree0->execute()) << std::endl;

		delete etree1;
	}

	{
		// command test

		hlvm::VariableMap expressionTreeMap;

		expressionTreeMap.addVariable("var0", hlvm::Variable::from(0));

		hlvm::AssignVariableCommand *command0 = new hlvm::AssignVariableCommand(boost::any(hlvm::VariableReference("var0", &expressionTreeMap)), boost::any(hlvm::Variable::from(111)));

		command0->execute();

		std::cout << expressionTreeMap.getVariableByName("var0").getIntValue() << std::endl;
	}

	{
		// command test

		hlvm::VariableMap expressionTreeMap;

		expressionTreeMap.addVariable("var0", hlvm::Variable::from(2.1f));
		expressionTreeMap.addVariable("result", hlvm::Variable::from(0.0f));

		hlvm::ExpressionTree *etree0 = new hlvm::ExpressionTree();
		hlvm::ExpressionNode *eroot0 = new hlvm::ExpressionNode(new hlvm::ExpressionDataNode());
		eroot0->getData().push_back(hlvm::Variable::from(1));
		eroot0->getData().push_back(hlvm::VariableReference("var0", &expressionTreeMap));
		eroot0->getOperators().push_back(hlvm::Operator::PLUS);

		etree0->set_root(eroot0);

		hlvm::AssignExpressionCommand *command0 = new hlvm::AssignExpressionCommand(hlvm::VariableReference("result", &expressionTreeMap), etree0);
		command0->execute();

		std::cout << expressionTreeMap.getVariableByName("result").getDoubleValue() << std::endl;
	}

	{
		// block test

		hlvm::VariableMap expressionTreeMap;

		expressionTreeMap.addVariable("var0", hlvm::Variable::from(0));

		// add 1 to var0
		hlvm::ExpressionTree *etree0 = new hlvm::ExpressionTree();
		hlvm::ExpressionNode *eroot0 = new hlvm::ExpressionNode(new hlvm::ExpressionDataNode());
		eroot0->getData().push_back(hlvm::VariableReference("var0", &expressionTreeMap));
		eroot0->getData().push_back(hlvm::Variable::from(1));
		eroot0->getOperators().push_back(hlvm::Operator::PLUS);
		etree0->set_root(eroot0);

		// add 1 to var0
		hlvm::ExpressionTree *etree1 = new hlvm::ExpressionTree();
		hlvm::ExpressionNode *eroot1 = new hlvm::ExpressionNode(new hlvm::ExpressionDataNode());
		eroot1->getData().push_back(hlvm::VariableReference("var0", &expressionTreeMap));
		eroot1->getData().push_back(hlvm::Variable::from(1));
		eroot1->getOperators().push_back(hlvm::Operator::PLUS);
		etree1->set_root(eroot1);

		hlvm::AssignExpressionCommand *command0 = new hlvm::AssignExpressionCommand(hlvm::VariableReference("var0", &expressionTreeMap), etree0);
		hlvm::AssignExpressionCommand *command1 = new hlvm::AssignExpressionCommand(hlvm::VariableReference("var0", &expressionTreeMap), etree1);

		hlvm::Block block;
		block.get_commands().push_back(command0);
		block.get_commands().push_back(command1);

		block.execute();

		std::cout << expressionTreeMap.getVariableByName("var0").getIntValue() << std::endl;
	}

	{
		// block test

		hlvm::VariableMap expressionTreeMap;

		expressionTreeMap.addVariable("var0", hlvm::Variable::from(0));

		// add 1 to var0
		hlvm::ExpressionTree *etree0 = new hlvm::ExpressionTree();
		hlvm::ExpressionNode *eroot0 = new hlvm::ExpressionNode(new hlvm::ExpressionDataNode());
		eroot0->getData().push_back(hlvm::VariableReference("var0", &expressionTreeMap));
		eroot0->getData().push_back(hlvm::Variable::from(1));
		eroot0->getOperators().push_back(hlvm::Operator::PLUS);
		etree0->set_root(eroot0);

		hlvm::AssignExpressionCommand *command0 = new hlvm::AssignExpressionCommand(hlvm::VariableReference("var0", &expressionTreeMap), etree0);

		hlvm::Block block;
		block.get_commands().push_back(command0);

		hlvm::ConditionTree* conditionTree = new hlvm::ConditionTree();

		conditionTree->set_root(new hlvm::ConditionNode(new hlvm::RelationCondition(
		{
			hlvm::Variable::ptrFrom(hlvm::VariableReference("var0", &expressionTreeMap)),
			hlvm::Variable::ptrFrom(1)
		},
		{
			hlvm::RelationOperators::EQUAL
		})));

		hlvm::Block innerBlock;

		// add 1 to var0
		hlvm::ExpressionTree *etree1 = new hlvm::ExpressionTree();
		hlvm::ExpressionNode *eroot1 = new hlvm::ExpressionNode(new hlvm::ExpressionDataNode());
		eroot1->getData().push_back(hlvm::VariableReference("var0", &expressionTreeMap));
		eroot1->getData().push_back(hlvm::Variable::from(1));
		eroot1->getOperators().push_back(hlvm::Operator::PLUS);
		etree1->set_root(eroot1);

		hlvm::AssignExpressionCommand *commandInner = new hlvm::AssignExpressionCommand(hlvm::VariableReference("var0", &expressionTreeMap), etree1);

		innerBlock.get_commands().push_back(commandInner);

		hlvm::ConditionalCommand *command1 = new hlvm::ConditionalCommand(innerBlock, conditionTree);

		block.get_commands().push_back(command1);

		block.execute();

		std::cout << expressionTreeMap.getVariableByName("var0").getIntValue() << std::endl;
	}

	{
		// cycle command test

		hlvm::VariableMap expressionTreeMap;

		expressionTreeMap.addVariable("var0", hlvm::Variable::from(0));

		hlvm::ConditionTree* conditionTree = new hlvm::ConditionTree();

		conditionTree->set_root(new hlvm::ConditionNode(new hlvm::RelationCondition(
		{
			hlvm::Variable::ptrFrom(hlvm::VariableReference("var0", &expressionTreeMap)),
			hlvm::Variable::ptrFrom(5)
		},
		{
			hlvm::RelationOperators::LOWER_OR_EQUAL
		})));

		// add 1 to var0
		hlvm::ExpressionTree *etree0 = new hlvm::ExpressionTree();
		hlvm::ExpressionNode *eroot0 = new hlvm::ExpressionNode(new hlvm::ExpressionDataNode());
		eroot0->getData().push_back(hlvm::VariableReference("var0", &expressionTreeMap));
		eroot0->getData().push_back(hlvm::Variable::from(1));
		eroot0->getOperators().push_back(hlvm::Operator::PLUS);
		etree0->set_root(eroot0);

		hlvm::AssignExpressionCommand *commandInner = new hlvm::AssignExpressionCommand(hlvm::VariableReference("var0", &expressionTreeMap), etree0);

		hlvm::Block innerBlock;
		innerBlock.get_commands().push_back(commandInner);

		hlvm::CycleCommand *cycleCommand = new hlvm::CycleCommand(innerBlock, conditionTree);

		cycleCommand->execute();

		std::cout << expressionTreeMap.getVariableByName("var0").getIntValue() << std::endl;
		
		hlvm::CycleCommand *cycleCommand1 = cycleCommand->copy();

		delete cycleCommand;

		expressionTreeMap.getVariableByName("var0").value = 0;

		cycleCommand1->execute();

		std::cout << expressionTreeMap.getVariableByName("var0").getIntValue() << std::endl;
	}
	
	return 0;
}