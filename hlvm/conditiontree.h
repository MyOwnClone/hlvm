#pragma once

#include "tree.h"
#include "conditionnode.h"
#include "logiccondition.h"

namespace hlvm
{
	class ConditionTree : public Tree<Condition*>
	{
	public:
		ConditionTree()
		{
			set_root(NULL);
		}

		bool execute();
		virtual ~ConditionTree();
		virtual ConditionTree* copy();
		static void addRelationConditionWithVariablesToRoot(ConditionTree* tree, std::vector<Variable*> variables, std::vector<RelationOperators> operators);
		static void addRelationConditionWithAnyToRoot(ConditionTree* tree, std::vector<boost::any> variables, std::vector<RelationOperators> operators);
		static void addRelationConditionTo(TreeNode<Condition*>* node, std::vector<Variable*> variables, std::vector<RelationOperators> operators);
		static void addRelationConditionTo(ConditionNode* node, std::vector<Variable*> variables, std::vector<RelationOperators> operators);
		static void addLogicConditionTo(TreeNode<Condition*>* node, std::vector<ConditionNode*> conditionNodes, std::vector<LogicOperators> operators);
		static void addLogicConditionTo(ConditionNode* node, std::vector<ConditionNode*> conditionNodes, std::vector<LogicOperators> operators);
	};
}