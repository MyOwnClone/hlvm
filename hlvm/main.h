#pragma once

// getter for class attr's
#define def_get(type, attr) type get_##attr(void) { return attr; }
#define def_s_get(type, attr) static type get_##attr(void) { return attr; }
// setter for class attr's
#define def_set(type, attr) void set_##attr(type _##attr) { attr = _##attr; }
#define def_s_set(type, attr) static void set_##attr(type _##attr) { attr = _##attr; }

// setter's and getter's
#define def_get_set(type, attr) def_get(type, attr); \
	def_set(type, attr)

#define def_s_get_set(type, attr) def_s_get(type, attr); \
	def_s_set(type, attr)

#define PRINTLOCATION() \
	cout << "Details: " << endl; \
	cout << "File: " << string(__FILE__) << " Line: " << __LINE__ << " Function: " << string(__FUNCDNAME__) << endl;