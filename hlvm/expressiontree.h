#pragma once

#include "tree.h"
#include "treenode.h"
#include "expressionnode.h"
#include "expressiondatanode.h"
#include "expressionproxynode.h"
#include "main.h"

namespace hlvm
{
	class ExpressionTree : public Tree<Expression*>
	{
	protected:
		static int counter;
		int id = -1;
		const char* processVariableListReference(boost::any data);
		std::string getHighestPriorityTypeName(TreeNode<Expression*> *node = NULL);
	public:
		ExpressionTree()
		{
			id = counter++;
			set_root(NULL);
		}

		def_get(int, id);

		boost::any execute();
		virtual ~ExpressionTree();
		virtual ExpressionTree* copy();
		static void addExpressionDataNode(ExpressionNode *node, std::vector<boost::any> data, std::vector<Operator> operators);
		static void addExpressionDataNodeToRoot(ExpressionTree *tree, std::vector<boost::any> data, std::vector<Operator> operators);
		static void addExpressionProxyNode(ExpressionNode *node, std::vector<ExpressionNode*> children, std::vector<Operator> operators);
		static void addNode(ExpressionNode *node, std::vector<boost::any> data, std::vector<Operator> operators);
		static void addNode(ExpressionNode *node, std::vector<ExpressionNode*> children, std::vector<Operator> operators);		
	};
}