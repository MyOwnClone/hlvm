#pragma once

#include <unordered_map>
#include "function.h"
#include "variablemap.h"
#include "main.h"

namespace hlvm
{
    typedef std::unordered_map<std::string, Function> StringFunctionMap_t;
    
	class Object
	{
	private:
		int id = -1;
		static int counter;
	protected:
		VariableMap locals;
		std::unordered_map<std::string, Function> functions;
	public:
		Object()
		{
			id = counter++;
		}

		Object(const Object& orig)
		{
			this->id		= orig.id;
			this->locals	= orig.locals;
			this->functions	= orig.functions;
		}

		Object& operator=(const Object& orig)
		{
			this->id = orig.id;
			this->locals = orig.locals;
			this->functions = orig.functions;

			return *this;
		}

		def_get(int, id);
		def_get(VariableMap&, locals);
		def_get(StringFunctionMap_t&, functions);

		bool addLocalVariable(std::string name, Variable variable);
		bool addFunction(std::string name, Function &function);
		Function& getFunctionReferenceByName(std::string name);
		Function getFunctionByName(std::string name);
		Variable& getVariableReferenceByName(std::string name);
	};
}