#include "resolver.h"
#include "variablemap.h"
#include "expressiontree.h"
#include "variableexception.h"
#include "variablelist.h"
#include "variablelistreference.h"
#include "object.h"
#include "objectvariablereference.h"
#include "objectfunctionreference.h"
#include "functionptrcallcommand.h"

namespace hlvm
{
	Variable* Resolver::resolveVariableReferenceToPtr(VariableReference reference)
	{
		if (reference.get_map())
		{
			auto name = reference.get_name();
			auto map = reference.get_map();
			auto result = map->getVariableByName(name);

			if (result.value.type().name() == typeid(VariableReference).name())
			{
				return resolveVariableReferenceToPtr(boost::any_cast<VariableReference>(result.value));
			}
			else if (result.value.type().name() == typeid(VariableReference*).name())
			{
				return resolveVariableReferenceToPtr(boost::any_cast<VariableReference*>(result.value));
			}
			else if (result.value.type().name() == typeid(Variable*).name())
			{
				return boost::any_cast<Variable*>(result.value);
			}
			else if (result.value.type().name() == typeid(Variable).name())
			{
                auto tmp = Variable::ptrFromAny(result.value)->setDispose(true);
				return tmp;
			}
			else if (result.value.type().name() == typeid(ExpressionTree).name())
			{
				auto tmp = new Variable();
				tmp->value = boost::any_cast<ExpressionTree>(result.value).execute();
				return tmp;
			}
			else if (result.value.type().name() == typeid(ExpressionTree*).name())
			{
				auto tmp = new Variable();
				tmp->value = boost::any_cast<ExpressionTree*>(result.value)->execute();
				return tmp;
			}
			else if (result.value.type().name() == typeid(VariableListReference).name())
			{
				auto ref0 = boost::any_cast<VariableListReference>(result.value);

				auto list = ref0.get_list();
				auto index = ref0.get_index();
				return &((*list).getByIndex(index));
			}
			else
			{
				return &(map->getVariableByName(name));
			}
		}
		else
		{
			throw VariableException("map stored in reference is null!!!");
		}
	}

	Variable* Resolver::resolveVariableReferenceToPtr(VariableReference* reference)
	{
		if (reference->get_map())
		{
			return resolveVariableReferenceToPtr(*reference);
		}
		else
		{
			throw VariableException("map stored in reference is null!!!");
		}
	}

	Variable* Resolver::resolveVariableReferenceToPtr(boost::any reference)
	{
		if (reference.type().name() == typeid(VariableReference).name())
		{
			return resolveVariableReferenceToPtr(boost::any_cast<VariableReference>(reference));
		}
		else if (reference.type().name() == typeid(VariableReference*).name())
		{
			return resolveVariableReferenceToPtr(boost::any_cast<VariableReference*>(reference));
		}
		else if (reference.type().name() == typeid(ObjectVariableReference).name())
		{
			auto ref0 = boost::any_cast<ObjectVariableReference>(reference);
			auto name = ref0.get_name();
			auto obj = ref0.get_object();

			return resolveVariableReferenceAndExecuteToPtr(obj->get_locals().getVariableByName(name));
		}
		else if (reference.type().name() == typeid(ObjectFunctionReference).name())
		{
			auto ref0 = boost::any_cast<ObjectFunctionReference>(reference);
			auto name = ref0.get_name();
			auto obj = ref0.get_object();

			auto result = Variable::ptrFromAny(boost::any(obj->getFunctionReferenceByName(name)));

			return result->setDispose(true);
		}
		else
		{
			return NULL;
		}
	}

	Variable* Resolver::processObjectVariableReferenceToPtr(Object *object, std::string name, int index)
	{
		if (index == -1)
		{
			return &(object->get_locals().getVariableByName(name));
		}
		else
		{
			auto ref1 = object->getVariableReferenceByName(name);

			if (ref1.value.type().name() == typeid(VariableList).name())
			{
				auto ref2 = boost::any_cast<VariableList>(ref1.value);

				return Variable::ptrFrom(ref2.getByIndex(index).value);
			}
			else if (ref1.value.type().name() == typeid(VariableList*).name())
			{
				auto ref2 = boost::any_cast<VariableList*>(ref1.value);

				return Variable::ptrFrom(ref2->getByIndex(index).value);
			}
			else
				throw ExpressionException("ObjectVariableReference has index set, but target variable is not list!!!");
		}
	}

	Variable* Resolver::resolve(boost::any reference, int index)
	{
		return resolveVariableReferenceAndExecuteToPtr(reference, index);
	}

	Variable* Resolver::resolveVariableReferenceAndExecuteToPtr(boost::any reference, int index)
	{
		Variable* tmp = NULL; 

		// in case that reference is Variable, process it and return immediately
		if (reference.type().name() == typeid(Variable).name())
		{
			// create new variable from old variable which is stored in reference
			tmp = (new Variable(boost::any_cast<Variable>(reference)))->setDispose(true); // let caller know to release 

			// if tmp is VariableList
			if (tmp->value.type().name() == typeid(VariableList).name() && index != -1)
			{
				// extract list
				auto list0 = boost::any_cast<VariableList>(tmp->value);

				delete tmp;

				// get item from list by index and process it
				return resolveVariableReferenceAndExecuteToPtr(list0.getByIndex(index));
			}
			else if (tmp->value.type().name() == typeid(VariableList*).name() && index != -1)
			{
				auto list0 = boost::any_cast<VariableList*>(tmp->value);

				delete tmp;

				return resolveVariableReferenceAndExecuteToPtr(list0->getByIndex(index));
			}
		}
		else if (reference.type().name() == typeid(VariableList*).name())
		{
			return Variable::ptrFrom(boost::any_cast<VariableList*>(reference))->setDispose(true);
		}
		else if (reference.type().name() == typeid(VariableList).name())
		{
			return Variable::ptrFrom(reference)->setDispose(true);
		}
		// if reference is call command to function
		else if (reference.type().name() == typeid(FunctionPtrCallCommand*).name())
		{
			// extract reference
			auto ref = boost::any_cast<FunctionPtrCallCommand*>(reference);

			// execute function
			ref->execute();

			// get return value
			auto tmpResult = ref->getFunctionToCall()->get_returnValue();

			// create variable from return value
			return Variable::ptrFromAny(tmpResult)->setDispose(true);
		}
		// reference is reference to variable in object
		else if (reference.type().name() == typeid(ObjectVariableReference).name())
		{
			// extract reference
			auto ref0 = boost::any_cast<ObjectVariableReference>(reference);
			// get name of variable
			auto name = ref0.get_name();
			// extract target object
			auto obj = ref0.get_object();
			auto index = ref0.get_index();

			// get variable from object
			return processObjectVariableReferenceToPtr(obj, name, index);
		}
		else if (reference.type().name() == typeid(ObjectVariableReference*).name())
		{
			// extract reference
			auto ref0 = boost::any_cast<ObjectVariableReference*>(reference);
			// get name of variable
			auto name = ref0->get_name();
			// extract target object
			auto obj = ref0->get_object();
			auto index = ref0->get_index();

			// get variable from object
			return processObjectVariableReferenceToPtr(obj, name, index);
		}
		// reference is pointer to Variable
		else if (reference.type().name() == typeid(Variable*).name())
		{
			tmp = boost::any_cast<Variable*>(reference);
		}

		if (tmp)
		{
			if (tmp->value.type().name() == typeid(VariableReference).name())
			{
				tmp = resolveVariableReferenceToPtr(boost::any_cast<VariableReference>(tmp->value));
			}
			else if (tmp->value.type().name() == typeid(VariableReference*).name())
			{
				tmp = resolveVariableReferenceToPtr(boost::any_cast<VariableReference*>(tmp->value));
			}
			else if (tmp->value.type().name() == typeid(VariableListReference).name())
			{
				return processVariableListReferenceToPtr(tmp->value);
			}
			else if (tmp->value.type().name() == typeid(ObjectVariableReference).name())
			{
				// extract reference
				auto ref0 = boost::any_cast<ObjectVariableReference>(tmp->value);
				// get name of variable
				auto name = ref0.get_name();
				// extract target object
				auto obj = ref0.get_object();
				auto index = ref0.get_index();

				// get variable from object
				return processObjectVariableReferenceToPtr(obj, name, index);
			}
			else if (tmp->value.type().name() == typeid(ObjectVariableReference*).name())
			{
				// extract reference
				auto ref0 = boost::any_cast<ObjectVariableReference*>(tmp->value);
				// get name of variable
				auto name = ref0->get_name();
				// extract target object
				auto obj = ref0->get_object();
				auto index = ref0->get_index();

				// get variable from object
				return processObjectVariableReferenceToPtr(obj, name, index);
			}
		}
		else
		{
			// tmp is NULL
			if (reference.type().name() == typeid(VariableReference).name())
			{
				auto tmpInner = boost::any_cast<VariableReference>(reference);

				if (tmpInner.get_empty())
				{
					return NULL;
				}

				tmp = resolveVariableReferenceToPtr(reference);
			}
			else if (reference.type().name() == typeid(VariableReference*).name())
			{
				tmp = resolveVariableReferenceToPtr(reference);
			}
		}
		
		if (tmp)
		{
			Variable* innerTmp = NULL;

			if (tmp->value.type().name() == typeid(ExpressionTree*).name())
			{
				innerTmp = new Variable(); 
				innerTmp->set_dispose(true); // let caller know that he should release it
				innerTmp->value = (boost::any_cast<ExpressionTree*>(tmp->value))->execute();
			}
			else if (tmp->value.type().name() == typeid(ExpressionTree).name())
			{
				innerTmp = new Variable(); 
				innerTmp->set_dispose(true); // let caller know that he should release it
				innerTmp->value = boost::any_cast<ExpressionTree>(tmp->value).execute();
			}
			else
			{
				return tmp;
			}

			return innerTmp;
		}
		else
		{
			// tmp is NULL
			if (reference.type().name() == typeid(ExpressionTree*).name())
			{
				tmp = new Variable(); // leak
				tmp->set_dispose(true); // let caller know that he should release it
				tmp->value = (boost::any_cast<ExpressionTree*>(reference))->execute();
			}
			else if (reference.type().name() == typeid(ExpressionTree).name())
			{
				tmp = new Variable(); // leak
				tmp->set_dispose(true); // let caller know that he should release it
				tmp->value = boost::any_cast<ExpressionTree>(reference).execute();
			}

			if (reference.type().name() == typeid(VariableListReference).name())
			{
				return processVariableListReferenceToPtr(reference);
			}

			return tmp;
		}		
	}

	Variable* Resolver::processVariableListReferenceToPtr(boost::any reference)
	{
		auto ref0	= boost::any_cast<VariableListReference>(reference);

		auto list	= ref0.get_list();
		auto index	= ref0.get_index();
		auto name	= ref0.get_name();

		if (name.size() > 0 && list)
		{
			auto tmpResult = list->getByIndex(index);

			if (tmpResult.value.type().name() == typeid(Object).name())
			{
				auto obj = boost::any_cast<Object>(tmpResult.value);
				ObjectVariableReference ref(name, &obj);

				return resolve(ref);
			}
			else if (tmpResult.value.type().name() == typeid(Object*).name())
			{
				auto obj = boost::any_cast<Object*>(tmpResult.value);
				ObjectVariableReference ref(name, obj);

				return resolve(ref);
			}
			else
				throw VariableException("Invalid reference on object!!!");
		}
		else if (list)
		{
			return resolveVariableReferenceAndExecuteToPtr(list->getByIndex(index));
		}
		else
		{
			auto map  = ref0.get_map();
			auto name = ref0.get_name();

			return resolveVariableReferenceAndExecuteToPtr(map->getVariableByName(name), index);
		}
	}

	Variable* Resolver::resolveVariableListReferenceToPtr(VariableListReference *reference)
	{
		if (reference)
		{
			return &(reference->get_list()->getByIndex(reference->get_index()));
		}
		else
		{
			return NULL;
		}
	}

	Variable* Resolver::resolveVariableListReferenceToPtr(VariableListReference reference)
	{
		return &(reference.get_list()->getByIndex(reference.get_index()));
	}
}