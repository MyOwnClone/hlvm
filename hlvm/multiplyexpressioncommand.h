#pragma once

#include "boost/any.hpp"
#include "command.h"
#include "expressiontree.h"
#include "commandexception.h"

namespace hlvm
{
	class MultiplyExpressionCommand : public Command
	{
	private:
		boost::any target;
		ExpressionTree* source;
	public:
		MultiplyExpressionCommand(boost::any target, ExpressionTree* source) : target(target), source(source), Command()
		{

		};

		virtual bool execute();
		virtual MultiplyExpressionCommand* copy();
		virtual MultiplyExpressionCommand* deepCopy();
	};
}