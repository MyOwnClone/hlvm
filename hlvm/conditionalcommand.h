#pragma once

#include "block.h"
#include "command.h"
#include "conditiontree.h"

namespace hlvm
{
	class ConditionalCommand : public Command
	{
	protected:
		Block block;
		ConditionTree *conditionTree;
	public:
		ConditionalCommand(Block &block, ConditionTree *conditionTree) :block(block), conditionTree(conditionTree) {}

		virtual bool execute();
		virtual ConditionalCommand* copy();
	};
}