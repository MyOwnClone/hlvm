#include "object.h"
#include "variableexception.h"

namespace hlvm
{
	bool Object::addLocalVariable(std::string name, Variable variable)
	{
		if (locals.variableExists(name))
		{
			return false;
		}
		else
		{
			locals.addVariable(name, variable);
		}
	}

	bool Object::addFunction(std::string name, Function &function)
	{
		if (functions.find(name) != functions.end())
		{
			return false;
		}
		else
		{
			functions[name] = function;
			return true;
		}
	}

	Function& Object::getFunctionReferenceByName(std::string name)
	{
		if (functions.find(name) != functions.end())
		{
			return functions[name];
		}
		else
		{
			throw VariableException("Function with given name not found!!!");
		}
	}

	int Object::counter = 0;

	Variable& Object::getVariableReferenceByName(std::string name)
	{
		if (locals.variableExists(name))
		{
			return locals.getVariableByName(name);
		}
		else
		{
			throw VariableException("Function with given name not found!!!");
		}
	}

	Function Object::getFunctionByName(std::string name)
	{
		if (functions.find(name) != functions.end())
		{
			return functions[name];
		}
		else
		{
			throw VariableException("Function with given name not found!!!");
		}
	}
}